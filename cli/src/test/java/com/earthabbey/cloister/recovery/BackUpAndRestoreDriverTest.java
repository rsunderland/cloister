package com.earthabbey.cloister.recovery;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.earthabbey.client.cli.BackUpDriver;
import com.earthabbey.client.cli.RestoreDriver;

public class BackUpAndRestoreDriverTest {

	private static final String PERSISTENCE_UNIT_NAME = "TEST_PERSISTENCE_UNIT";

	@Test
	public void canCreateBackUpToStream() throws IOException {
		RestoreDriver restore = new RestoreDriver(PERSISTENCE_UNIT_NAME);

		String sample = IOUtils.toString(BackUpAndRestoreDriverTest.class
				.getClassLoader().getResourceAsStream("sample_backup.xml"));

		restore.restoreBackup(new StringReader(sample));

		BackUpDriver backup = new BackUpDriver(PERSISTENCE_UNIT_NAME);

		StringWriter writer = new StringWriter();

		backup.createBackup(writer);

		String result = writer.toString();

		System.out.println("[" + sample + "]");
		
		// currently order of backup is not predictable, so not
		// possible to do comparison,
		//TODO make backup up deterministic
		//assertThat(result.trim(), is(sample.trim()));
		
	}

}
