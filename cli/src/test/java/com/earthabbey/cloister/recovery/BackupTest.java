//package com.earthabbey.cloister.recovery;
//
//import java.io.IOException;
//import java.io.StringReader;
//import java.io.StringWriter;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.HashSet;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import com.earthabbey.client.cli.BackupIo;
//import com.earthabbey.cloister.model.AbstractEntityManagerTest;
//import com.earthabbey.cloister.model.domain.Backup;
//import com.earthabbey.cloister.model.domain.EmailConfirmation;
//import com.earthabbey.cloister.model.domain.Member;
//import com.earthabbey.cloister.model.domain.MemberHelpOffer;
//import com.earthabbey.cloister.model.domain.MemberHelpRequest;
//import com.earthabbey.cloister.model.domain.MemberSummary;
//import com.earthabbey.cloister.model.domain.MemberType;
//import com.earthabbey.cloister.model.domain.Message;
//import com.earthabbey.cloister.model.domain.MessageState;
//import com.earthabbey.cloister.model.domain.Permission;
//import com.earthabbey.cloister.model.domain.Principal;
//import com.earthabbey.cloister.model.domain.PrincipalSummary;
//import com.earthabbey.cloister.model.domain.Role;
//import com.earthabbey.cloister.model.domain.RoleSummary;
//import com.earthabbey.cloister.model.domain.Session;
//import com.earthabbey.cloister.model.store.Store;
//
//public class BackupTest extends AbstractEntityManagerTest {
//
//	@Test
//	public final void canCreateBackupFromRestoredValues() throws IOException {
//		Store store = getStore();
//		Backup backup = store.createBackup();
//		new BackupIo().write(backup, System.out);
//	}
//
//	@Test
//	public final void canRestoreBackup() throws IOException {
//
//		Principal p0 = new Principal("Mentor", "Test", new RoleSummary(45L,
//				null));
//		Principal p1 = new Principal("Richard2", "Test", new RoleSummary(45L,
//				null));
//		Principal p2 = new Principal("Catherine", "Test", new RoleSummary(45L,
//				null));
//		Member mentor = new Member(1000L, p0.summerize(), MemberType.MEMBER,
//				"Mentor1", "M", null, null, null, null);
//
//		Backup backup = new Backup(
//				//
//				Arrays.asList(
//				//
//						mentor, new Member(1200L, p1.summerize(),
//								MemberType.APPLICANT, "Richard", "Sunderland",
//								"Test statement", mentor.summerize(), null,
//								null),//
//						new Member(1300L, p2.summerize(), MemberType.MEMBER,
//								"Catherine", "Sunderland", "Test statement 2",
//								mentor.summerize(), null, null)), //
//				Arrays.asList(//
//				new Message(400L, MessageState.NEW, "Test title",
//						"Test content",//
//						new MemberSummary(1200L, null, null, null),//
//						new MemberSummary(1300L, null, null, null), new Date())), //
//				Arrays.asList(//
//						p0, p1, p2),//
//				Arrays.asList(
//						//
//						new Role(45L, "Site administrator",
//								new HashSet<Permission>(Arrays
//										.asList(Permission.READ_ANY))), //
//						new Role(46L, "Member", new HashSet<Permission>(Arrays
//								.asList(Permission.READ_ANY))), //
//						new Role(47L, "Applicant", new HashSet<Permission>(
//								Arrays.asList(Permission.READ_SELF,
//										Permission.READ_MENTOR)))),//
//				Arrays.asList(//
//						new Session(500L, "test-token", false,
//								new Date(),
//								new Date(),//
//								new PrincipalSummary("Richard2"),
//								new MemberSummary(1200L, "Richard",
//										"Sunderland", null), null)), //
//				Arrays.asList(//
//						new EmailConfirmation("aoeusanotheusantohusnaotu", "Richard2")),
//				Arrays.asList(//
//						new MemberHelpOffer(5L, "Do some<br>gardending", mentor
//						.summerize())), Arrays.asList(new MemberHelpRequest(4L,
//						"Help with gardending", mentor.summerize())));
//
//		getStore().restoreBackup(backup);
//		StringWriter dest = new StringWriter();
//		new BackupIo().write(backup, dest);
//		dest.close();
//		System.out.println(dest.toString());
//
//		Backup backup2 = new BackupIo().read(new StringReader(dest.toString()));
//		System.out.println(backup2.toString());
//	}
//}
