package com.earthabbey.client.cli;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Command Line Parser responsible for converting command line arguments into
 * accessible field values.
 * 
 * Throws <code>IllegalArgumentException</code> if the arguments supplied are
 * invalid.
 * 
 * @author rich
 */
public class CommandLineParser {

	/**
	 * Persistence unit that will be return if none are specified.
	 */
	private static final String DEFAULT_PERSISTENCE_UNIT_NAME = "PRODUCTION_PERSISTENCE_UNIT";

	/**
	 * Key that specifies a file that contains a backup to restore.
	 */
	private final static String RESTORE = "restore";

	/**
	 * Key that specifies a file that should be populated with a backup.
	 */
	private final static String BACKUP = "backup";

	/**
	 * Key that specifies that help has been requested.
	 */
	private static final String HELP = "help";

	/**
	 * Key that specifies that a non default persistence unit should be used.
	 */
	private static final String UNIT = "unit";

	/**
	 * Command line options.
	 */
	private Options options;

	/**
	 * Restore option.
	 */
	private Option restore;

	/**
	 * Backup option.
	 */
	private Option backup;

	/**
	 * Help option.
	 */
	private Option help;

	/**
	 * Persistence unit override option.
	 */
	private Option unit;

	/**
	 * Line digested from supplied args.
	 */
	private CommandLine line;

	/**
	 * Constructor.
	 * 
	 * @param args
	 *            arguments passed to program.
	 * 
	 */
	public CommandLineParser() {

		options = new Options();
		restore = OptionBuilder.withArgName("file").hasArg()
				.withDescription("xml file backup file to restore.")
				.create(RESTORE);

		backup = OptionBuilder.withArgName("file").hasArg()
				.withDescription("generate xml backup file.").create(BACKUP);

		help = OptionBuilder.withDescription("print this help").create(HELP);

		unit = OptionBuilder
				.hasArg()
				.withDescription(
						"alternative persistence unit to use (if "
								+ "not specified a default of "
								+ "PRODUCTION_PERSISTENCE_UNIT is used)")
				.create(UNIT);

		options.addOption(restore);
		options.addOption(backup);
		options.addOption(unit);
		options.addOption(help);
	}

	public void parse(String[] args) throws ParseException {

		GnuParser parser = new GnuParser();

		line = parser.parse(options, args);

	}

	/**
	 * 
	 * @return true if the help flag was present on the command line.
	 */
	private boolean helpRequest() {
		return line.hasOption(HELP);
	}

	/**
	 * 
	 * @return true if the restore flag was present on the command line.
	 */
	private boolean restoreRequested() {
		return line.hasOption(RESTORE);
	}

	/**
	 * 
	 * @return true if the backup flag was present on the command line.
	 */
	private boolean backupRequsted() {
		return line.hasOption(BACKUP);
	}

	/**
	 * Determine which operation has been selected.
	 * @return the selected operation.
	 */
	public Operation getOperation() {
		Operation result = null;
		if (helpRequest()) {
			result = Operation.HELP;
		} else {
			if (restoreRequested() && backupRequsted()) {
				throw new IllegalArgumentException(
						"More that one operation requested.  Please request either backup or restore. ");
			} else if (restoreRequested()) {
				result = Operation.RESTORE;
			} else if (backupRequsted()) {
				result = Operation.BACKUP;
			} else {
				throw new IllegalArgumentException(
						"No operations requested. Please request either backup or restore.");
			}
		}
		return result;
	}

	public File getFile() {
		switch (getOperation()) {
		case BACKUP:
			return getBackupFile();
		case RESTORE:
			return getRestoreFile();
		default:
			throw new IllegalStateException();
		}
	}

	public String getPersistenceUnitName() {
		if (line.hasOption(UNIT)) {
			return line.getOptionValue(UNIT);
		} else {
			return DEFAULT_PERSISTENCE_UNIT_NAME;
		}
	}

	private File getRestoreFile() {
		File file = new File(line.getOptionValue(RESTORE));
		if (!file.exists()) {
			throw new IllegalArgumentException(
					"Cannot find backup to restore at "
							+ file.getAbsolutePath());
		}

		if (!file.canRead()) {
			throw new IllegalArgumentException("Found backup to restore at "
					+ file.getAbsolutePath()
					+ ", but can not read it. Check permissionns.");
		}

		return file;
	}

	private File getBackupFile() {
		File file = new File(line.getOptionValue(BACKUP));

		File parent = file.getAbsoluteFile().getParentFile();

		if (!parent.exists()) {
			throw new IllegalArgumentException("Directory does not exist "
					+ parent.getAbsolutePath() + ".");
		}

		if (!parent.canWrite()) {
			throw new IllegalArgumentException("Cannot create backup at "
					+ file.getAbsolutePath() + ". Check permissions");
		}

		return file;
	}

	public void printUsage() {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("cloister-cli", options);
	}

}
