package com.earthabbey.client.cli;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.earthabbey.cloister.model.domain.Backup;
import com.earthabbey.cloister.model.store.Store;

/**
 * Restore Driver responsible for restoring a backup xml file into a persistence
 * unit.
 * 
 * @author rich
 * 
 */
public final class RestoreDriver {

	private final EntityManagerFactory factory;

	/**
	 * Constructor.
	 * 
	 * @param persistenceUnitName
	 *            name of persistence unit to restore backup into.
	 */
	public RestoreDriver(String persistenceUnitName) {
		factory = Persistence.createEntityManagerFactory(persistenceUnitName);
	}

	public RestoreDriver(EntityManagerFactory factory) {
		this.factory = factory;
	}

	/**
	 * Restore a backup. Caller is responsible for ensuring file exists and is
	 * readable.
	 * 
	 * @param file
	 *            file to be populated with backup.
	 * @throws IOException
	 */
	public void restoreBackup(File file) throws IOException {
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			restoreBackup(in);
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	/**
	 * Restore a backup from an InputStream. This method is responsible for
	 * wrapping stream in a reader and delegating it on.
	 * 
	 * @param reader
	 *            to read backup from.
	 * @throws IOException
	 *             if failed to read from reader.
	 */
	public void restoreBackup(InputStream in) throws IOException {
		restoreBackup(new InputStreamReader(in));
	}

	/**
	 * Restore a backup from a Reader. This method is responsible for creating
	 * the entity manager and handling the transaction.
	 * 
	 * @param reader
	 *            to read backup from.
	 * @throws IOException
	 *             if failed to read from reader.
	 */
	public void restoreBackup(Reader reader) throws IOException {

		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		try {
			restoreBackup(reader, manager);
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive()) {
				manager.getTransaction().rollback();
			}
		}
	}

	/***
	 * Create backup by extracting it from a entity manager and outputting to a
	 * Reader.
	 * 
	 * @param in
	 *            to read backup from.
	 * @param manager
	 *            entity manager containing schema to be backedup.
	 * @throws IOException
	 *             if failed to read from stream.
	 * 
	 * @throws IOException
	 */
	private void restoreBackup(Reader in, EntityManager manager)
			throws IOException {
		Store store = new Store(manager);
		Backup backup = new BackupIo().read(in);
		store.restoreBackup(backup);
	}
}
