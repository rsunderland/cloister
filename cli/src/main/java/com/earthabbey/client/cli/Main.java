package com.earthabbey.client.cli;

import java.io.File;
import java.io.IOException;

import javax.persistence.PersistenceException;

import org.apache.commons.cli.ParseException;

public class Main {

	public static void main(String[] args) {
		new Main(args);
	}

	public Main(String[] args) {
		CommandLineParser parser = new CommandLineParser();
		try {
			parser.parse(args);
			switch (parser.getOperation()) {
			case BACKUP:
				createBackup(parser.getFile(), parser.getPersistenceUnitName());
				break;
			case RESTORE:
				restoreBackup(parser.getFile(), parser.getPersistenceUnitName());
				break;
			case HELP:
				printHelp(parser);
				break;
			}
		} catch (IllegalArgumentException e) {
			error("Illegal argument: " + e.getLocalizedMessage(), parser);
		} catch (ParseException e) {
			error("Failed to parse command line: " + e.getLocalizedMessage(),
					parser);
		} catch (IOException e) {
			error("Failed to perform operation: " + e.getLocalizedMessage(),
					parser);
		} catch (PersistenceException e) {
			error("Failed to perform operation: " + e.getLocalizedMessage(),
					parser);
		}
	}

	private void printHelp(CommandLineParser parser) {
		parser.printUsage();
	}

	private void createBackup(File file, String persistenceUnitName)
			throws IOException {
		new BackUpDriver(persistenceUnitName).createBackup(file);
		System.out.println("Back up created.");
	}

	private void restoreBackup(File file, String persistenceUnitName)
			throws IOException {
		new RestoreDriver(persistenceUnitName).restoreBackup(file);
		System.out.println("Back up restored.");
	}

	private void error(String string, CommandLineParser parser) {
		System.out.println("Operation Failed.\n");
		System.out.println("Reason: " + string);
		parser.printUsage();
	}

}
