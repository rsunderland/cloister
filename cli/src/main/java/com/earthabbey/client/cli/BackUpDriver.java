package com.earthabbey.client.cli;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.earthabbey.cloister.model.domain.Backup;
import com.earthabbey.cloister.model.store.Store;

/**
 * Back Up Driver responsible for generating a backup xml file from a
 * persistence unit.
 * 
 * @author rich
 * 
 */
public final class BackUpDriver {

	private final EntityManagerFactory factory;

	/**
	 * Constructor.
	 * 
	 * @param persistenceUnitName
	 *            name of the persistence unit to backup.
	 */
	public BackUpDriver(String persistenceUnitName) {
		factory = Persistence.createEntityManagerFactory(persistenceUnitName);
	}

	/**
	 * Create a backup. Caller is responsible for ensuring file is writable.
	 * 
	 * @param file
	 *            file to be populated with backup.
	 * @throws IOException
	 */
	public void createBackup(File file) throws IOException {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(file);
			createBackup(out);
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * Create a backup to the OutputStream. Wraps stream in writer and
	 * delegates.
	 * 
	 * 
	 * @param out
	 *            to populate with backup.
	 * @throws IOException
	 *             if failed to write to stream.
	 */
	public void createBackup(OutputStream out) throws IOException {
		createBackup(new OutputStreamWriter(out));
	}

	/**
	 * Create a backup to the OutputStream. This method is responsible for
	 * creating the entity manager and handling the transaction.
	 * 
	 * @param out
	 *            to populate with backup.
	 * @throws IOException
	 *             if failed to write to stream.
	 */
	public void createBackup(Writer writer) throws IOException {
		EntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();
		try {
			createBackup(writer, manager);
			manager.getTransaction().commit();
		} finally {
			if (manager.getTransaction().isActive()) {
				manager.getTransaction().rollback();
			}
		}
	}

	/***
	 * Create backup by extracting it from a entity manager and outputting to a
	 * writer.
	 * 
	 * @param out
	 *            writer to be populated with backup.
	 * @param manager
	 *            entity manager containing schema to be backedup.
	 * @throws IOException
	 */
	private void createBackup(Writer out, EntityManager manager)
			throws IOException {
		Store store = new Store(manager);
		Backup backup = store.createBackup();
		new BackupIo().write(backup, out);
	}

}
