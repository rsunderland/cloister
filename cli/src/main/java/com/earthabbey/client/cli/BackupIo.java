package com.earthabbey.client.cli;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import com.earthabbey.cloister.model.domain.Backup;
import com.earthabbey.cloister.model.domain.EmailConfirmation;
import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.domain.Session;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XppDriver;

/**
 * Back Up IO responsible for configuring XStream to correctly
 * serialize/deserealize domain model to and from XML.
 * 
 * @author rich
 */
public class BackupIo {

	private XStream xstream;

	public BackupIo() {

		xstream = new XStream(new CDataDriver());
		// xstream = new XStream(new DomDriver());
		xstream.processAnnotations(Backup.class);
		xstream.processAnnotations(Principal.class);
		xstream.processAnnotations(Member.class);
		xstream.processAnnotations(Message.class);
		xstream.processAnnotations(Session.class);
		xstream.processAnnotations(Role.class);
		xstream.processAnnotations(Permission.class);
		xstream.processAnnotations(EmailConfirmation.class);
		xstream.processAnnotations(MemberHelpOffer.class);
		xstream.processAnnotations(MemberHelpRequest.class);
	}

	public void write(Backup backup, Writer writer) throws IOException {
		xstream.toXML(backup, writer);
		writer.close();
	}

	public void write(Backup backup, OutputStream out) throws IOException {
		write(backup, new OutputStreamWriter(out));
	}

	public Backup read(InputStream in) throws IOException {
		return read(new InputStreamReader(in));
	}

	public Backup read(Reader reader) throws IOException {
		try {
			return new Backup((Backup) xstream.fromXML(reader));
		} finally {
			reader.close();
		}
	}

	private class CDataDriver extends XppDriver {
		public HierarchicalStreamWriter createWriter(Writer out) {
			return new CDataWriter(out);
		}
	}

	private class CDataWriter extends PrettyPrintWriter {
		public CDataWriter(Writer writer) {
			super(writer);
		}

		protected void writeText(QuickWriter writer, String text) {
			writer.write("<![CDATA[");
			writer.write(text);
			writer.write("]]>");
		}
	}
}
