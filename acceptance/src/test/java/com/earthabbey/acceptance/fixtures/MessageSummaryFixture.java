package com.earthabbey.acceptance.fixtures;

import java.util.ArrayList;
import java.util.List;

import com.earthabbey.acceptance.fixtures.util.ClassLoaderRowFixture;
import com.earthabbey.cloister.model.domain.MessageSummary;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.spi.CloisterService;

public class MessageSummaryFixture extends ClassLoaderRowFixture {

	@SuppressWarnings("unchecked")
	@Override
	public Class getTargetClass() {
		return MessageSummaryRow.class;
	}

	@Override
	public Object[] query() throws Exception {
		CloisterService service = ServiceSingleton.getInstance();
		Session session = LoginFixture.getSession();

		List<MessageSummaryRow> rows = new ArrayList<MessageSummaryRow>();

		if (session != null) {
//			List<MessageSummary> summaries = service.getMessages(session,
//					session.getMember());
//			for (MessageSummary summary : summaries) {
//				rows.add(new MessageSummaryRow(summary));
//			}
		}
		return rows.toArray();

	}
}
