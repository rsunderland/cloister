package com.earthabbey.acceptance.fixtures;

import java.util.logging.Logger;

import org.codehaus.mojo.fit.ClassLoaderActionFixture;

import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.spi.CloisterService;

public class LoginFixture extends ClassLoaderActionFixture {

	private final static Logger logger = Logger.getLogger(LoginFixture.class
			.getCanonicalName());

	/**
	 * The username supplied by the fixture.
	 */
	private String username;

	/**
	 * The password suppiled by the fixture.
	 */
	private String password;

	private CloisterService service;

	private static Session session;

	public LoginFixture() {
		service = ServiceSingleton.getInstance();
	}

	public void username(String username) {
		this.username = username;
	}

	public void password(String password) {
		this.password = password;
	}

	public void login() {
		logger.info("Attempting login username=" + username + " password="
				+ password);

		//session = service.startSession(username, password);
	}
	
	public void logout()
	{
		session = null;
	}

	public boolean userIsLoggedIn() {
		return session != null;
	}

	public String loggedInMemberName() {
		String name = "null";
		if (session != null && session.getMember() != null) {
			name = session.getMember().getDisplayText();
		}
		return name;
	}
	
	public static Session getSession()
	{
		return session;
	}

}
