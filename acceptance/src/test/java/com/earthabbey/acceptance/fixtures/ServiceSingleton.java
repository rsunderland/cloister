package com.earthabbey.acceptance.fixtures;

import java.io.IOException;

import com.earthabbey.cloister.server.service.CloisterServiceImpl;
import com.earthabbey.cloister.spi.CloisterService;

public class ServiceSingleton {

	private static CloisterService service;

	public static CloisterService getInstance() {
		if (service == null) {
			try {
				service = new CloisterServiceImpl();
			} catch (IOException e) {
				throw new RuntimeException("Failed to create "
						+ CloisterServiceImpl.class.getCanonicalName());
			}
		}

		return service;
	}
}
