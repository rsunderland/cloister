package com.earthabbey.acceptance.fixtures.util;

import org.codehaus.mojo.fit.FixtureClassLoader;
import org.codehaus.mojo.fit.FixtureClassLoaderEnabled;

import fit.Fixture;
import fit.RowFixture;

/**
 * Extends ColumnFixture to allow a custom ClassLoader to be used for loading
 * fixtures
 * 
 * @author Mauro Talevi
 */
public abstract class ClassLoaderRowFixture extends RowFixture implements
        FixtureClassLoaderEnabled
{    
    private FixtureClassLoader classLoader;
    
    public ClassLoaderRowFixture()
    {
        this(new FixtureClassLoader());
    }
    
    public ClassLoaderRowFixture(FixtureClassLoader classLoader)
    {
        this.classLoader = classLoader;
    }
    
    public void enableClassLoader(FixtureClassLoader classLoader)
    {
        this.classLoader = classLoader;
    }
    
    public Fixture loadFixture(String fixtureName)
            throws InstantiationException, IllegalAccessException
    {
        return classLoader.newFixture(fixtureName);
    }
}

