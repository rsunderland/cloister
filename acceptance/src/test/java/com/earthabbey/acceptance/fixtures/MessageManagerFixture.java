package com.earthabbey.acceptance.fixtures;

import org.codehaus.mojo.fit.ClassLoaderActionFixture;

import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.spi.CloisterService;

public class MessageManagerFixture extends ClassLoaderActionFixture {

	private Long id;
	private CloisterService service;
	private Session session;
	private Message message;

	public MessageManagerFixture() {
		service = ServiceSingleton.getInstance();
		session = LoginFixture.getSession();
	}

	public void selectMessage(Long id) {
		this.id = id;
	}

	public void open() {
		//message = service.getMessage(session, id);
	}

	public String title() {
		return message.getTitle();
	}

	public String content() {
		return message.getContent();
	}

}
