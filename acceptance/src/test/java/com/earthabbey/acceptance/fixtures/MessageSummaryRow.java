package com.earthabbey.acceptance.fixtures;

import com.earthabbey.cloister.model.domain.MessageSummary;

public class MessageSummaryRow {
	private MessageSummary summary;

	public MessageSummaryRow(MessageSummary summary) {
		this.summary = summary;
	}
	
	public Long id()
	{
		return summary.getId();
	}
	
	public String title()
	{
		return summary.getTitle();
	}
	
	public boolean read()
	{
		return summary.isRead();
	}
	
	public String senderName()
	{
		return summary.getSender().getDisplayText();
	}
	
	public Long senderId()
	{
		return summary.getSender().getId();
	}
	
}