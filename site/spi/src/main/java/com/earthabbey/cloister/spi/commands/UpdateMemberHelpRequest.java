package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.spi.Command;

/**
 * Update member details.
 * 
 * @author richards
 *
 */
public class UpdateMemberHelpRequest implements Command {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long memberHelpRequestId;
	private String replacementText;
			
	public UpdateMemberHelpRequest(Long memberHelpRequestId, String replacementText)
	{
		this.memberHelpRequestId = memberHelpRequestId;
		this.replacementText = replacementText;
	}	
	
	/**
	 * Serialisation only constructor.
	 */	
	public UpdateMemberHelpRequest()
	{
	}

	public Long getMemberHelpRequestId() {
		return memberHelpRequestId;
	}

	public String getReplacementText() {
		return replacementText;
	}
}
