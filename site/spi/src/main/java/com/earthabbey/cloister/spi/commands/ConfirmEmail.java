package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.spi.Command;


/**
 * Confirm that a email address is valid.
 * 
 * @author richards
 *
 */
public class ConfirmEmail implements Command {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Validation token supplied via url query string by email client.
	 */
	private String token;
	
	/**
	 * Constructor. 
	 * @param token Validation token supplied via url query string by email client.
	 */
	public ConfirmEmail(String token)
	{
		this.token = token;
	}	
	
	/**
	 * Serialisation only constructor.
	 */	
	public ConfirmEmail()
	{
	}

	/**
	 * @return validation token.
	 */
	public String getToken() {
		return token;
	}
}
