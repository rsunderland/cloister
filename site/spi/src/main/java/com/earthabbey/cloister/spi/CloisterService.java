package com.earthabbey.cloister.spi;

import com.earthabbey.cloister.model.domain.SessionHandle;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("CloisterService")
public interface CloisterService  extends RemoteService {	
	 GwtEvent<?> perform(Command command, SessionHandle sessionHandle);
}
