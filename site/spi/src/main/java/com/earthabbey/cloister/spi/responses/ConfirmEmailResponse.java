package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.spi.handlers.ConfirmEmailResponseHandler;
import com.google.gwt.event.shared.GwtEvent;

public class ConfirmEmailResponse extends GwtEvent<ConfirmEmailResponseHandler> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<ConfirmEmailResponseHandler> TYPE = new GwtEvent.Type<ConfirmEmailResponseHandler>();

	private Member member;

	private boolean success;

	public ConfirmEmailResponse() {
	}
	
	
	public ConfirmEmailResponse(boolean success, Member member) {
		this.success = success;
		this.member = member;
	}

	@Override
	protected void dispatch(ConfirmEmailResponseHandler handler) {
		handler.handleConfirmEmailResponse(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ConfirmEmailResponseHandler> getAssociatedType() {
		return TYPE;
	}


	public Member getMember() {
		return member;
	}

	public boolean isSuccess() {
		return success;
	}	
}
