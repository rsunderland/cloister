package com.earthabbey.cloister.spi.commands;

import java.util.List;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.Command;

/**
 * Send messages to members.
 * @author richards
 *
 */
public class SendMessage implements Command {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	private List<MemberSummary> recipients;
	private MemberSummary sender;
	private String title;
	private String content;
		
	
	public SendMessage(MemberSummary sender,
			List<MemberSummary> recipients, String title, String content) {
		super();
		this.sender = sender;
		this.recipients = recipients;
		this.title = title;
		this.content = content;
	}

	/**
	 * Serialisation only constructor.
	 */	
	public SendMessage()
	{		
	}
	
	public List<MemberSummary> getRecipients() {
		return recipients;
	}
	
	public void setRecipients(List<MemberSummary> recipients) {
		this.recipients = recipients;
	}
	
	public MemberSummary getSender() {
		return sender;
	}
	
	public void setSender(MemberSummary sender) {
		this.sender = sender;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
}
