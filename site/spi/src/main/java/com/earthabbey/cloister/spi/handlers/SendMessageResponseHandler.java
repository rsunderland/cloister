package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.SendMessageResponse;
import com.google.gwt.event.shared.EventHandler;

/**
 * Send Message Response Handler.
 * 
 * Handler that can handle a {@link SendMessageResponse}. 
 * 
 * @author rich
 *
 */
public interface SendMessageResponseHandler extends EventHandler {
	public void handleSendMessageResponse(SendMessageResponse loginResponse);
}
