package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.ServiceErrorResponse;
import com.google.gwt.event.shared.EventHandler;

public interface ServiceErrorResponseHandler extends EventHandler {
	public void handleServiceError(ServiceErrorResponse serviceErrorResponse);
}
