package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.spi.Command;

public class DeleteMessage implements Command {

	private Long messageId;

	public DeleteMessage() {
	}

	public DeleteMessage(Long messageId) {
		this.messageId = messageId;
	}

	public Long getMessageId() {
		return messageId;
	}
}
