package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;

import com.earthabbey.cloister.spi.commands.ApproveApplicant;
import com.earthabbey.cloister.spi.handlers.ApproveApplicantResponseHandler;
import com.google.gwt.event.shared.GwtEvent;


/**
 * Approve Applicant Response.
 * 
 * Response sent by service to a {@link ApproveApplicant} command.
 * 
 * @author rich
 *
 */
public class ApproveApplicantResponse extends GwtEvent<ApproveApplicantResponseHandler> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<ApproveApplicantResponseHandler> TYPE = new GwtEvent.Type<ApproveApplicantResponseHandler>();

	public ApproveApplicantResponse() {
	}

	@Override
	protected void dispatch(ApproveApplicantResponseHandler handler) {
		handler.handleApproveApplicantResponse(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ApproveApplicantResponseHandler> getAssociatedType() {
		return TYPE;
	}
}
