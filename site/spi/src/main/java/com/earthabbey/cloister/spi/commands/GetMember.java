package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.Command;

/**
 * Get a members details.
 * @author richards
 *
 */
public class GetMember implements Command {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	public GetMember(MemberSummary messageSummmary)
	{
		this.id = messageSummmary.getId();
	}
	
	public GetMember(long id) {
		this.id = id;
	}
	
	/**
	 * Serialisation only constructor.
	 */	
	public GetMember()
	{
	}
	
	public Long getId()
	{
		return id;
	}
}
