package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.GetMessageResponse;
import com.google.gwt.event.shared.EventHandler;


/**
 * Get Message Response Handler.
 * 
 * Handler that can handle a {@link GetMessageResponse}. 
 * 
 * @author rich
 *
 */
public interface GetMessageResponseHandler extends EventHandler {
	public void handleGetMessageResponse(GetMessageResponse response);
}
