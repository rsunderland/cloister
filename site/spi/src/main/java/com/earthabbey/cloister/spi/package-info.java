/**
 * Cloister Service Provider Interface.
 * 
 * Defines the interface provided by the service
 * to the user interface.
 * 
 */
package com.earthabbey.cloister.spi;

