package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.spi.handlers.RefreshMemberHandler;
import com.google.gwt.event.shared.GwtEvent;

public class RefreshMemberResponse extends GwtEvent<RefreshMemberHandler>
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<RefreshMemberHandler> TYPE = new GwtEvent.Type<RefreshMemberHandler>();

	
	private Member member;
	
	private boolean editable;
	
	private boolean summaryChanged;
	
	public RefreshMemberResponse(Member member, boolean editable, boolean summaryChanged) {
		this.member = member;
		this.editable = editable;
		this.summaryChanged = summaryChanged;
	}

	public RefreshMemberResponse() {
	}

	public Member getMember() {
		return member;
	}
	
	public boolean isEditable(){
		return editable;
	}
	
	public boolean isSummaryChanged() {
		return summaryChanged;
	}

	@Override
	protected void dispatch(RefreshMemberHandler handler) {
		handler.refreshMember(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<RefreshMemberHandler> getAssociatedType() {
		return TYPE;
	}


}
