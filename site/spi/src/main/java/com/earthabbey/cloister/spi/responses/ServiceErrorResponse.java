package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;

import com.earthabbey.cloister.spi.handlers.ServiceErrorResponseHandler;
import com.google.gwt.event.shared.GwtEvent;

public class ServiceErrorResponse extends GwtEvent<ServiceErrorResponseHandler>
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<ServiceErrorResponseHandler> TYPE = new GwtEvent.Type<ServiceErrorResponseHandler>();

	private String details;

	/**
	 * Serialization only constructor.
	 */
	public ServiceErrorResponse() {
	}

	public ServiceErrorResponse(String details) {
		this.details = details;
	}

	@Override
	protected void dispatch(ServiceErrorResponseHandler handler) {
		handler.handleServiceError(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ServiceErrorResponseHandler> getAssociatedType() {
		return TYPE;
	}

	public String getDetails() {
		return details;
	}
}
