package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;

import com.earthabbey.cloister.spi.commands.Logout;
import com.earthabbey.cloister.spi.handlers.LogoutResponseHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Log Out Response.
 * 
 * Response sent by the service to a {@link Logout}.
 * 
 * @author rich
 *
 */
public class LogoutResponse extends GwtEvent<LogoutResponseHandler> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<LogoutResponseHandler> TYPE = new GwtEvent.Type<LogoutResponseHandler>();

	public LogoutResponse() {
	}

	@Override
	protected void dispatch(LogoutResponseHandler handler) {
		handler.handleLogoutResponse(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LogoutResponseHandler> getAssociatedType() {
		return TYPE;
	}
}
