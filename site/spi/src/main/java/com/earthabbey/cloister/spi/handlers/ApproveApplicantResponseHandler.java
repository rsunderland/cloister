package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.ApproveApplicantResponse;
import com.google.gwt.event.shared.EventHandler;

/**
 * Approve Applicant Response Handler.
 * 
 * @author rich
 *
 */
public interface ApproveApplicantResponseHandler extends EventHandler {
	public void handleApproveApplicantResponse(ApproveApplicantResponse response);
}
