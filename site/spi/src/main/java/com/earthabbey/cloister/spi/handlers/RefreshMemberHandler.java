package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;
import com.google.gwt.event.shared.EventHandler;

/**
 * Get Member Response Handler.
 * 
 * Handler that can handle a {@link RefreshMemberResponse}. 
 * 
 * @author rich
 *
 */
public interface RefreshMemberHandler extends EventHandler {
	public void refreshMember(RefreshMemberResponse response);
}
