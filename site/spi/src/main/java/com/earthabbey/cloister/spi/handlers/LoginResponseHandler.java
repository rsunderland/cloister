package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.LoginResponse;
import com.google.gwt.event.shared.EventHandler;


/**
 * Log In Response Handler.
 * 
 * Handler that can handle a {@link LoginResponse}. 
 * 
 * @author rich
 *
 */
public interface LoginResponseHandler extends EventHandler {
	public void handleLoginResponse(LoginResponse loginResponse);
}
