package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;

import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.spi.handlers.LoginResponseHandler;
import com.google.gwt.event.shared.GwtEvent;

public class LoginResponse extends GwtEvent<LoginResponseHandler>  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final GwtEvent.Type<LoginResponseHandler> TYPE = new GwtEvent.Type<LoginResponseHandler>();
	
	private Session session;
	
	public LoginResponse(Session session) {
		this.session = session;
	}
	
	public LoginResponse()
	{
		
	}
	
	public Session getSession()
	{
		return session;
	}

	@Override
	protected void dispatch(LoginResponseHandler handler) {
		handler.handleLoginResponse(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LoginResponseHandler> getAssociatedType() {
		return TYPE;
	}
}
