package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.Command;

/**
 * Add a new member help request.
 * 
 * @author richards
 * 
 */
public class CreateMemberHelpRequest implements Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Member requesting help.
	 */
	private MemberSummary requestingMember;

	/**
	 * Help requested.
	 */
	private String text;

	/**
	 * Constructor.
	 * 
	 * @param requestingMember Member requesting help.
	 * @param text Help requested.
	 */
	public CreateMemberHelpRequest(MemberSummary requestingMember, String text) {
		this.requestingMember = requestingMember;
		this.text = text;
	}

	/**
	 * Serialisation only constructor.
	 */
	public CreateMemberHelpRequest() {
	}

	public MemberSummary getRequestingMember() {
		return requestingMember;
	}

	public String getText() {
		return text;
	}
}
