package com.earthabbey.cloister.spi;

import com.google.gwt.user.client.rpc.IsSerializable;


/**
 * Action Request.
 * 
 * Marker interface for request sent to service.
 * 
 * @author rich
 *
 */
public interface Command extends IsSerializable {

}
