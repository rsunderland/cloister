package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;
import java.util.List;

import com.earthabbey.cloister.model.domain.MessageSummary;
import com.earthabbey.cloister.spi.handlers.GetMessagesResponseHandler;
import com.google.gwt.event.shared.GwtEvent;

public class GetMessagesResponse extends GwtEvent<GetMessagesResponseHandler>
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<GetMessagesResponseHandler> TYPE = new GwtEvent.Type<GetMessagesResponseHandler>();

	private List<MessageSummary> messageSummaries;

	public GetMessagesResponse() {
	}

	public GetMessagesResponse(List<MessageSummary> messageSummaries) {
		this.messageSummaries = messageSummaries;
	}

	@Override
	protected void dispatch(GetMessagesResponseHandler handler) {
		handler.handleGetMessagesResponse(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<GetMessagesResponseHandler> getAssociatedType() {
		return TYPE;
	}

	public List<MessageSummary> getMessageSummaries() {
		return messageSummaries;
	}
}
