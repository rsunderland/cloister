package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.spi.Command;


/**
 * Log in to service.
 * @author richards
 *
 */
public class Login implements Command {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	
	
	/**
	 * Serialisation only constructor.
	 */	
	public Login()
	{
	}
	
	public Login(String username, String password)
	{
		this.username= username;
		this.password= password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}
