package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.GetMembersResponse;
import com.google.gwt.event.shared.EventHandler;


/**
 * Get Members Response Handler.
 * 
 * Handler that can handle a {@link GetMembersResponse}. 
 * 
 * @author rich
 *
 */
public interface GetMembersResponseHandler extends EventHandler {
	public void handleGetMessageResponse(GetMembersResponse response);
}
