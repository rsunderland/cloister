package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.Command;

/**
 * Add a new member help request.
 * 
 * @author richards
 * 
 */
public class CreateMemberHelpOffer implements Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Member offering help.
	 */
	private MemberSummary offeringMember;

	/**
	 * Help offered.
	 */
	private String text;

	/**
	 * Constructor.
	 * 
	 * @param offeringMember Member offering help.
	 * @param text Help offered.
	 */
	public CreateMemberHelpOffer(MemberSummary offeringMember, String text) {
		this.offeringMember = offeringMember;
		this.text = text;
	}

	/**
	 * Serialisation only constructor.
	 */
	public CreateMemberHelpOffer() {
	}

	public MemberSummary getOfferingMember() {
		return offeringMember;
	}

	public String getText() {
		return text;
	}
}
