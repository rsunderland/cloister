package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;

import com.earthabbey.cloister.spi.handlers.SendMessageResponseHandler;
import com.google.gwt.event.shared.GwtEvent;

public class SendMessageResponse extends GwtEvent<SendMessageResponseHandler> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<SendMessageResponseHandler> TYPE = new GwtEvent.Type<SendMessageResponseHandler>();

	public SendMessageResponse() {
	}

	@Override
	protected void dispatch(SendMessageResponseHandler handler) {
		handler.handleSendMessageResponse(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<SendMessageResponseHandler> getAssociatedType() {
		return TYPE;
	}
}
