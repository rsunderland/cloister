package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.model.domain.MessageSummary;
import com.earthabbey.cloister.spi.Command;

/**
 * Get details of a message.
 * 
 * @author richards
 * 
 */
public class GetMessage implements Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long messageId;

	public GetMessage(Long id) {
		this.messageId = id;
	}

	/**
	 * Serialisation only constructor.
	 */
	public GetMessage() {
	}

	public Long getMessageId() {
		return messageId;
	}

}
