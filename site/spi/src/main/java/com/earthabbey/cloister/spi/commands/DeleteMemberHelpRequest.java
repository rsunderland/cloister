package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.spi.Command;

/**
 * Update member details.
 * 
 * @author richards
 *
 */
public class DeleteMemberHelpRequest implements Command {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long memberHelpRequestId;
			
	public DeleteMemberHelpRequest(Long memberHelpRequestId)
	{
		this.memberHelpRequestId = memberHelpRequestId;
	}	
	
	/**
	 * Serialisation only constructor.
	 */	
	public DeleteMemberHelpRequest()
	{
	}

	public Long getMemberHelpRequestId() {
		return memberHelpRequestId;
	}
}
