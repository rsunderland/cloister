package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.spi.Command;

/**
 * Create new account.
 * 
 * @author richards
 * 
 */
public class CreateAccount implements Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Email address used to identify the account.
	 */
	private String emailAddress;

	/**
	 * Password used to authenticate this account.
	 */
	private String password;

	/**
	 * First name of the newly created user to be associated with this account.
	 */
	private String firstName;

	/**
	 * Last name of the newly created user to be associated with this account.
	 */
	private String lastName;

	/**
	 * URL that should be used as a base of the email confirmation link.
	 */
	private String emailConfirmationLinkBaseUrl;

	/**
	 * Constructor.
	 * 
	 * @param emailAddress
	 *            Email address used to identify the account.
	 * @param firstName
	 *            First name of the newly created user to be associated with
	 *            this account.
	 * @param lastName
	 *            Last name of the newly created user to be associated with this
	 *            account.
	 * @param password
	 *            Password used to authenticate this account.
	 * @param emailConfirmationBaseLink
	 *            URL that should be used as base of the email confirmation
	 *            link.
	 */
	public CreateAccount(String emailAddress, String firstName,
			String lastName, String password, String emailConfirmationBaseLink) {
		super();
		this.emailAddress = emailAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.emailConfirmationLinkBaseUrl = emailConfirmationBaseLink;
	}

	/**
	 * Serialisation only constructor.
	 */
	public CreateAccount() {
	}

	/**
	 * @return Email address used to identify the account.
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @return First name of the newly created user to be associated with this
	 *         account.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return Last name of the newly created user to be associated with this
	 *         account.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return Password used to authenticate this account.
	 */
	public String getPassword() {
		return password;
	}
	
	public String getEmailConfirmationBaseUrl() {
		return emailConfirmationLinkBaseUrl;
	}

	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append("email:").append(emailAddress);
		bld.append(" firstName:").append(firstName);
		bld.append(" lastName:").append(lastName);
		return bld.toString();
	}

	
}
