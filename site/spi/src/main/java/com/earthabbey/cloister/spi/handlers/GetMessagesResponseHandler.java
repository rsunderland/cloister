package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.GetMessagesResponse;
import com.google.gwt.event.shared.EventHandler;

/**
 * Get Messages Response Handler.
 * 
 * Handler that can handle a {@link GetMessagesResponse}. 
 * 
 * @author rich
 *
 */
public interface GetMessagesResponseHandler extends EventHandler {
	public void handleGetMessagesResponse(GetMessagesResponse response);
}
