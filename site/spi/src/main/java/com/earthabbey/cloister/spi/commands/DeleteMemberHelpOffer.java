package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.spi.Command;

/**
 * Update member details.
 * 
 * @author richards
 * 
 */
public class DeleteMemberHelpOffer implements Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long memberHelpOfferId;

	public DeleteMemberHelpOffer(Long memberHelpOfferId) {
		this.memberHelpOfferId = memberHelpOfferId;
	}

	/**
	 * Serialisation only constructor.
	 */
	public DeleteMemberHelpOffer() {
	}

	public Long getMemberHelpOfferId() {
		return memberHelpOfferId;
	}
}
