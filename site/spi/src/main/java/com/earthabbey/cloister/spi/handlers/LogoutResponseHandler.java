package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.LogoutResponse;
import com.google.gwt.event.shared.EventHandler;

/**
 * Log Out Response Handler.
 * 
 * Handler that can handle a {@link LogoutResponse}. 
 * 
 * @author rich
 *
 */
public interface LogoutResponseHandler extends EventHandler {
	public void handleLogoutResponse(LogoutResponse loginResponse);
}
