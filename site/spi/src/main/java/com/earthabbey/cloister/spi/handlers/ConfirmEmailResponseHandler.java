package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.ConfirmEmailResponse;
import com.google.gwt.event.shared.EventHandler;


/**
 * Confirm Email Response Handler.
 * 
 * Handler that can handle a {@link ConfirmEmailResponse}. 
 * 
 * @author rich
 *
 */
public interface ConfirmEmailResponseHandler extends EventHandler {
	public void handleConfirmEmailResponse(ConfirmEmailResponse response);
}
