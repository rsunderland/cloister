package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;
import java.util.List;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.spi.handlers.CreateAccountResponseHandler;
import com.google.gwt.event.shared.GwtEvent;

public class CreateAccountResponse extends
		GwtEvent<CreateAccountResponseHandler> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<CreateAccountResponseHandler> TYPE = new GwtEvent.Type<CreateAccountResponseHandler>();

	private boolean success;
	private List<String> failureReasons;
	private Member newlyCreatedMember;

	public CreateAccountResponse() {
	}

	public CreateAccountResponse(boolean success, List<String> failureReasons,
			Member newlyCreatedMember) {
		this.success = success;
		this.failureReasons = failureReasons;
		this.newlyCreatedMember = newlyCreatedMember;
	}

	public Member getNewlyCreatedMember() {
		return newlyCreatedMember;
	}

	public boolean isSuccess() {
		return success;
	}

	public List<String> getFailureReasons() {
		return failureReasons;
	}

	@Override
	protected void dispatch(CreateAccountResponseHandler handler) {
		handler.handle(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<CreateAccountResponseHandler> getAssociatedType() {
		return TYPE;
	}
}
