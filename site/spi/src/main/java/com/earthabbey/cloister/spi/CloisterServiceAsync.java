package com.earthabbey.cloister.spi;

import com.earthabbey.cloister.model.domain.SessionHandle;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface CloisterServiceAsync {

	void perform(
			Command command,
			SessionHandle sessionHandle,
			AsyncCallback<GwtEvent<?>> response);

}
