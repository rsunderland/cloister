package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.Command;

/**
 * Get list of messages.
 * @author richards
 *
 */
public class GetMessages implements Command {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MemberSummary member;
		

	
	public GetMessages(MemberSummary member)
	{
		this.member = member;
	}
	
	
	/**
	 * Serialisation only constructor.
	 */	
	public GetMessages()
	{
	}

	public MemberSummary getMember() {
		return member;
	}
}
