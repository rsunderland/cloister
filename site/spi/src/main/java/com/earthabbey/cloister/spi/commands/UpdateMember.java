package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.spi.Command;

/**
 * Update member details.
 * 
 * @author richards
 *
 */
public class UpdateMember implements Command {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Member member;
			
	public UpdateMember(Member member)
	{
		this.member = member;
	}	
	
	/**
	 * Serialisation only constructor.
	 */	
	public UpdateMember()
	{
	}

	public Member getMember() {
		return member;
	}
}
