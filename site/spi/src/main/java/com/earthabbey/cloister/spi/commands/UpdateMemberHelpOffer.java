package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.spi.Command;

/**
 * Update member details.
 * 
 * @author richards
 *
 */
public class UpdateMemberHelpOffer implements Command {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long memberHelpOfferId;
	private String replacementText;
			
	public UpdateMemberHelpOffer(Long memberHelpOfferId, String replacementText)
	{
		this.memberHelpOfferId = memberHelpOfferId;
		this.replacementText = replacementText;
	}	
	
	/**
	 * Serialisation only constructor.
	 */	
	public UpdateMemberHelpOffer()
	{
	}

	public Long getMemberHelpOfferId() {
		return memberHelpOfferId;
	}

	public String getReplacementText() {
		return replacementText;
	}
}
