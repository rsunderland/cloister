package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;
import java.util.List;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.handlers.GetMembersResponseHandler;
import com.google.gwt.event.shared.GwtEvent;

public class GetMembersResponse extends GwtEvent<GetMembersResponseHandler>
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<GetMembersResponseHandler> TYPE = new GwtEvent.Type<GetMembersResponseHandler>();

	private List<MemberSummary> memberSummaries;

	
	public GetMembersResponse(List<MemberSummary> memberSummaries) {
		this.memberSummaries = memberSummaries;
	}

	public GetMembersResponse() {
	}

	public List<MemberSummary> getMemberSummaries() {
		return memberSummaries;
	}

	@Override
	protected void dispatch(GetMembersResponseHandler handler) {
		handler.handleGetMessageResponse(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<GetMembersResponseHandler> getAssociatedType() {
		return TYPE;
	}
}
