package com.earthabbey.cloister.spi.handlers;

import com.earthabbey.cloister.spi.responses.CreateAccountResponse;
import com.google.gwt.event.shared.EventHandler;

/**
 * Create Account Response Handler.
 * 
 * Handler that can handle a {@link CreateAccountResponse}. 
 * 
 * @author rich
 *
 */
public interface CreateAccountResponseHandler extends EventHandler {
	public void handle(CreateAccountResponse createAccountResponse);
}
