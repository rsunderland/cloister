package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.spi.Command;


/**
 * Log out of service.
 * @author richards
 *
 */
public class Logout implements Command {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	public Logout()
	{
	}
}
