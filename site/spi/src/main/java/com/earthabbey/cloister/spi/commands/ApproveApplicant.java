package com.earthabbey.cloister.spi.commands;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.Command;

/**
 * Upgraded an applicant to full member status.
 * 
 * @author richards
 * 
 */
public class ApproveApplicant implements Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Applicant to be upgraded.
	 */
	private MemberSummary applicant;

	/**
	 * Constructor.
	 * 
	 * @param applicant
	 *            applicant to be upgraded.
	 */
	public ApproveApplicant(MemberSummary applicant) {
		this.applicant = applicant;
	}

	/**
	 * Serialisation only constructor.
	 */
	public ApproveApplicant() {
	}

	public MemberSummary getApplicant() {
		return applicant;
	}
}
