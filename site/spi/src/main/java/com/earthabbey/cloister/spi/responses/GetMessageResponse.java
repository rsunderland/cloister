package com.earthabbey.cloister.spi.responses;

import java.io.Serializable;

import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.spi.handlers.GetMessageResponseHandler;
import com.google.gwt.event.shared.GwtEvent;

public class GetMessageResponse extends GwtEvent<GetMessageResponseHandler> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final GwtEvent.Type<GetMessageResponseHandler> TYPE = new GwtEvent.Type<GetMessageResponseHandler>();

	private Message message;

	public GetMessageResponse() {
	}

	public GetMessageResponse(Message message) {
		this.message = message;
	}

	public Message getMessage() {
		return message;
	}

	@Override
	protected void dispatch(GetMessageResponseHandler handler) {
		handler.handleGetMessageResponse(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<GetMessageResponseHandler> getAssociatedType() {
		return TYPE;
	}
}
