package com.earthabbey.cloister.model.test.domain;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public final class SerializationUtils {

	public static <T> T roundTrip(T request) throws IOException,
			ClassNotFoundException {
		ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
		ObjectOutputStream objectOutput = new ObjectOutputStream(byteOutput);
		objectOutput.writeObject(request);
		objectOutput.close();
		byteOutput.close();

		ByteArrayInputStream byteInput = new ByteArrayInputStream(
				byteOutput.toByteArray());
		ObjectInputStream objectInput = new ObjectInputStream(byteInput);
		Object objectIn = objectInput.readObject();
		objectInput.close();
		byteInput.close();
		return (T) objectIn;
	}

}
