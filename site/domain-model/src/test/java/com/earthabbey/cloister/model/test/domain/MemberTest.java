package com.earthabbey.cloister.model.test.domain;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.PrincipalSummary;

/**
 * Test unit for {@link MemberHelpRequest}.
 * 
 * @author rich
 * 
 */
public class MemberTest {

	@Test
	public void canSerialize() throws IOException, ClassNotFoundException {

		Long memberId = 45L;
		MemberType type = MemberType.APPLICANT;
		String firstName = "firstName";
		String lastName = "surname";
		String wayOfLifeStatement = "way of life statement";
		MemberSummary mentor = new MemberSummary(5L, "mentor-first",
				"mentor-surname", MemberType.APPLICANT);

		MemberSummary summary = new MemberSummary(memberId, firstName,
				lastName, type);

		List<MemberHelpOffer> offers = Arrays.asList(new MemberHelpOffer(7L,
				"offer1", summary), new MemberHelpOffer(8L, "offer2", summary));

		List<MemberHelpRequest> requests = Arrays.asList(new MemberHelpRequest(
				9L, "request1", summary), new MemberHelpRequest(10L,
				"request2", summary));		
		
		Member member = new Member(memberId, new PrincipalSummary("45"), type, firstName, lastName,
				wayOfLifeStatement, mentor, offers, requests);

		Member memberOut = SerializationUtils.roundTrip(member);

		assertNotNull(memberOut);
		assertThat(memberOut.getId(), is(memberOut.getId()));
		assertThat(memberOut.getFirstName(), is(memberOut.getFirstName()));
		assertThat(memberOut.getSurname(), is(memberOut.getSurname()));
		assertThat(memberOut.getWayOfLifeSummary(),
				is(memberOut.getWayOfLifeSummary()));

		MemberSummary mentorOut = memberOut.getMentor();
		assertThat(mentorOut.getId(), is(mentor.getId()));
		assertThat(mentorOut.getDisplayText(), is(mentor.getDisplayText()));
		assertThat(mentorOut.getType(), is(mentor.getType()));

		assertThat(memberOut.getHelpOffers().size(), is(member.getHelpOffers()
				.size()));
		int i;
		i = 0;
		for (MemberHelpOffer offerOut : memberOut.getHelpOffers()) {
			MemberHelpOffer offerIn = member.getHelpOffers().get(i);
			i++;
			assertNotNull(offerOut);
			assertThat(offerOut.getId(), is(offerIn.getId()));
			assertThat(offerOut.getText(), is(offerIn.getText()));
			assertThat(offerOut.getOfferingMember().getId(), is(offerIn
					.getOfferingMember().getId()));
			assertThat(offerOut.getOfferingMember().getDisplayText(),
					is(offerIn.getOfferingMember().getDisplayText()));
			assertThat(offerOut.getOfferingMember().getType(), is(offerIn
					.getOfferingMember().getType()));
		}

		i = 0;
		for (MemberHelpRequest requestOut : memberOut.getHelpRequests()) {
			MemberHelpRequest requestIn = member.getHelpRequests().get(i);
			i++;
			assertNotNull(requestOut);
			assertThat(requestOut.getId(), is(requestIn.getId()));
			assertThat(requestOut.getText(), is(requestIn.getText()));
			assertThat(requestOut.getRequestingMember().getId(), is(requestIn
					.getRequestingMember().getId()));
			assertThat(requestOut.getRequestingMember().getDisplayText(),
					is(requestIn.getRequestingMember().getDisplayText()));
			assertThat(requestOut.getRequestingMember().getType(), is(requestIn
					.getRequestingMember().getType()));
		}

	}

}
