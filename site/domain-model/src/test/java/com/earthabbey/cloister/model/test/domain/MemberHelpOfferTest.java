package com.earthabbey.cloister.model.test.domain;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;

import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.MemberType;

/**
 * Test unit for {@link MemberHelpRequest}.
 * 
 * @author rich
 * 
 */
public class MemberHelpOfferTest {

	@Test
	public void canSerialize() throws IOException, ClassNotFoundException {
		MemberSummary memberSummary = new MemberSummary(5L, "first", "surname",
				MemberType.APPLICANT);
		MemberHelpOffer requestIn = new MemberHelpOffer(6L, "request text",
				memberSummary);

		MemberHelpOffer requestOut = SerializationUtils.roundTrip(requestIn);

		assertNotNull(requestOut);
		assertThat(requestOut.getId(), is(requestIn.getId()));
		assertThat(requestOut.getText(), is(requestIn.getText()));
		assertThat(requestOut.getOfferingMember().getId(), is(requestIn
				.getOfferingMember().getId()));
		assertThat(requestOut.getOfferingMember().getDisplayText(),
				is(requestIn.getOfferingMember().getDisplayText()));
		assertThat(requestOut.getOfferingMember().getType(), is(requestIn
				.getOfferingMember().getType()));

	}

}
