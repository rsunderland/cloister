package com.earthabbey.cloister.model.test.domain;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;

import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.MemberType;

/**
 * Test unit for {@link MemberHelpRequest}.
 * 
 * @author rich
 * 
 */
public class MemberHelpRequestTest {

	@Test
	public void canSerialize() throws IOException, ClassNotFoundException {
		MemberSummary memberSummary = new MemberSummary(5L, "first", "surname",
				MemberType.APPLICANT);
		MemberHelpRequest requestIn = new MemberHelpRequest(6L, "request text",
				memberSummary);

		MemberHelpRequest requestOut = SerializationUtils.roundTrip(requestIn);

		assertNotNull(requestOut);
		assertThat(requestOut.getId(), is(requestIn.getId()));
		assertThat(requestOut.getText(), is(requestIn.getText()));
		assertThat(requestOut.getRequestingMember().getId(), is(requestIn
				.getRequestingMember().getId()));
		assertThat(requestOut.getRequestingMember().getDisplayText(), is(requestIn
				.getRequestingMember().getDisplayText()));
		assertThat(requestOut.getRequestingMember().getType(), is(requestIn
				.getRequestingMember().getType()));

	}

}
