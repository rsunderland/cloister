package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Permission")
public enum Permission {
		
	/**
	 * Principal can login.
	 */
	LOGIN,
		
	/**
	 * Principal can read the details of all members.
	 */
	READ_ANY,
	
	/**
	 * Principal can read the details of all members (not necessarily include applicants).
	 */
	READ_MEMBERS,

	/**
	 * Principal can read the details of all applicants.
	 */
	READ_APPLICANTS,
	
	/**
	 * Principal can read the details of the associated member.
	 */
	READ_SELF,	
	
	/**
	 * Principal can read the details of the mentor of the associtated member.
	 */
	READ_MENTOR,	
	
	/**
	 * Principal can send a message to mentor of the associated member.
	 */
	MESSAGE_MENTOR,
	
	/**
	 * Principal can send a message to any member.
	 */
	MESSAGE_ANY,
	
	/**
	 * Principal can update the details of the associated member.
	 */
	UPDATE_SELF,
	
	/**
	 * Principal can update the details of any member.
	 */
	UPDATE_ANY,
	
	
	/**
	 * Accept in applicants as mentees
	 */
	ACCEPT_NEW_MENTEE,
	
	/**
	 * An approve an applicant, converting them to a full member.
	 */
	APPROVE_APPLICANT,
		
	/**
	 * Website administration.
	 */
	SITE_ADMIN;

}
