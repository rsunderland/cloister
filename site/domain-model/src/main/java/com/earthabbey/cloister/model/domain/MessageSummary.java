package com.earthabbey.cloister.model.domain;

public class MessageSummary extends IdentifiedObject implements SummaryObject<Long> {
	
	/**
	 * Serial id. 
	 */
	private static final long serialVersionUID = 1L;
	
	private String title;
	private boolean read;
	private MemberSummary recipient;
	private MemberSummary sender;
	
	/**
	 * Serialization only constructor.
	 */
	public MessageSummary() {

	}

	public MessageSummary(Long id, String title, boolean read, MemberSummary sender, MemberSummary recipient) {
		setId(id);		
		this.title = title;
		this.read = read;
		this.sender = sender;
		this.recipient = recipient;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}
	
	 
	

	public MemberSummary getRecipient() {
		return recipient;
	}

	public void setRecipient(MemberSummary recipient) {
		this.recipient = recipient;
	}

	public MemberSummary getSender() {
		return sender;
	}

	public void setSender(MemberSummary sender) {
		this.sender = sender;
	}
	
	public String toString()
	{
		StringBuilder bld = new StringBuilder();
		bld.append(getId()).append(":'").append(title).append("':").append(read?"R":"N");
		return bld.toString();
	}


}
