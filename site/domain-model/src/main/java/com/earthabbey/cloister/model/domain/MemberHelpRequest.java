package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("HelpRequest")
public class MemberHelpRequest extends IdentifiedObject implements DomainObject<Long, MemberHelpRequestSummary>  {
	
	/**
	 * Default Serial Id.
	 */
	private static final long serialVersionUID = 1L;

	private String text;
	
	private MemberSummary requestingMember;

	/**
	 * Serialization only.
	 */
	public MemberHelpRequest()
	{		
	}
	
	public MemberHelpRequest(Long id, String text, MemberSummary offeringMember)
	{
		super(id);
		this.text = text;
		this.requestingMember = offeringMember;
	}
	
	@Override
	public MemberHelpRequestSummary summerize() {
		return new MemberHelpRequestSummary(getId(), text, requestingMember);
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public MemberSummary getRequestingMember() {
		return requestingMember;
	}

	public void setRequestingMember(MemberSummary offeringMember) {
		this.requestingMember = offeringMember;
	}
	
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append("MemberHelpRequest(");
		bld.append(" id=").append(getId());
		bld.append(" member=").append(requestingMember);
		bld.append(" text=").append(text);
		bld.append(")");
		return bld.toString();
	}
}
