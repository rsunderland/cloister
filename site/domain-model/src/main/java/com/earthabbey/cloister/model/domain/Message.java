package com.earthabbey.cloister.model.domain;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * A message sent to a {@link Member}.
 * 
 * @author richards
 * 
 */
@XStreamAlias("Message")
public class Message extends IdentifiedObject implements
		DomainObject<Long, MessageSummary> {

	/**
	 * Serial id.
	 */
	private static final long serialVersionUID = 3611958717769544884L;

	private MemberSummary owner;

	private MemberSummary sender;

	private MemberSummary recipient;

	@XStreamAsAttribute
	private Date sendTime;

	@XStreamAsAttribute
	private MessageState state;

	@XStreamAsAttribute
	private String title;

	private String content;

	/** Serialisation only */
	public Message() {

	}

	public Message(Long id, MessageState state, String title, String content,
			MemberSummary owner, MemberSummary recipient, MemberSummary sender,
			Date sendTime) {
		super(id);
		this.state = state;
		this.title = title;
		this.content = content;
		this.owner = owner;
		this.recipient = recipient;
		this.sendTime = sendTime;
		this.sender = sender;
	}

	
	
	public MemberSummary getOwner() {
		return owner;
	}

	public void setOwner(MemberSummary owner) {
		this.owner = owner;
	}

	public MemberSummary getSender() {
		return sender;
	}

	public void setSender(MemberSummary sender) {
		this.sender = sender;
	}

	public MemberSummary getRecipient() {
		return recipient;
	}

	public void setRecipient(MemberSummary recipient) {
		this.recipient = recipient;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public MessageState getState() {
		return state;
	}

	public void setState(MessageState state) {
		this.state = state;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append("Message(");
		bld.append("id=").append(getId());
		bld.append(" sender=").append(sender.getId());
		bld.append(" recipient=").append(recipient.getId());
		bld.append(" title=").append(title);
		bld.append(" content=").append(content);
		bld.append(" state=").append(state);
		bld.append(" sendTime=").append(sendTime);
		bld.append(")");
		return bld.toString();
	}

	@Override
	public MessageSummary summerize() {
		return new MessageSummary(getId(), title, state == MessageState.READ,
				sender, recipient);
	}
}
