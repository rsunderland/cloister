package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class MemberSummary extends IdentifiedObject implements
		SummaryObject<Long> {

	/**
	 * Serial id.
	 */
	private static final long serialVersionUID = 1L;

	@XStreamOmitField
	private String displayText;

	@XStreamOmitField
	private MemberType type;

	/** Serialisation only */
	public MemberSummary() {
	}

	public MemberSummary(long id, String firstName, String surname, MemberType type) {
		setId(id);
		this.displayText = firstName + " " + surname;
		this.type = type;
	}

	public String getDisplayText() {
		return displayText;
	}

	public String toString() {
		return getId() + "{" + displayText + "}";
	}

	public MemberType getType() {
		return type;
	}

	public boolean isApplicant() {
		return type == MemberType.APPLICANT;
	}

	public boolean isMember() {
		return type == MemberType.MEMBER;
	}

	public boolean isMentorOf(Member actor) {
		return actor != null && actor.getMentor() != null
				&& actor.getMentor().getId().equals(getId());
	}

	public boolean is(Member actor) {
		return actor != null && actor.getId().equals(getId());
	}

	
}
