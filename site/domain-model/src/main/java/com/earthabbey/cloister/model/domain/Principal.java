package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("Principal")
public class Principal implements DomainObject<String, PrincipalSummary> {

	/**
	 * Serial id.
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAsAttribute
	private String id;

	@XStreamAsAttribute
	private String hashedPassword;

	@XStreamOmitField
	private MemberSummary member;

	private RoleSummary role;

	/**
	 * Serialization only constructor.
	 */
	@Deprecated
	public Principal() {

	}

	public Principal(String id, String hashedPassword, RoleSummary role) {
		this.id = id;
		this.hashedPassword = hashedPassword;		
		this.role = role;
		if (role == null) {
			throw new IllegalArgumentException(
					"Attempting to create a principal without a role.");
		}
	}

	@Override
	public String getId() {
		return id;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public void setId(String id) {
		this.id = id;
	}

	public MemberSummary getMember() {
		return member;
	}

	public void setMember(MemberSummary member) {
		this.member = member;
	}

	public RoleSummary getRole() {
		return role;
	}

	public void setRole(RoleSummary role) {
		this.role = role;
	}

	@Override
	public PrincipalSummary summerize() {
		return new PrincipalSummary(getId());
	}

	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append("Principal(id=").append(id);
		if (member != null) {
			bld.append(" member=").append(member.getId());
		}
		bld.append(")");
		return bld.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hashedPassword == null) ? 0 : hashedPassword.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((member == null) ? 0 : member.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Principal other = (Principal) obj;
		if (hashedPassword == null) {
			if (other.hashedPassword != null)
				return false;
		} else if (!hashedPassword.equals(other.hashedPassword))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (member == null) {
			if (other.member != null)
				return false;
		} else if (!member.equals(other.member))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}

}
