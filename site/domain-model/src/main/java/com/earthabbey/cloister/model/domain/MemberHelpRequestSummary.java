package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("HelpRequest")
public class MemberHelpRequestSummary extends IdentifiedObject implements SummaryObject<Long>  {
	
	/**
	 * Default Serial Id.
	 */
	private static final long serialVersionUID = 1L;

	private String text;

	private MemberSummary requestingMember;

	/**
	 * Serialization only.
	 */
	public MemberHelpRequestSummary()
	{		
	}
	
	public MemberHelpRequestSummary(Long id, String text, MemberSummary memberSummary)
	{
		super(id);
		this.text = text;		
		this.requestingMember = memberSummary;
	}
	
	public String getText() {
		return text;
	}
}
