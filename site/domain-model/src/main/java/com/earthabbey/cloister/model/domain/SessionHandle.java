package com.earthabbey.cloister.model.domain;

public class SessionHandle extends IdentifiedObject {

	/**
	 * Serial id. 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String token;
	
	/** Serialization only */
	public SessionHandle(){}
	
	public SessionHandle(Long id, String token)
	{
		super(id);
		this.token = token;
	}

	public String getToken() {
		return token;
	}
}
