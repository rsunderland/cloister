package com.earthabbey.cloister.model.domain;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


public class IdentifiedObject implements IdentifiableObject<Long>, Serializable {

	/**
	 * Serial id. 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * The unique id of this object. Id is only unique with respect to final
	 * concrete class. May be null if object has not yet been stored.
	 */
	@XStreamAsAttribute
	private Long id;

	/** Serialisation only */
	public IdentifiedObject()
	{		
	}
	
	public IdentifiedObject(Long id) {
		this.id = id;
	}

	/**
	 * Get the id of the object.
	 */
	@Override
	public Long getId() {
		return id;
	}

	/**
	 * Update the id. This should be used with extreme care, there should not
	 * normally be a need to change the id.
	 * 
	 * @param id
	 *            replacement value for the id.
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdentifiedObject other = (IdentifiedObject) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
