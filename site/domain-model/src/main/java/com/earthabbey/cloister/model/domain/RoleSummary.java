package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("RoleSummary")
public class RoleSummary extends IdentifiedObject implements SummaryObject<Long> {

	/**
	 * Serial id.
	 */
	private static final long serialVersionUID = 1L;



	@XStreamOmitField
	private String descriptiveName;

	/**
	 * Serialisation only.
	 */
	public RoleSummary() {
	}
	
	public RoleSummary(Long id, String descriptiveName)
	{
		super(id);
		this.descriptiveName= descriptiveName;
	}


	public String getDescriptiveName() {
		return descriptiveName;
	}

	public void setDescriptiveName(String descriptiveName) {
		this.descriptiveName = descriptiveName;
	}
	
	public String toString(){
		return "Role(" + descriptiveName + ")";
	}

}
