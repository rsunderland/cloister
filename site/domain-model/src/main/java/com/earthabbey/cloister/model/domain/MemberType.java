package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("MemberType")
public enum MemberType {

	/**
	 * A member that is not a full member yet.
	 */
	APPLICANT,

	/**
	 * A normal member.
	 */
	MEMBER
}
