package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("EmailConfirmation")
public class EmailConfirmation implements
		DomainObject<String, EmailConfirmation>, SummaryObject<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String token;
	private String emailAddress;

	/**
	 * Serialization only.
	 */
	public EmailConfirmation() {
	}

	public EmailConfirmation(String token, String emailAddress) {
		super();
		this.token = token;
		this.emailAddress = emailAddress;
	}

	@Override
	public EmailConfirmation summerize() {
		return this;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public String getId() {
		return token;
	}
}
