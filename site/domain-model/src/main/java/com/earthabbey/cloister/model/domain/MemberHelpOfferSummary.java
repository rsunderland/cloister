package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("HelpOffer")
public class MemberHelpOfferSummary extends IdentifiedObject implements  SummaryObject<Long>  {
	
	/**
	 * Default Serial Id.
	 */
	private static final long serialVersionUID = 1L;

	private String text;


	/**
	 * Serialization only.
	 */
	public MemberHelpOfferSummary()
	{		
	}
	
	public MemberHelpOfferSummary(Long id, String text, MemberSummary offeringMember)
	{
		super(id);
		this.text = text;
	}	
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
