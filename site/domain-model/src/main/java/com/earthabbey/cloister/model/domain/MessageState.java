package com.earthabbey.cloister.model.domain;

/**
 * Description of the state of a message from the point of view of the
 * recipient.
 * 
 * @author richards
 */
public enum MessageState {
	/**
	 * This message is marked as having not been read.
	 */
	NEW,

	/**
	 * This message is marked as having been read.
	 */
	READ,

	/**
	 * This message is marked as having been deleted.
	 */
	DELETED
}
