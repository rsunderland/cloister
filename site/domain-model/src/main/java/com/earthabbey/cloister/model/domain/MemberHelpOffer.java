package com.earthabbey.cloister.model.domain;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("HelpOffer")
public class MemberHelpOffer extends IdentifiedObject implements DomainObject<Long, MemberHelpOfferSummary>, IsSerializable{
	
	/**
	 * Default Serial Id.
	 */
	private static final long serialVersionUID = 1L;

	private String text;
	
	private MemberSummary offeringMember;

	/**
	 * Serialization only.
	 */
	public MemberHelpOffer()
	{		
	}
	
	public MemberHelpOffer(Long id, String text, MemberSummary offeringMember)
	{
		super(id);
		this.text = text;
		this.offeringMember = offeringMember;
	}
	
	@Override
	public MemberHelpOfferSummary summerize() {
		return new MemberHelpOfferSummary(getId(), text, offeringMember);
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public MemberSummary getOfferingMember() {
		return offeringMember;
	}

	public void setOfferingMember(MemberSummary offeringMember) {
		this.offeringMember = offeringMember;
	}
	
	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append("MemberHelpOffer(");
		bld.append(" id=").append(getId());
		bld.append(" member=").append(offeringMember);
		bld.append(" text=").append(text);
		bld.append(")");
		return bld.toString();
	}

}
