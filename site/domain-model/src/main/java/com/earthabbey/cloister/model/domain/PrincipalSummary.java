package com.earthabbey.cloister.model.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("PrincipalSummary")
public class PrincipalSummary implements SummaryObject<String>{

	/**
	 * Serial id.
	 */
	private static final long serialVersionUID = 1L;
		
	@XStreamAsAttribute
	private String id;
		
	/** Serialisation only */
	public PrincipalSummary()
	{
		
	}
	
	public PrincipalSummary(String id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return id;
	}
}
