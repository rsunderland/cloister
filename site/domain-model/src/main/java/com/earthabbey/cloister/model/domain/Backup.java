package com.earthabbey.cloister.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Backup")
public class Backup implements Serializable, IsSerializable {

	/**
	 * Serial id.
	 */
	private static final long serialVersionUID = 1L;

	@XStreamImplicit(itemFieldName = "Role")
	private List<Role> roles;

	@XStreamImplicit(itemFieldName = "Principal")
	private List<Principal> principals;

	@XStreamImplicit(itemFieldName = "Member")
	private List<Member> members;

	@XStreamImplicit(itemFieldName = "Message")
	private List<Message> messages;

	@XStreamImplicit(itemFieldName = "Session")
	private List<Session> sessions;

	@XStreamImplicit(itemFieldName = "HelpOffer")
	private List<MemberHelpOffer> helpOffers;

	@XStreamImplicit(itemFieldName = "HelpRequest")
	private List<MemberHelpRequest> helpRequests;

	@XStreamImplicit(itemFieldName = "EmailConfirmation")
	private List<EmailConfirmation> emailConfirmations;

	/** Serialisation only */
	public Backup() {

	}

	public Backup(List<Member> members, List<Message> messages,
			List<Principal> principals, List<Role> roles,
			List<Session> sessions, List<EmailConfirmation> emailConfirmations,
			List<MemberHelpOffer> helpOffers,
			List<MemberHelpRequest> helpRequests) {
		super();
		this.members = copyOrInitialise(members);
		this.messages = copyOrInitialise(messages);
		this.principals = copyOrInitialise(principals);
		this.roles = copyOrInitialise(roles);
		this.sessions = copyOrInitialise(sessions);
		this.emailConfirmations = copyOrInitialise(emailConfirmations);
		this.helpOffers = copyOrInitialise(helpOffers);
		this.helpRequests = copyOrInitialise(helpRequests);
	}

	public Backup(Backup other) {
		this(other.getMembers(), other.getMessages(), other.getPrincipals(),
				other.getRoles(), other.getSessions(), other
						.getEmailConfirmations(), other.helpOffers,
				other.helpRequests);
	}

	public <T> List<T> copyOrInitialise(List<T> argument) {
		if (argument == null) {
			return new ArrayList<T>();
		}
		return argument;
	}

	public List<Principal> getPrincipals() {
		return principals;
	}

	public List<Member> getMembers() {
		return members;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public List<Session> getSessions() {
		return sessions;
	}

	public List<EmailConfirmation> getEmailConfirmations() {
		return emailConfirmations;
	}

	public List<MemberHelpOffer> getHelpOffers() {
		return helpOffers;
	}

	public List<MemberHelpRequest> getHelpRequests() {
		return helpRequests;
	}

	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append("Backup(\n");
		for (Member member : members) {
			bld.append(member).append("\n");
		}
		for (Message message : messages) {
			bld.append(message).append("\n");
		}
		for (Principal principal : principals) {
			bld.append(principal).append("\n");
		}
		for (Role role : roles) {
			bld.append(role).append("\n");
		}
		for (Session session : sessions) {
			bld.append(session).append("\n");
		}
		for (EmailConfirmation confirmation : emailConfirmations) {
			bld.append(confirmation).append("\n");
		}
		for (MemberHelpOffer helpOffer : helpOffers) {
			bld.append(helpOffer).append("\n");
		}
		for (MemberHelpRequest helpRequest : helpRequests) {
			bld.append(helpRequest).append("\n");
		}
		bld.append(")");

		return bld.toString();

	}
}
