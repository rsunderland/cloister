package com.earthabbey.cloister.model.domain;

public class SessionSummary extends IdentifiedObject implements SummaryObject<Long>{

	/**
	 * Serial id. 
	 */
	private static final long serialVersionUID = 1L;
	
	/** Serialisation only */
	public SessionSummary()
	{
	
	}
	
	public SessionSummary(Long id)
	{
		super(id);
	}
}
