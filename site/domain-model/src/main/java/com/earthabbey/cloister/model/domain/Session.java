package com.earthabbey.cloister.model.domain;

import java.util.Collections;
import java.util.Date;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("Session")
public class Session extends SessionHandle implements
		DomainObject<Long, SessionSummary> {

	/**
	 * Serial id.
	 */
	private static final long serialVersionUID = 1L;

	private PrincipalSummary principal;
	
	private MemberSummary member;	

	@XStreamAsAttribute
	private Date loginTime;

	@XStreamAsAttribute
	private Date logOutTime;

	@XStreamAsAttribute
	private boolean expired;
	
	@XStreamOmitField
	private Set<Permission> permissions;

	/** Serialization only */
	public Session() {
	}

	public Session(Long id, String token, boolean expired, Date logInTime,
			Date logOutTime, PrincipalSummary principal, MemberSummary member, Set<Permission> permissions) {
		super(id, token);
		this.expired = expired;
		this.logOutTime = logOutTime;
		this.loginTime = logInTime;
		this.principal = principal;
		this.member = member;
		this.permissions = permissions;
	}

	public Session(String token, PrincipalSummary principal, MemberSummary member) {
		this(null, token, false, new Date(), null,
				principal, member, Collections.EMPTY_SET);
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogOutTime() {
		return logOutTime;
	}

	public void setLogOutTime(Date logOutTime) {
		this.logOutTime = logOutTime;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public PrincipalSummary getPrincipal() {
		return principal;
	}

	public void setPrincipal(PrincipalSummary principal) {
		this.principal = principal;
	}
	
	public MemberSummary getMember() {
		return member;
	}

	public void setMember(MemberSummary member) {
		this.member = member;
	}
	
	public Set<Permission> getPermissions() {
		return permissions;
	}	

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	@Override
	public SessionSummary summerize() {
		return new SessionSummary(getId());
	}

}
