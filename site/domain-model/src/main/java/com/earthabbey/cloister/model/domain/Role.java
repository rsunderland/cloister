package com.earthabbey.cloister.model.domain;

import java.util.HashSet;
import java.util.Set;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Role")
public class Role extends IdentifiedObject implements
		DomainObject<Long, RoleSummary> {

	/**
	 * Serial id.
	 */
	private static final long serialVersionUID = 5084848899245848370L;

	@XStreamAsAttribute
	private String name;

	/**
	 * The permissions this role has.
	 */
	@XStreamImplicit
	private Set<Permission> permissions;

	/**
	 * Serialisation only.
	 */
	public Role() {
		super();
	}

	public Role(Long id, String descriptiveName, Set<Permission> permissions) {
		super(id);
		this.name = descriptiveName;

		// make a defensive copy of permissions collections to
		// ensure that exotic collection implementations (like those
		// used by JPA) do not make there way this domain object.
		this.permissions = new HashSet<Permission>();
		if (permissions != null) {
			for (Permission permission : permissions) {
				this.permissions.add(permission);
			}
		}
	}

	@Override
	public RoleSummary summerize() {
		return new RoleSummary(getId(), name);
	}

	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return "Role(" + getId() + ":" + name + ")";
	}

}
