package com.earthabbey.cloister.model.domain;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("Member")
public class Member extends IdentifiedObject implements
		DomainObject<Long, MemberSummary> {

	/**
	 * Serial id.
	 */
	private static final long serialVersionUID = 1L;

	@XStreamAsAttribute
	private String firstName;

	@XStreamAsAttribute
	private String surname;

	private String wayOfLifeSummary;

	@XStreamAsAttribute
	private MemberType type;

	private MemberSummary mentor;

	/**
	 * Help Offers.
	 * 
	 * These are not serialised by XStream as part of the member because they
	 * are serialised in there own right.
	 */
	@XStreamOmitField
	private List<MemberHelpOffer> helpOffers;

	/**
	 * Help Requests.
	 * 
	 * These are not serialised by XStream as part of the member because they
	 * are serialised in there own right.
	 */
	@XStreamOmitField
	private List<MemberHelpRequest> helpRequests;

	/**
	 * Prinicpal that this member is associated with. 
	 */	
	private PrincipalSummary principal;

	/**
	 * This constructor is for serialisation only. Use the parameter-ed
	 * constructor below.
	 */
	public Member() {
	}

	public Member(Long id, PrincipalSummary principal, MemberType type,
			String firstName, String surname, String wayOfLifeStatement,
			MemberSummary mentor, List<MemberHelpOffer> helpOffers,
			List<MemberHelpRequest> helpRequests) {
		setId(id);
		if(principal == null)
		{
			throw new IllegalArgumentException("Can not create a Member with a null Principal");
		}
		this.principal = principal;
		this.type = type;
		this.firstName = firstName;
		this.surname = surname;
		this.wayOfLifeSummary = wayOfLifeStatement;
		this.mentor = mentor;
		this.helpOffers = helpOffers;
		this.helpRequests = helpRequests;
	}

	public Member(Member member) {
		this(member.getId(), member.principal, member.getType(), member.firstName,
				member.surname, member.wayOfLifeSummary, member.mentor,
				member.helpOffers, member.helpRequests);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getWayOfLifeSummary() {
		return wayOfLifeSummary;
	}

	public void setWayOfLifeSummary(String wayOfLifeSummary) {
		this.wayOfLifeSummary = wayOfLifeSummary;
	}

	public MemberSummary summerize() {
		return new MemberSummary(getId(), firstName, surname, type);
	}

	public MemberSummary getMentor() {
		return mentor;
	}

	public void setMentor(MemberSummary mentor) {
		this.mentor = mentor;
	}

	public MemberType getType() {
		return type;
	}

	public void setType(MemberType type) {
		this.type = type;
	}

	public List<MemberHelpOffer> getHelpOffers() {
		return helpOffers;
	}

	public void setHelpOffers(List<MemberHelpOffer> helpOffers) {
		this.helpOffers = helpOffers;
	}

	public List<MemberHelpRequest> getHelpRequests() {
		return helpRequests;
	}

	public void setHelpRequests(List<MemberHelpRequest> helpRequests) {
		this.helpRequests = helpRequests;
	}

	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append("Member(");
		bld.append(" id=").append(getId());		
		bld.append(" firstName=").append(firstName);
		bld.append(" surname=").append(surname);
		bld.append(" wayoflifestatement=").append(wayOfLifeSummary);
		bld.append(")");
		return bld.toString();
	}

	public PrincipalSummary getPrincipal() {
		return principal;
	}

	public void setPrincipal(PrincipalSummary principal) {
		this.principal = principal;
	}
}
