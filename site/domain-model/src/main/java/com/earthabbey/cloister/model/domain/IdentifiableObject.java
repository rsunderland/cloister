package com.earthabbey.cloister.model.domain;

public interface IdentifiableObject<K> {
	K getId();
}
