package com.earthabbey.cloister.model.domain;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface DomainObject<K, SO extends SummaryObject<K>> extends
		Serializable, IdentifiableObject<K>, IsSerializable {
	SO summerize();
}
