package com.earthabbey.cloister.model.domain;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface SummaryObject<K> extends Serializable, IdentifiableObject<K>,
		IsSerializable {
}
