package com.earthabbey.cloister.client.ui.impl;

import java.util.List;

import com.earthabbey.cloister.client.ui.api.MemberSelectorView;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

public class MemberSelectorViewImpl extends Composite implements
		MemberSelectorView, SelectionChangeEvent.Handler {

	private static MemberSelectorViewImplUiBinder uiBinder = GWT
			.create(MemberSelectorViewImplUiBinder.class);

	interface MemberSelectorViewImplUiBinder extends
			UiBinder<Widget, MemberSelectorViewImpl> {
	}

	public static final CommonResources base = GWT
			.create(CommonResources.class);

	static {
		base.style().ensureInjected();
	}

	@UiField(provided = true)
	CellList<MemberSummary> memberList;

	private Presenter presenter;

	private SingleSelectionModel<MemberSummary> selectionModel;

	private MemberSummaryCell cell;

	public MemberSelectorViewImpl() {
		cell = new MemberSummaryCell();
		memberList = new CellList<MemberSummary>(cell);
		initWidget(uiBinder.createAndBindUi(this));

		selectionModel = new SingleSelectionModel<MemberSummary>();
		memberList.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(this);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setMemberSummaries(List<MemberSummary> summaries) {
		memberList.setRowCount(summaries.size());
		memberList.setPageSize(summaries.size());
		memberList.setRowData(0, summaries);
		memberList.redraw();
	}

	@Override
	public void onSelectionChange(SelectionChangeEvent arg0) {
		MemberSummary selected = selectionModel.getSelectedObject();
		presenter.select(selected);
	}

}
