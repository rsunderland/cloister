package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.client.ui.api.AppLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class AppLayoutImpl extends Composite implements AppLayout {

	interface AppLayoutUiBinder extends UiBinder<LayoutPanel, AppLayoutImpl> {
	}

	private static AppLayoutUiBinder binder = GWT
			.create(AppLayoutUiBinder.class);

	private final LayoutPanel mainLayoutPanel;

	@UiField
	ScrollPanel selectorPanel;

	@UiField
	ScrollPanel mainPanel;

	@UiField
	SimplePanel actionPanel;

	private static final int GUTTER = 10;

	public AppLayoutImpl() {
		mainLayoutPanel = binder.createAndBindUi(this);
		initWidget(mainLayoutPanel);
		setNoSideSelectorLayout();
	}

	@Override
	public LayoutPanel getMainLayoutPanel() {

		return mainLayoutPanel;
	}

	@Override
	public AcceptsOneWidget getSelectorContainer() {
		return new AcceptsOneWidget() {
			@Override
			public void setWidget(IsWidget w) {
				Widget widget = Widget.asWidgetOrNull(w);
				selectorPanel.setWidget(widget);
			}
		};
	}

	@Override
	public AcceptsOneWidget getMainContainer() {
		return new AcceptsOneWidget() {
			@Override
			public void setWidget(IsWidget w) {
				Widget widget = Widget.asWidgetOrNull(w);
				mainPanel.setWidget(widget);
			}
		};
	}

	@Override
	public AcceptsOneWidget getActionContainer() {
		return new AcceptsOneWidget() {
			@Override
			public void setWidget(IsWidget w) {
				Widget widget = Widget.asWidgetOrNull(w);
				actionPanel.setWidget(widget);
			}
		};
	}

	@Override
	public void setMessageLayout() {
		setLayout(250, 100);
	}

	@Override
	public void setMembersLayout() {
		setLayout(200, 150);
	}

	@Override
	public void setNoSideSelectorLayout() {
		setLayout(0, 0);
	}

	private void setLayout(int sideBarWidth, int actionBarHeight) {

		// horizontal layout

		mainLayoutPanel.setWidgetLeftWidth(selectorPanel, GUTTER, Unit.PX,
				sideBarWidth, Unit.PX);

		mainLayoutPanel.setWidgetLeftWidth(actionPanel, GUTTER, Unit.PX,
				sideBarWidth, Unit.PX);

		mainLayoutPanel.setWidgetLeftRight(mainPanel, sideBarWidth + GUTTER
				+ GUTTER, Unit.PX, GUTTER, Unit.PX);

		// vertical layout.

		mainLayoutPanel.setWidgetTopBottom(mainPanel, GUTTER, Unit.PX, GUTTER,
				Unit.PX);

		mainLayoutPanel.setWidgetTopBottom(selectorPanel, GUTTER, Unit.PX,
				actionBarHeight + GUTTER + GUTTER, Unit.PX);

		mainLayoutPanel.setWidgetBottomHeight(actionPanel, GUTTER, Unit.PX,
				actionBarHeight, Unit.PX);

	}
}
