package com.earthabbey.cloister.client.ui.api;

import java.util.List;

import com.earthabbey.cloister.model.domain.MessageSummary;
import com.google.gwt.user.client.ui.IsWidget;

public interface MessageSelectorView extends IsWidget {

	void setPresenter(Presenter presenter);
	
	void setMessageSummaries(List<MessageSummary> summaries);
	

	public interface Presenter {
		void select(MessageSummary summary);
	}


	void setViewerId(long viewerId);
	
	void setShowSentMessages(boolean show);
}
