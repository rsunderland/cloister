package com.earthabbey.cloister.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;


public class LoginPlace extends Place {
	
	@Prefix("login")
	public static class Tokenizer implements PlaceTokenizer<LoginPlace> {
		@Override
		public String getToken(LoginPlace place) {
			return "";
		}

		@Override
		public LoginPlace getPlace(String token) {
			return new LoginPlace();
		}
	}
}