package com.earthabbey.cloister.client.ui.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface TextEditor extends IsWidget {
	
	public enum Mode
	{
		EDIT,
		DISPLAY
	}
	
	void setHTML(String html);
	void setEditable(boolean editable);
	void setDeletable(boolean deletable);
	void setPresenter(Presenter presenter);
	void setMode(Mode mode);
		
	public interface Presenter
	{
		void save(String updatedHtml);
		void delete();
		void cancel();
	}

}
