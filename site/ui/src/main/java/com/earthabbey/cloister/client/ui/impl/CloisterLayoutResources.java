package com.earthabbey.cloister.client.ui.impl;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

public interface CloisterLayoutResources extends ClientBundle {

	@Source("cloister-layout.css")
	Style style();

	@ImageOptions(repeatStyle = RepeatStyle.None)
	@Source("com/earthabbey/page_head.jpg")
	ImageResource headerImage();

	@ImageOptions(repeatStyle = RepeatStyle.None)
	@Source("com/earthabbey/page_foot.jpg")
	ImageResource footerImage();

	public interface Style extends CssResource {
		String header();
		
		String title();
		
		String subTitle();

		String content();

		String footer();
	}
}
