package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.model.domain.MessageSummary;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class MessageSummaryCell extends AbstractCell<MessageSummary> {

	/**
	 * Whether both sent and received messages are being displayed, or just
	 * received messages.
	 */
	private boolean showSent;

	/**
	 * Id of viewer, used to determine whether a message is a sent or recieved
	 * message.
	 */
	private long viewerId;

	public void setShowSent(boolean showSentMessages) {
		this.showSent = showSentMessages;
	}

	public void setViewerId(long viewerId) {
		this.viewerId = viewerId;
	}

	@Override
	public void render(MessageSummary summary, Object arg1, SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (summary == null) {
			return;
		}
		
		boolean sentMessage = false;
		if(showSent)
		{
			sentMessage = (summary.getSender().getId() == viewerId);
		}

		sb.appendHtmlConstant("<div class='selection-panel'><table><tr><td colspan='2'>");
		if (!summary.isRead()) {
			sb.appendHtmlConstant("<b>");
		}
		if (showSent) {
			if(sentMessage)
			{
				sb.appendHtmlConstant("To ");
				sb.appendEscaped(summary.getRecipient().getDisplayText());
			}
			else
			{
				sb.appendHtmlConstant("From ");
				sb.appendEscaped(summary.getSender().getDisplayText());
			}
		} else {
			sb.appendEscaped(summary.getSender().getDisplayText());
		}

		if (!summary.isRead()) {
			sb.appendHtmlConstant("</b>");
		}
		sb.appendHtmlConstant("</td></tr><tr><td style='width:10px'>&nbsp;</td><td>");
		sb.appendEscaped(summary.getTitle());

		sb.appendHtmlConstant("</td></tr></table></div>");

	}

}
