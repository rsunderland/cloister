package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class MemberSummaryCell extends AbstractCell<MemberSummary> {

	@Override
	public void render(MemberSummary summary, Object arg1, SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (summary == null) {
			return;
		}

		sb.appendHtmlConstant("<div class='selection-panel'>");
		if(summary.isApplicant())
		{
			sb.appendHtmlConstant("<b>");
		}
		sb.appendEscaped(summary.getDisplayText());
		if(summary.isApplicant())
		{
			sb.appendHtmlConstant("</b>");
		}
		sb.appendHtmlConstant("</div>");		
	}

}
