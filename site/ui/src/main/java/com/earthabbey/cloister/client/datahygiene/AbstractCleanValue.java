package com.earthabbey.cloister.client.datahygiene;

public class AbstractCleanValue<T> implements CleanValue<T> {

	private T cleanValue;

	private final CleanContext context;

	public AbstractCleanValue(CleanContext context) {
		this.context = context;
	}

	protected void setCleanValue(T cleanValue) {
		this.cleanValue = cleanValue;
	}

	protected void addIssue(String validationError) {
		context.addIssue(validationError);
	}

	public String toString() {
		return cleanValue == null ? "null" : cleanValue.toString();
	}

	@Override
	public T getCleanValue() {
		return cleanValue;
	}
}
