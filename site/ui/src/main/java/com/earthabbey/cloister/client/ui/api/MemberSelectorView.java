package com.earthabbey.cloister.client.ui.api;

import java.util.List;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.google.gwt.user.client.ui.IsWidget;

public interface MemberSelectorView  extends IsWidget {
	
	
	void setPresenter(Presenter presenter);
	
	void setMemberSummaries(List<MemberSummary> summaries);
	

	
	public interface Presenter
	{
		void select(MemberSummary summary);
	}

}
