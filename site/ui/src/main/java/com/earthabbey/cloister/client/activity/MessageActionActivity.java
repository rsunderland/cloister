package com.earthabbey.cloister.client.activity;

import com.earthabbey.cloister.client.compose.ComposeMessageEvent;
import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.MessageActionView;
import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.spi.commands.DeleteMessage;
import com.earthabbey.cloister.spi.handlers.GetMessageResponseHandler;
import com.earthabbey.cloister.spi.responses.GetMessageResponse;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Message Action Activity manages display and selection of message related
 * actions.
 * @author rich
 */
public class MessageActionActivity extends AbstractActivity implements
		MessageActionView.Presenter, GetMessageResponseHandler {

	private ClientFactory clientFactory;
	private MessagePlace place;
	private MessageActionView view;
	private EventBus eventBus;
	private Message message;

	public MessageActionActivity(MessagePlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.place = place;
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {
		view = clientFactory.getMessageActionView();
		view.setPresenter(this);
		container.setWidget(view);
		this.eventBus = eventBus;
		eventBus.addHandler(GetMessageResponse.TYPE, this);
		
		
	}

	@Override
	public void reply() {
		if (message != null) {
			eventBus.fireEvent(new ComposeMessageEvent(message.getRecipient(),
					message.getSender()));
		}
	}

	@Override
	public void delete() {
		if (message != null) {
			clientFactory.getInvoker().send(new DeleteMessage(message.getId()));
		}
	}

	@Override
	public void handleGetMessageResponse(GetMessageResponse response) {
		this.message = response.getMessage();
		view.enableReply(message.getRecipient().getId().equals(clientFactory.getInvoker().getSelf().getId()));
	}

	@Override
	public void showSentMessage(boolean show) {
		clientFactory.getPlaceController().goTo(new MessagePlace(show));
	}
}
