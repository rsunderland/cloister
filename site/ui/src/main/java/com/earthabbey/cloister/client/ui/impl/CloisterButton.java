package com.earthabbey.cloister.client.ui.impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class CloisterButton extends Composite implements HasClickHandlers {

	private static CloisterButtonUiBinder uiBinder = GWT
			.create(CloisterButtonUiBinder.class);

	interface CloisterButtonUiBinder extends UiBinder<Widget, CloisterButton> {
	}

	public static final CloisterButtonResources local = GWT
			.create(CloisterButtonResources.class);

	static {
		local.style().ensureInjected();
	}

	@UiField
	FocusPanel container;

	@UiField
	Label text;

	private boolean enabled;

	public CloisterButton() {
		initWidget(uiBinder.createAndBindUi(this));
		enabled = true;
	}

	public void setText(String text) {
		this.text.setText(text);
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return container.addClickHandler(new ClickHandlerImpl(handler));
	}

	private class ClickHandlerImpl implements ClickHandler {
		private ClickHandler handler;

		public ClickHandlerImpl(ClickHandler handler) {
			this.handler = handler;
		}

		@Override
		public void onClick(ClickEvent arg0) {
			if (enabled) {
				handler.onClick(arg0);
			}
		}
	}
}
