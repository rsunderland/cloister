package com.earthabbey.cloister.client.datahygiene;

public interface CleanValue<T> {
	
	T getCleanValue(); 
}
