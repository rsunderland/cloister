package com.earthabbey.cloister.client.mvp;

import com.earthabbey.cloister.client.place.AdminPlace;
import com.earthabbey.cloister.client.place.ConfirmEmailPlace;
import com.earthabbey.cloister.client.place.LoginPlace;
import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.place.SignUpPlace;
import com.earthabbey.cloister.model.domain.EmailConfirmation;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

@WithTokenizers({ LoginPlace.Tokenizer.class, SignUpPlace.Tokenizer.class,
		MemberPlace.Tokenizer.class, MessagePlace.Tokenizer.class,
		AdminPlace.Tokenizer.class, ConfirmEmailPlace.Tokenizer.class })
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}