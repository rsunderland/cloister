package com.earthabbey.cloister.client.ui.api;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.LayoutPanel;

public interface AppLayout extends IsWidget {
	
	LayoutPanel getMainLayoutPanel();

	AcceptsOneWidget getSelectorContainer();

	AcceptsOneWidget getMainContainer();	
	
	AcceptsOneWidget getActionContainer();

	void setMessageLayout();

	void setMembersLayout();

	void setNoSideSelectorLayout();
}
