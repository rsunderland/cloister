package com.earthabbey.cloister.client.activity;

import java.util.ArrayList;
import java.util.List;

import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.place.MessageSelectorPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.MessageSelectorView;
import com.earthabbey.cloister.model.domain.MessageSummary;
import com.earthabbey.cloister.spi.handlers.GetMessagesResponseHandler;
import com.earthabbey.cloister.spi.handlers.LogoutResponseHandler;
import com.earthabbey.cloister.spi.responses.GetMessagesResponse;
import com.earthabbey.cloister.spi.responses.LogoutResponse;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Message Selector Activity displays the list of available messages and allows
 * the user to select which to view.
 * 
 * @author rich
 * 
 */
public class MessageSelectorActivity extends AbstractActivity implements
		MessageSelectorView.Presenter, LogoutResponseHandler,
		GetMessagesResponseHandler {

	private ClientFactory clientFactory;
	private MessageSelectorView view;
	private final boolean showSentMessages;

	public MessageSelectorActivity(MessageSelectorPlace place,
			ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		showSentMessages = place.isShowSentMessages();
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {
		view = clientFactory.getMessageSelectorView();
		view.setPresenter(this);
		container.setWidget(view);		
		view.setViewerId(clientFactory.getInvoker().getSelf().getId());
		view.setShowSentMessages(showSentMessages);
		eventBus.addHandler(LogoutResponse.TYPE, this);
		eventBus.addHandler(GetMessagesResponse.TYPE, this);
	}

	@Override
	public void select(MessageSummary summary) {
		clientFactory.getPlaceController().goTo(new MessagePlace(summary.getId()));
	}

	@Override
	public void handleGetMessagesResponse(GetMessagesResponse response) {
		List<MessageSummary> summaries = response.getMessageSummaries();
		if (showSentMessages) {
			view.setMessageSummaries(summaries);
		} else {
			view.setMessageSummaries(filterOutSentMessages(summaries));
		}
	}

	private List<MessageSummary> filterOutSentMessages(
			List<MessageSummary> summaries) {
		List<MessageSummary> filteredSummaries = new ArrayList<MessageSummary>();
		long selfId = clientFactory.getInvoker().getSelf().getId();
		for (MessageSummary summary : summaries) {
			if (!summary.getSender().getId().equals(selfId)) {
				filteredSummaries.add(summary);
			}
		}
		return filteredSummaries;
	}

	@Override
	public void handleLogoutResponse(LogoutResponse loginResponse) {
		view.setMessageSummaries(new ArrayList<MessageSummary>());
	}
}
