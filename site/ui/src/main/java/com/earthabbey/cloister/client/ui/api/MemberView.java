package com.earthabbey.cloister.client.ui.api;

import java.util.List;

import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.google.gwt.user.client.ui.IsWidget;

public interface MemberView extends IsWidget {

	void setEditable(boolean editable);

	void setName(String name);

	void setWayOfLifeSummary(String wayOfLifeSummaryHtml);

	void setPresenter(Presenter presenter);

	void setHelpOffers(List<MemberHelpOffer> offers);

	void setHelpRequests(List<MemberHelpRequest> requests);
	

	public interface Presenter {

		void setWayOfLifeStatement(String updatedHtml);

		void updateHelpOffer(Long id, String updatedHtml);

		void deleteHelpOffer(Long id);

		void updateHelpRequest(Long id, String updatedHtml);

		void deleteHelpRequest(Long id);

		void createHelpRequest(String updatedHtml);

		void createHelpOffer(String updatedHtml);

	}
}
