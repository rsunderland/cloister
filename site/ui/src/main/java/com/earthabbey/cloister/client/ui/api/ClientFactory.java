package com.earthabbey.cloister.client.ui.api;

import com.earthabbey.cloister.client.ActionServiceInvoker;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;

public interface ClientFactory {	
	ActionServiceInvoker getInvoker();
	EventBus getEventBus();
	PlaceController getPlaceController();
	
	CloisterLayout getCloisterLayout();
	LoginView getLoginView();
	SignUpView getSignUpView();	
	AdminView getAdminView();
	AppLayout getAppLayout();
		
	MemberView getMemberView();
	MemberActionView getMemberActionView();
	MemberSelectorView getMemberSelectorView();
		
	MessageView getMessageView();
	MessageActionView getMessageActionView();
	MessageSelectorView getMessageSelectorView();
	
	ConfirmEmailView getConfirmEmailView();
}
