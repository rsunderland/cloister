package com.earthabbey.cloister.client.mvp;

import com.earthabbey.cloister.client.activity.MemberSelectorActivity;
import com.earthabbey.cloister.client.activity.MessageSelectorActivity;
import com.earthabbey.cloister.client.place.MemberSelectorPlace;
import com.earthabbey.cloister.client.place.MessageSelectorPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class SelectorPanelMapper implements ActivityMapper {

	private ClientFactory clientFactory;

	public SelectorPanelMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public Activity getActivity(Place place) {

		if (place instanceof MemberSelectorPlace) {
			return new MemberSelectorActivity((MemberSelectorPlace) place,
					clientFactory);
		} else if (place instanceof MessageSelectorPlace) {
			return new MessageSelectorActivity((MessageSelectorPlace) place,
					clientFactory);
		}
		return null;
	}

}
