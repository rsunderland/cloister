package com.earthabbey.cloister.client.ui.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface AdminView extends IsWidget {
	
	public interface Presenter {

	}

	void setPresenter(Presenter presenter);

}
