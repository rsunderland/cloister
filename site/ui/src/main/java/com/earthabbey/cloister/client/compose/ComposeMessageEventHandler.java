package com.earthabbey.cloister.client.compose;

import com.google.gwt.event.shared.EventHandler;

public interface ComposeMessageEventHandler extends EventHandler{
	public void handleComposeMessageEvent(
			ComposeMessageEvent composeMessageEvent);
}
