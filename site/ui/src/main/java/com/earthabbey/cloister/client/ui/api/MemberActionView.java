package com.earthabbey.cloister.client.ui.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface MemberActionView extends IsWidget {

	void enableSendMessage(boolean enable);
	void enableApproveApplicant(boolean enable);
	
	void setPresenter(Presenter presenter);

	public interface Presenter {
		void sendMessage();

		void approveApplicant();
	}
}
