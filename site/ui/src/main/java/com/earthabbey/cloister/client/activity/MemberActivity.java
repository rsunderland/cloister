package com.earthabbey.cloister.client.activity;

import java.util.ArrayList;

import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.MemberView;
import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.spi.commands.CreateMemberHelpOffer;
import com.earthabbey.cloister.spi.commands.CreateMemberHelpRequest;
import com.earthabbey.cloister.spi.commands.DeleteMemberHelpOffer;
import com.earthabbey.cloister.spi.commands.DeleteMemberHelpRequest;
import com.earthabbey.cloister.spi.commands.GetMember;
import com.earthabbey.cloister.spi.commands.GetMembers;
import com.earthabbey.cloister.spi.commands.UpdateMember;
import com.earthabbey.cloister.spi.commands.UpdateMemberHelpOffer;
import com.earthabbey.cloister.spi.commands.UpdateMemberHelpRequest;
import com.earthabbey.cloister.spi.handlers.LogoutResponseHandler;
import com.earthabbey.cloister.spi.handlers.RefreshMemberHandler;
import com.earthabbey.cloister.spi.responses.LogoutResponse;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Member Activity manages presentation and editing of member details.
 * 
 * @author rich
 */
public class MemberActivity extends AbstractActivity implements
		MemberView.Presenter, RefreshMemberHandler, LogoutResponseHandler {

	private ClientFactory clientFactory;
	private MemberView view;
	private MemberPlace place;
	private Member member;
	private EventBus eventBus;

	public MemberActivity(MemberPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.place = place;

	}

	@Override
	public void start(AcceptsOneWidget container, EventBus event) {
		GWT.log("start member activity " + place);
		event.addHandler(RefreshMemberResponse.TYPE, this);
		event.addHandler(LogoutResponse.TYPE, this);

		this.eventBus = event;
		view = clientFactory.getMemberView();
		view.setPresenter(this);

		container.setWidget(view);
		clientFactory.getInvoker().send(new GetMembers());

		if (place.getId() != null) {
			clientFactory.getInvoker().send(new GetMember(place.getId()));
		}

	}

	@Override
	public void handleLogoutResponse(LogoutResponse loginResponse) {
		member = null;
		view.setEditable(false);
		view.setName("");
		view.setWayOfLifeSummary("");
		view.setHelpOffers(new ArrayList<MemberHelpOffer>());
		view.setHelpRequests(new ArrayList<MemberHelpRequest>());
	}

	@Override
	public void refreshMember(RefreshMemberResponse response) {
		view.setEditable(response.isEditable());
		member = response.getMember();
		view.setName(member.summerize().getDisplayText());
		view.setWayOfLifeSummary(member.getWayOfLifeSummary());
		view.setHelpOffers(member.getHelpOffers());
		view.setHelpRequests(member.getHelpRequests());
	}

	@Override
	public void setWayOfLifeStatement(String updatedHtml) {
		member.setWayOfLifeSummary(updatedHtml);
		clientFactory.getInvoker().send(new UpdateMember(member));
	}

	@Override
	public void updateHelpOffer(Long id, String updatedHtml) {
		clientFactory.getInvoker().send(
				new UpdateMemberHelpOffer(id, updatedHtml));
	}

	@Override
	public void deleteHelpOffer(Long id) {
		clientFactory.getInvoker().send(new DeleteMemberHelpOffer(id));
	}

	@Override
	public void updateHelpRequest(Long id, String updatedHtml) {
		clientFactory.getInvoker().send(
				new UpdateMemberHelpRequest(id, updatedHtml));
	}

	@Override
	public void deleteHelpRequest(Long id) {
		clientFactory.getInvoker().send(new DeleteMemberHelpRequest(id));
	}

	@Override
	public void createHelpRequest(String updatedHtml) {
		clientFactory.getInvoker().send(
				new CreateMemberHelpRequest(member.summerize(), updatedHtml));
	}

	@Override
	public void createHelpOffer(String updatedHtml) {
		clientFactory.getInvoker().send(
				new CreateMemberHelpOffer(member.summerize(), updatedHtml));
	}

}
