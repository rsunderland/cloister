package com.earthabbey.cloister.client.mvp;

import com.earthabbey.cloister.client.activity.MemberActionActivity;
import com.earthabbey.cloister.client.activity.MessageActionActivity;
import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class ActionPanelActivityMapper implements ActivityMapper {
	
	private ClientFactory clientFactory;

	public ActionPanelActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;		
	}

	@Override
	public Activity getActivity(Place place) {		
		if (place instanceof MemberPlace) {
			return new MemberActionActivity((MemberPlace) place, clientFactory);
		} else if (place instanceof MessagePlace) {
			return new MessageActionActivity((MessagePlace) place, clientFactory);
		} else {
			return null;
		}
	}
}
