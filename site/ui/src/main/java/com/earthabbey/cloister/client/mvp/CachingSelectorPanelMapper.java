package com.earthabbey.cloister.client.mvp;

import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.place.MemberSelectorPlace;
import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.place.MessageSelectorPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.activity.shared.CachingActivityMapper;
import com.google.gwt.activity.shared.FilteredActivityMapper;
import com.google.gwt.place.shared.Place;

public class CachingSelectorPanelMapper implements ActivityMapper,
		FilteredActivityMapper.Filter {

	private ActivityMapper cachingMapper;

	public CachingSelectorPanelMapper(ClientFactory clientFactory) {
		cachingMapper = new CachingActivityMapper(new FilteredActivityMapper(
				this, new SelectorPanelMapper(clientFactory)));
	}

	@Override
	public Place filter(Place place) {
		if (place instanceof MessagePlace) {
			return new MessageSelectorPlace(((MessagePlace) place).getShowSentMessages());
		} else if (place instanceof MemberPlace) {
			return new MemberSelectorPlace();
		}
		return place;
	}

	@Override
	public Activity getActivity(Place place) {
		return cachingMapper.getActivity(place);
	}

}
