package com.earthabbey.cloister.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

@Prefix("confirm")
public class ConfirmEmailPlace extends Place {
	
	private String confirmationToken;
	
	public ConfirmEmailPlace(String token) {
		this.confirmationToken = token;
	}

	public String getConfirmationToken() {
		return confirmationToken;
	}
	
	public static class Tokenizer implements PlaceTokenizer<ConfirmEmailPlace> {
		@Override
		public String getToken(ConfirmEmailPlace place) {
			return place.confirmationToken;
		}

		@Override
		public ConfirmEmailPlace getPlace(String token) {
			return new ConfirmEmailPlace(token);
		}
	}
}