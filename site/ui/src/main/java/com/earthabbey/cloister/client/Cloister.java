package com.earthabbey.cloister.client;

import com.earthabbey.cloister.client.mvp.ActionPanelActivityMapper;
import com.earthabbey.cloister.client.mvp.AppPlaceHistoryMapper;
import com.earthabbey.cloister.client.mvp.CachingSelectorPanelMapper;
import com.earthabbey.cloister.client.mvp.MainPanelActivityMapper;
import com.earthabbey.cloister.client.place.ConfirmEmailPlace;
import com.earthabbey.cloister.client.place.LoginPlace;
import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.ui.api.AppLayout;
import com.earthabbey.cloister.client.ui.impl.ClientFactoryImpl;
import com.earthabbey.cloister.spi.commands.ConfirmEmail;
import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceChangeEvent;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Cloister implements EntryPoint {

	private Place defaultPlace = new LoginPlace();

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		ClientFactoryImpl clientFactory = new ClientFactoryImpl();
		EventBus eventBus = clientFactory.getEventBus();

		PlaceController placeController = clientFactory.getPlaceController();
		AppLayout layout = clientFactory.getAppLayout();

		addMainPanelActivity(clientFactory, eventBus);
		addActionPanelActivity(clientFactory, eventBus);
		addSelectorActivity(clientFactory, eventBus);

		eventBus.addHandler(PlaceChangeEvent.TYPE,
				new HandlePlaceLayout(layout));

		AppPlaceHistoryMapper historyMapper = GWT
				.create(AppPlaceHistoryMapper.class);

		PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(
				historyMapper);

		CloisterLayoutPresenter layoutPresenter = new CloisterLayoutPresenter(
				clientFactory);

		Place firstPlace = defaultPlace;
		
		String emailConfirmationToken = Window.Location.getParameter("ec");
		if (emailConfirmationToken != null) {
			firstPlace  = new ConfirmEmailPlace(emailConfirmationToken);
		}
		historyHandler.register(placeController, eventBus, firstPlace);

		RootLayoutPanel.get().add(layoutPresenter.getView());

	
		new ApplicationController(clientFactory, this);
		
		historyHandler.handleCurrentHistory();

	}

	private void addSelectorActivity(ClientFactoryImpl clientFactory,
			EventBus eventBus) {
		ActivityMapper mapper = new CachingSelectorPanelMapper(clientFactory);
		ActivityManager manager = new ActivityManager(mapper, eventBus);
		manager.setDisplay(clientFactory.getAppLayout().getSelectorContainer());
	}

	private void addActionPanelActivity(ClientFactoryImpl clientFactory,
			EventBus eventBus) {
		ActivityMapper mapper = new ActionPanelActivityMapper(clientFactory);
		ActivityManager manager = new ActivityManager(mapper, eventBus);
		manager.setDisplay(clientFactory.getAppLayout().getActionContainer());
	}

	private void addMainPanelActivity(ClientFactoryImpl clientFactory,
			EventBus eventBus) {
		ActivityMapper mapper = new MainPanelActivityMapper(clientFactory);
		ActivityManager manager = new ActivityManager(mapper, eventBus);
		manager.setDisplay(clientFactory.getAppLayout().getMainContainer());
	}

	private static class HandlePlaceLayout implements PlaceChangeEvent.Handler {
		private final AppLayout appLayout;

		public HandlePlaceLayout(AppLayout appLayout) {
			this.appLayout = appLayout;
		}

		@Override
		public void onPlaceChange(PlaceChangeEvent event) {
			Place newPlace = event.getNewPlace();
			if (newPlace instanceof MessagePlace) {
				appLayout.setMessageLayout();
			} else if (newPlace instanceof MemberPlace) {
				appLayout.setMembersLayout();
			} else {
				appLayout.setNoSideSelectorLayout();
			}
		}
	}
}
