package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.client.ui.api.ConfirmEmailView;
import com.google.gwt.core.client.GWT;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class ConfirmEmailViewImpl extends Composite implements ConfirmEmailView {

	private static ConfirmEmailViewImplUiBinder uiBinder = GWT
			.create(ConfirmEmailViewImplUiBinder.class);

	interface ConfirmEmailViewImplUiBinder extends
			UiBinder<Widget, ConfirmEmailViewImpl> {
	}
	
	@UiField
	HTMLPanel successPanel;
	
	@UiField
	HTMLPanel checkingPanel;
	
	@UiField
	Label memberName;
	
//	@UiField
//	Label mentorName;
//	
//	@UiField
//	Anchor loginAnchor;

	private Presenter presenter;


	public ConfirmEmailViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setMemberName(String name) {
		this.memberName.setText(name);
	}

	@Override
	public void showSuccess(boolean success) {
		checkingPanel.setVisible(false);
		successPanel.setVisible(true);
	}

	@Override
	public void showChecking() {
		checkingPanel.setVisible(true);
		successPanel.setVisible(false);
	}

	@Override
	public void setMentorName(String name) {
//		mentorName.setText(name);
	}

	@Override
	public void setSiteUrl(String url) {
//		loginAnchor.setHref(url);
//		loginAnchor.setText(url);
	}
}
