package com.earthabbey.cloister.client;

import com.earthabbey.cloister.client.compose.ComposeMessageEvent;
import com.earthabbey.cloister.client.place.LoginPlace;
import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.impl.ComposeMessageDialog;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.handlers.LoginResponseHandler;
import com.earthabbey.cloister.spi.handlers.LogoutResponseHandler;
import com.earthabbey.cloister.spi.handlers.ServiceErrorResponseHandler;
import com.earthabbey.cloister.spi.responses.LoginResponse;
import com.earthabbey.cloister.spi.responses.LogoutResponse;
import com.earthabbey.cloister.spi.responses.SendMessageResponse;
import com.earthabbey.cloister.spi.responses.ServiceErrorResponse;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;

public class ApplicationController implements LoginResponseHandler,
		LogoutResponseHandler, ServiceErrorResponseHandler {

	private ActionServiceInvoker invoker;
	private EventBus eventBus;
	private Cloister cloister;
	private ClientFactory clientFactory;

	public ApplicationController(ClientFactory clientFactory, Cloister cloister) {
		this.invoker = clientFactory.getInvoker();
		this.clientFactory = clientFactory;
		this.eventBus = clientFactory.getEventBus();
		this.cloister = cloister;
		addHandlers();
	}

	private void addHandlers() {
		eventBus.addHandler(LoginResponse.TYPE, this);
		eventBus.addHandler(LogoutResponse.TYPE, this);

		ComposeMessageDialog composeMessageDialog = new ComposeMessageDialog(
				invoker);
		eventBus.addHandler(ComposeMessageEvent.TYPE, composeMessageDialog);
		eventBus.addHandler(SendMessageResponse.TYPE, composeMessageDialog);
		eventBus.addHandler(ServiceErrorResponse.TYPE, this);
	}

	@Override
	public void handleLoginResponse(LoginResponse loginResponse) {
		if (loginResponse.getSession() == null) {
			System.out.println("Invalid password.");
		} else {
			invoker.setSessionHandle(loginResponse.getSession());			
			MemberSummary member = loginResponse.getSession().getMember();
			if (member == null) {
				clientFactory.getPlaceController().goTo(new MemberPlace());
			} else {
				clientFactory.getPlaceController()
						.goTo(new MemberPlace(member));
			}

		}
	}

	@Override
	public void handleLogoutResponse(LogoutResponse loginResponse) {
		invoker.setSessionHandle(null);		
		clientFactory.getPlaceController().goTo(new LoginPlace());
	}

	@Override
	public void handleServiceError(ServiceErrorResponse serviceErrorResponse) {
		StringBuilder bld = new StringBuilder();
		bld.append("We are sorry, but an unexpected error has occured.\n");
		bld.append("For further help please contact EarthAbbey directly.");
		bld.append("The details (which may help diagnose problem) are as follows:");
		bld.append(serviceErrorResponse.getDetails());
		Window.alert(bld.toString());
	}

}
