package com.earthabbey.cloister.client.ui.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface LoginView extends IsWidget {
	
	void setPresenter(Presenter presenter);
	void displayInvalidLogin(boolean isInvalid);
	
	void clearPassword();
	
	public interface Presenter {
		void attemptLogin(String emailAdderss, String password);
		void signUp();
	}



}
