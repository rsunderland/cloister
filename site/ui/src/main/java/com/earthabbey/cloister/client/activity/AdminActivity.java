package com.earthabbey.cloister.client.activity;

import com.earthabbey.cloister.client.place.AdminPlace;
import com.earthabbey.cloister.client.ui.api.AdminView;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.AdminView.Presenter;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Administrative Activity, as yet unused.
 * @author rich
 */
public class AdminActivity extends AbstractActivity implements
		AdminView.Presenter {

	private ClientFactory clientFactory;	
	private AdminView view;

	public AdminActivity(AdminPlace AdminPlace, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {		
		view = clientFactory.getAdminView();
		view.setPresenter(this);
		container.setWidget(view);		
	}

}
