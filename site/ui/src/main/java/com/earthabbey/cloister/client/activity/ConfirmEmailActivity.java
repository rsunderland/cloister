package com.earthabbey.cloister.client.activity;

import com.earthabbey.cloister.client.place.ConfirmEmailPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.ConfirmEmailView;
import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.commands.ConfirmEmail;
import com.earthabbey.cloister.spi.handlers.ConfirmEmailResponseHandler;
import com.earthabbey.cloister.spi.responses.ConfirmEmailResponse;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Member Action Activity manages display and selection of member related
 * actions.
 * 
 * @author rich
 */
public class ConfirmEmailActivity extends AbstractActivity implements
		ConfirmEmailView.Presenter, ConfirmEmailResponseHandler {

	private ClientFactory clientFactory;
	private ConfirmEmailPlace place;
	private ConfirmEmailView view;

	public ConfirmEmailActivity(ConfirmEmailPlace place,
			ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.place = place;
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {
		view = clientFactory.getConfirmEmailView();
		view.setPresenter(this);
		view.showChecking();
		container.setWidget(view);
		
		eventBus.addHandler(ConfirmEmailResponse.TYPE, this);
		
		clientFactory.getInvoker().send(
				new ConfirmEmail(place.getConfirmationToken()));
	}

	@Override
	public void handleConfirmEmailResponse(ConfirmEmailResponse response) {

		Member member = response.getMember();
		if (member != null) {
			view.setMemberName(member.getFirstName() + " "+ member.getSurname());
			
			MemberSummary mentor = member.getMentor();
			if(mentor != null)
			{
				view.setMentorName(mentor.getDisplayText());
			}
		}
				
		view.setSiteUrl(GWT.getHostPageBaseURL());
		view.showSuccess(response.isSuccess());
	}
}
