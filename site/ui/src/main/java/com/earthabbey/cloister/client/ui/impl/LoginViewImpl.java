package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.client.ui.api.LoginView;
import com.earthabbey.cloister.spi.handlers.LoginResponseHandler;
import com.earthabbey.cloister.spi.responses.LoginResponse;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class LoginViewImpl extends Composite implements LoginView{

	private static LoginViewUiBinder uiBinder = GWT
			.create(LoginViewUiBinder.class);

	interface LoginViewUiBinder extends UiBinder<Widget, LoginViewImpl> {
	}


	@UiField
	HTMLPanel invalidLogin;

	@UiField
	CloisterButton apply;

	@UiField
	CloisterButton login;

	@UiField
	TextBox email;

	@UiField
	PasswordTextBox password;

	private Presenter presenter;

	public LoginViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));		
		
	}

	@UiHandler("login")
	void onLogin(ClickEvent e) {
		
		presenter.attemptLogin(email.getText(), password.getText());		
	}

	@UiHandler("apply")
	void onApply(ClickEvent e) {		
		presenter.signUp();
		
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void displayInvalidLogin(boolean isInvalid) {
		invalidLogin.setVisible(isInvalid);
	}

	@Override
	public void clearPassword() {
		password.setText("");
	}
}
