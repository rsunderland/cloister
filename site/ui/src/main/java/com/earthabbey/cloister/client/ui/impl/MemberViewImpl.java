package com.earthabbey.cloister.client.ui.impl;

import java.util.List;

import com.earthabbey.cloister.client.ui.api.MemberView;
import com.earthabbey.cloister.client.ui.api.TextEditor;
import com.earthabbey.cloister.client.ui.api.TextEditor.Mode;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class MemberViewImpl extends Composite implements MemberView {

	private static MemberViewImplUiBinder uiBinder = GWT
			.create(MemberViewImplUiBinder.class);

	interface MemberViewImplUiBinder extends UiBinder<Widget, MemberViewImpl> {
	}

	public static final CommonResources base = GWT
			.create(CommonResources.class);

	@UiField
	Label name;

	@UiField
	TextEditorImpl wayOfLifeSummary;

	@UiField(provided = true)
	Grid helpOffers;

	@UiField(provided = true)
	Grid helpRequests;

	@UiField
	CloisterButton addRequest;

	@UiField
	HTMLPanel noRequests;

	@UiField
	TextEditorImpl newRequestEditor;

	@UiField
	CloisterButton addOffer;

	@UiField
	HTMLPanel noOffers;

	@UiField
	TextEditorImpl newOfferEditor;


	private Presenter presenter;

	private boolean editable;

	static
	{
		base.style().ensureInjected();
	}
	
	public MemberViewImpl() {
		
		helpOffers = new Grid(0, 1);
		helpRequests = new Grid(0, 1);
		initWidget(uiBinder.createAndBindUi(this));

		wayOfLifeSummary.setPresenter(new WayOfLifePresenter());
		newOfferEditor.setPresenter(new CreateHelpOfferPresenter());
		newRequestEditor.setPresenter(new CreateHelpRequestPresenter());

		newOfferEditor.setEditable(true);
		newRequestEditor.setEditable(true);

	

	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setName(String name) {
		this.name.setText(name);
	}

	@Override
	public void setWayOfLifeSummary(String wayOfLifeSummaryHtml) {
		wayOfLifeSummary.setHTML(wayOfLifeSummaryHtml);
	}

	@Override
	public void setEditable(boolean editable) {
		this.editable = editable;
		wayOfLifeSummary.setEditable(editable);
	}



	@Override
	public void setHelpOffers(List<MemberHelpOffer> offers) {
		helpOffers.resizeRows(offers.size());
		int row = 0;
		for (MemberHelpOffer offer : offers) {
			TextEditorImpl editor = new TextEditorImpl();
			editor.setDeletable(true);
			editor.setHTML(offer.getText());
			editor.setEditable(editable);
			editor.setPresenter(new UpdateHelpOfferPresenter(offer.getId()));
			helpOffers.setWidget(row++, 0, editor);
		}
		addOffer.setVisible(editable);
		addOffer.setEnabled(editable);
		noOffers.setVisible(offers.isEmpty() && !editable);
	}

	@Override
	public void setHelpRequests(List<MemberHelpRequest> requests) {
		helpRequests.resizeRows(requests.size());
		int row = 0;
		for (MemberHelpRequest request : requests) {
			TextEditorImpl editor = new TextEditorImpl();
			editor.setDeletable(true);
			editor.setHTML(request.getText());
			editor.setEditable(editable);
			editor.setPresenter(new UpdateHelpRequestPresenter(request.getId()));
			helpRequests.setWidget(row++, 0, editor);
		}

		addRequest.setVisible(editable);
		addRequest.setEnabled(editable);
		noRequests.setVisible(requests.isEmpty() && !editable);
	}

	@UiHandler("addRequest")
	public void onAddRequest(ClickEvent e) {
		newRequestEditor.setMode(Mode.EDIT);
		newRequestEditor.setVisible(true);
		addRequest.setVisible(false);
	}

	@UiHandler("addOffer")
	public void onAddOffer(ClickEvent e) {
		newOfferEditor.setMode(Mode.EDIT);
		newOfferEditor.setVisible(true);
		addOffer.setVisible(false);
	}

	private class WayOfLifePresenter implements TextEditor.Presenter {
		@Override
		public void save(String updatedHtml) {
			presenter.setWayOfLifeStatement(updatedHtml);
		}

		@Override
		public void delete() {
			// does nothing.
		}

		@Override
		public void cancel() {
			// nothing to do.
		}
	}

	private class UpdateHelpOfferPresenter implements TextEditor.Presenter {

		private Long id;

		public UpdateHelpOfferPresenter(Long id) {
			this.id = id;
		}

		@Override
		public void save(String updatedHtml) {
			presenter.updateHelpOffer(id, updatedHtml);
		}

		@Override
		public void delete() {
			presenter.deleteHelpOffer(id);
		}

		@Override
		public void cancel() {
			// nothing to do.
		}

	}

	private class UpdateHelpRequestPresenter implements TextEditor.Presenter {

		private Long id;

		public UpdateHelpRequestPresenter(Long id) {
			this.id = id;
		}

		@Override
		public void save(String updatedHtml) {
			presenter.updateHelpRequest(id, updatedHtml);
		}

		@Override
		public void delete() {
			presenter.deleteHelpRequest(id);
		}

		@Override
		public void cancel() {
			// nothing to do.
		}

	}

	private class CreateHelpRequestPresenter implements TextEditor.Presenter {

		@Override
		public void save(String updatedHtml) {
			closeEditor();
			presenter.createHelpRequest(updatedHtml);
		}

		private void closeEditor() {
			newRequestEditor.setHTML("");
			newRequestEditor.setVisible(false);
			addRequest.setVisible(editable);
		}

		@Override
		public void delete() {
			// nothing to do
		}

		@Override
		public void cancel() {
			closeEditor();
		}
	}

	private class CreateHelpOfferPresenter implements TextEditor.Presenter {

		@Override
		public void save(String updatedHtml) {
			closeEditor();
			presenter.createHelpOffer(updatedHtml);
		}

		private void closeEditor() {
			newOfferEditor.setHTML("");
			newOfferEditor.setVisible(false);
			addOffer.setVisible(editable);
		}

		@Override
		public void delete() {
			// nothing to do
		}

		@Override
		public void cancel() {
			closeEditor();
		}
	}



}
