package com.earthabbey.cloister.client.ui.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface MessageActionView extends IsWidget {
		
	void enableReply(boolean enable);
	void setPresenter(Presenter presenter);
	
	public interface Presenter
	{
		void reply();
		void delete();
		void showSentMessage(boolean show);
	}
}

