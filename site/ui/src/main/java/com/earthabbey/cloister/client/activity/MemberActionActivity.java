package com.earthabbey.cloister.client.activity;

import com.earthabbey.cloister.client.compose.ComposeMessageEvent;
import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.MemberActionView;
import com.earthabbey.cloister.client.ui.impl.CommonResources;
import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.spi.commands.ApproveApplicant;
import com.earthabbey.cloister.spi.handlers.RefreshMemberHandler;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Member Action Activity manages display and selection of member related
 * actions.
 * 
 * @author rich
 */
public class MemberActionActivity extends AbstractActivity implements
		MemberActionView.Presenter, RefreshMemberHandler {

	public static final CommonResources base = GWT
			.create(CommonResources.class);

	static {
		base.style().ensureInjected();
	}

	private ClientFactory clientFactory;
	private MemberPlace place;
	private MemberActionView view;
	private Member member;
	private EventBus eventBus;

	public MemberActionActivity(MemberPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.place = place;
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {
		view = clientFactory.getMemberActionView();
		view.setPresenter(this);
		this.eventBus = eventBus;
		container.setWidget(view);
		eventBus.addHandler(RefreshMemberResponse.TYPE, this);
	}

	@Override
	public void refreshMember(RefreshMemberResponse response) {

		member = response.getMember();

		view.enableApproveApplicant(member.getType() == MemberType.APPLICANT
				&& clientFactory.getInvoker().can(Permission.APPROVE_APPLICANT));

		view.enableSendMessage(!clientFactory.getInvoker().isSelf(member));

	}

	@Override
	public void sendMessage() {
		eventBus.fireEvent(new ComposeMessageEvent(clientFactory.getInvoker()
				.getSelf(), member.summerize()));
	}

	@Override
	public void approveApplicant() {
		clientFactory.getInvoker().send(
				new ApproveApplicant(member.summerize()));
	}

}
