package com.earthabbey.cloister.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class SignUpPlace extends Place {

	@Prefix("signup")
	public static class Tokenizer implements PlaceTokenizer<SignUpPlace> {
		@Override
		public String getToken(SignUpPlace place) {
			return "";
		}

		@Override
		public SignUpPlace getPlace(String token) {
			return new SignUpPlace();
		}
	}
}