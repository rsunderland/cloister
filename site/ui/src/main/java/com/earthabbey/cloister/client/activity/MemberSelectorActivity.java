package com.earthabbey.cloister.client.activity;

import java.util.ArrayList;

import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.place.MemberSelectorPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.MemberSelectorView;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.commands.GetMembers;
import com.earthabbey.cloister.spi.handlers.GetMembersResponseHandler;
import com.earthabbey.cloister.spi.handlers.LogoutResponseHandler;
import com.earthabbey.cloister.spi.handlers.RefreshMemberHandler;
import com.earthabbey.cloister.spi.responses.GetMembersResponse;
import com.earthabbey.cloister.spi.responses.LogoutResponse;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Message Selector Activity responsible for displaying visible members and
 * allows to user to select which to view.
 * @author rich
 */
public class MemberSelectorActivity extends AbstractActivity implements
		MemberSelectorView.Presenter, GetMembersResponseHandler,
		RefreshMemberHandler, LogoutResponseHandler {
	
	
	private ClientFactory clientFactory;
	private MemberSelectorView view;

	public MemberSelectorActivity(MemberSelectorPlace place,
			ClientFactory clientFactory) {
		this.clientFactory = clientFactory;	
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {
		view = clientFactory.getMemberSelectorView();
		view.setPresenter(this);
		container.setWidget(view);
		triggerListRefresh();
		eventBus.addHandler(GetMembersResponse.TYPE, this);
		eventBus.addHandler(RefreshMemberResponse.TYPE, this);
		eventBus.addHandler(LogoutResponse.TYPE, this);
	}

	@Override
	public void select(MemberSummary summary) {
		clientFactory.getPlaceController().goTo(new MemberPlace(summary));
	}

	@Override
	public void handleGetMessageResponse(GetMembersResponse response) {
		view.setMemberSummaries(response.getMemberSummaries());
	}

	@Override
	public void refreshMember(RefreshMemberResponse response) {
		if (response.isSummaryChanged()) {
			triggerListRefresh();
		}
	}

	private void triggerListRefresh() {
		clientFactory.getInvoker().send(new GetMembers());
	}

	@Override
	public void handleLogoutResponse(LogoutResponse loginResponse) {
		view.setMemberSummaries(new ArrayList<MemberSummary>());
	}

}
