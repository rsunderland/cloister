package com.earthabbey.cloister.client.ui.impl;

import java.util.Arrays;

import com.earthabbey.cloister.client.ActionServiceInvoker;
import com.earthabbey.cloister.client.compose.ComposeMessageEvent;
import com.earthabbey.cloister.client.compose.ComposeMessageEventHandler;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.spi.commands.SendMessage;
import com.earthabbey.cloister.spi.handlers.SendMessageResponseHandler;
import com.earthabbey.cloister.spi.responses.SendMessageResponse;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.TextBox;

public class ComposeMessageDialog implements ComposeMessageEventHandler,
		SendMessageResponseHandler {

	private DialogBox dialogBox;

	private ActionServiceInvoker invoker;

	@UiField
	Label senderName;

	@UiField
	Label recipientName;

	@UiField
	CloisterButton sendButton;

	@UiField
	CloisterButton cancelButton;

	@UiField
	TextBox title;

	@UiField
	RichTextArea content;

	private MemberSummary sender;

	private MemberSummary recipient;

	interface ComposeUiBinder extends UiBinder<DialogBox, ComposeMessageDialog> {
	};

	private final static ComposeUiBinder uiBinder = GWT
			.create(ComposeUiBinder.class);

	public ComposeMessageDialog(ActionServiceInvoker invoker) {
		this.invoker = invoker;
		dialogBox = uiBinder.createAndBindUi(this);
		dialogBox.setPopupPosition(100, 100);
	}

	@Override
	public void handleComposeMessageEvent(
			ComposeMessageEvent composeMessageEvent) {
		this.sender = composeMessageEvent.getSender();
		this.recipient = composeMessageEvent.getRecipient();
		title.setText("");
		senderName.setText(sender.getDisplayText());
		recipientName.setText(recipient.getDisplayText());
		content.setText("");
		setEnabled(true);
		GWT.log("Showing compose message box");
		dialogBox.show();
	}

	private void setEnabled(boolean enabled) {
		title.setEnabled(enabled);
		content.setEnabled(enabled);
		sendButton.setEnabled(enabled);
		cancelButton.setEnabled(enabled);
	}

	@Override
	public void handleSendMessageResponse(SendMessageResponse loginResponse) {
		dialogBox.hide();
	}
	
	@UiHandler("sendButton")
	public void clickSend(ClickEvent e)
	{
		GWT.log("Send button pressed");
		setEnabled(false);
		invoker.send(new SendMessage(sender, Arrays.asList(recipient),
				title.getText(), content.getHTML()));		
	}
	
	@UiHandler("cancelButton")
	public void clickCancel(ClickEvent e)
	{
		dialogBox.hide();
	}
};
