package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.client.ui.api.AdminView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class AdminViewImpl extends Composite implements AdminView {

	private static AdminViewUiBinder uiBinder = GWT
			.create(AdminViewUiBinder.class);

	interface AdminViewUiBinder extends UiBinder<Widget, AdminViewImpl> {
	}

	private Presenter presenter;


	public AdminViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		
	//	restoreForm.setAction(GWT.getModuleBaseURL()+"fileupload");

	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
//
//	@UiHandler("restore")
//	public void onRestore(ClickEvent e) {
//		String backup = fileUpload.getFilename();
//
//		boolean selectedOkay = Window
//				.confirm("Are you sure< you want to restore '"
//						+ backup
//						+ "'. This will erase all existing site content.");
//
//		if (selectedOkay) {
//			restoreForm.submit();
//		}
//	}

}
