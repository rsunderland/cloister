package com.earthabbey.cloister.client.datahygiene;

public class CleanEmail extends AbstractCleanValue<String> {

	private final String EMAIL_REGEX = "^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";

	public CleanEmail(String emailText, CleanContext context) {
		super(context);
		if (emailText == null || emailText.trim().length() == 0) {
			addIssue("Email address is empty.");
		} else if (emailText.trim().matches(EMAIL_REGEX)) {
			setCleanValue(emailText.trim());
		} else {
			addIssue("Email address had a unsupported format.");
		}
	}

}
