package com.earthabbey.cloister.client.ui.impl;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

public interface CloisterButtonResources extends ClientBundle {

	@Source("cloister-button.css")
	CloisterBaseStyle style();

	@ImageOptions(repeatStyle = RepeatStyle.None)
	@Source("com/earthabbey/two_box_back.gif")
	ImageResource backgroundImage();

	public interface CloisterBaseStyle extends CssResource {
		String button();
		String text();
	}
}
