package com.earthabbey.cloister.client.place;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

public class MemberPlace extends Place {

	private Long id;
	
	public MemberPlace(MemberSummary member) {
		this.id = member.getId();
	}

	protected MemberPlace(String token) {		
		String parts[] = token.split(":");
		if (parts.length > 0) {
			String idPart = parts[0].trim();
			if (!idPart.isEmpty()) {
				id = Long.parseLong(idPart);
			}
		}	
	}

	public MemberPlace() {
		this.id = null;
	}

	public MemberPlace(Long id) {
		this.id = id;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	protected String getToken() {
		StringBuilder bld = new StringBuilder();
		bld.append(id);
		bld.append(":");
		return bld.toString();
	}

	public String toString() {
		return getToken();
	}

	public void populateNullValuesFrom(MemberPlace previousMemberPlace) {
		if (id == null) {
			id = previousMemberPlace.id;
		}

	}

	@Prefix("member")
	public static class Tokenizer implements PlaceTokenizer<MemberPlace> {
		@Override
		public MemberPlace getPlace(String token) {
			return new MemberPlace(token);
		}

		@Override
		public String getToken(MemberPlace place) {
			return place.getToken();
		}
	}

}
