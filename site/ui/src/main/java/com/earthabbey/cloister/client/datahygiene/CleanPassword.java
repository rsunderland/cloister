package com.earthabbey.cloister.client.datahygiene;

public class CleanPassword extends AbstractCleanValue<String> {

	public CleanPassword(String password, CleanContext context) {
		super(context);
		boolean passwordProvided = false;

		if (password == null || password.trim().length() == 0) {
			addIssue("Password empty.");

		} else {
			passwordProvided = true;
		}

		if (passwordProvided) {
			setCleanValue(password);
		}

	}

	public CleanPassword(String password, String confirmationPassword,
			CleanContext context) {
		super(context);
		boolean passwordProvided = false;

		if (password == null || password.trim().length() == 0) {
			addIssue("Password empty.");

		} else {
			passwordProvided = true;
		}

		boolean confirmationPasswordProvided = false;
		if (confirmationPassword == null
				|| confirmationPassword.trim().length() == 0) {
			addIssue("Confirmation empty.");
		} else {
			confirmationPasswordProvided = true;
		}

		if (passwordProvided && confirmationPasswordProvided) {
			password = password.trim();
			confirmationPassword = confirmationPassword.trim();

			if (password.equals(confirmationPassword)) {
				setCleanValue(password);
			} else {
				addIssue("Password and confirmation password do not match.");
			}
		}

	}

}
