package com.earthabbey.cloister.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;

/**
 * Message Place holds all the details required to display a message and the
 * message browser.
 */
public class MessagePlace extends Place {

	/**
	 * Identifier of message.
	 */
	private Long id;

	/**
	 * Whether the browser should show the sent messages or only the received
	 * messages.
	 */
	private Boolean showSentMessages;

	/**
	 * Constructor.
	 * 
	 * @param token
	 *            token to decompose.
	 */
	public MessagePlace(String token) {

		String parts[] = token.split(":");
		if (parts.length > 0) {
			String idPart = parts[0].trim();
			if (!idPart.isEmpty()) {
				id = Long.parseLong(idPart);
			}
		}

		if (parts.length > 1) {
			String filterPart = parts[1].trim();
			if (!filterPart.isEmpty()) {
				showSentMessages = Boolean.parseBoolean(filterPart);
			}
		}
	}

	/**
	 * Constructor. Show a message, but do not declare which one.
	 */
	public MessagePlace() {
		id = null;
	}

	/**
	 * Constructor. Show a particular message, but do not specify whether sent
	 * messages should be shown.
	 * 
	 * @param id
	 *            id of the messages to show.
	 */
	public MessagePlace(Long id) {
		this.id = id;
	}

	/**
	 * Constructor. Show an unspecified messages, but specify whether sent
	 * messages should be shown.
	 * 
	 * @param showSentMessages
	 *            Whether the browser should show the sent messages or only the
	 *            received messages.
	 */
	public MessagePlace(Boolean showSentMessages) {
		this.showSentMessages = showSentMessages;
	}

	/**
	 * Populate null values in this place using values from another place.
	 * 
	 * @param otherPlace
	 *            the otherPlace to take values from.
	 */
	public void populateNullValuesFrom(MessagePlace otherPlace) {
		if (id == null) {
			id = otherPlace.id;
		}

		if (showSentMessages == null) {
			showSentMessages = otherPlace.showSentMessages;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Get whether the browser should show the sent messages or only the
	 * received messages.
	 * 
	 * @return flag (if flag is not set a default value of false is returned).
	 */
	public boolean getShowSentMessages() {
		return showSentMessages != null && showSentMessages.booleanValue();
	}

	/**
	 * Set whether the browser should show the sent messages or only the
	 * received messages.
	 * 
	 * @param showSentMessages
	 *            whether sent messages should be shown.
	 */
	public void setShowSentMessages(boolean showSentMessages) {
		this.showSentMessages = showSentMessages;
	}

	/**
	 * Convert place into string token.
	 * 
	 * @return string token containing state of place.
	 */
	public String getToken() {
		StringBuilder bld = new StringBuilder();
		if (id != null) {
			bld.append(id);
		}
		bld.append(":");
		bld.append(getShowSentMessages());
		return bld.toString();
	}

	/**
	 * Get object as token.
	 * 
	 * @return return same result as {@link #getToken()}.
	 */
	public String toString() {
		return getToken();
	}

	/**
	 * Tokenizer.
	 */
	@Prefix("message")
	public static class Tokenizer implements PlaceTokenizer<MessagePlace> {
		@Override
		public MessagePlace getPlace(String token) {
			return new MessagePlace(token);
		}

		@Override
		public String getToken(MessagePlace arg0) {
			return arg0.getToken();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((showSentMessages == null) ? 0 : showSentMessages.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessagePlace other = (MessagePlace) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (showSentMessages == null) {
			if (other.showSentMessages != null)
				return false;
		} else if (!showSentMessages.equals(other.showSentMessages))
			return false;
		return true;
	}
}
