package com.earthabbey.cloister.client.ui.impl;

import java.util.Date;
import java.util.List;

import com.earthabbey.cloister.client.ui.api.MessageView;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.MessageSummary;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

public class MessageViewImpl extends Composite implements MessageView {

	private static MessageViewImplUiBinder uiBinder = GWT
			.create(MessageViewImplUiBinder.class);

	interface MessageViewImplUiBinder extends UiBinder<Widget, MessageViewImpl> {
	}

	public static final CommonResources base = GWT
			.create(CommonResources.class);

	private Presenter presenter;



	@UiField
	Label title;

	@UiField
	Label sender;

	@UiField
	Label date;

	@UiField
	HTML content;

	static {
		base.style().ensureInjected();
	}

	public MessageViewImpl() {
		MessageSummaryCell cell = new MessageSummaryCell();
		initWidget(uiBinder.createAndBindUi(this));

		final SingleSelectionModel<MessageSummary> selectionModel = new SingleSelectionModel<MessageSummary>();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setSender(MemberSummary summary) {
		sender.setText(summary.getDisplayText());
	}

	@Override
	public void setTitle(String titleText) {
		title.setText(titleText);
	}

	@Override
	public void setDate(Date sendTime) {
		String timeText = null;
		if (sendTime != null) {
			timeText = DateTimeFormat.getFullDateFormat().format(sendTime);
		}
		date.setText(timeText);
	}

	@Override
	public void setContent(String html) {
		content.setHTML(html);
	}
}
