package com.earthabbey.cloister.client.place;

import com.google.gwt.place.shared.Place;

/**
 * Message Selector Place controls how the message selector panel appears.
 * 
 * It is contains a subset of the information contained in a MessagePlace.
 * 
 */
public class MessageSelectorPlace extends Place {

	/**
	 * Whether the browser should show the sent messages or only the received
	 * messages.
	 */
	private final boolean showSentMessages;

	public MessageSelectorPlace(boolean showSentMessages) {
		this.showSentMessages = showSentMessages;
	}

	public boolean isShowSentMessages() {
		return showSentMessages;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (showSentMessages ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessageSelectorPlace other = (MessageSelectorPlace) obj;
		if (showSentMessages != other.showSentMessages)
			return false;
		return true;
	}
}
