package com.earthabbey.cloister.client;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.spi.CloisterServiceAsync;
import com.earthabbey.cloister.spi.Command;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class ActionServiceInvoker {

	private CloisterServiceAsync service;
	private EventBus responseHandler;
	private Session session;

	public ActionServiceInvoker(CloisterServiceAsync service,
			EventBus eventBus) {
		this.service = service;
		this.responseHandler = eventBus;
	}

	public void setSessionHandle(Session sessionHandle) {
		this.session = sessionHandle;
	}

	public void send(Command request) {
		service.perform(request, session, new EventCallback());
	}

	public boolean can(Permission permission) {
		boolean hasPermission = false;
		if (session != null) {
			hasPermission = session.getPermissions().contains(permission);
		}
		return hasPermission;
	}
	

	public MemberSummary getSelf() {
		MemberSummary self = null;
		if(session != null && session.getMember() != null)
		{
			self = session.getMember();
		}
		return self;
	}

	public boolean isSelf(Member member) {
		boolean isSelf = false;
		if (session != null && session.getMember() != null) {
			isSelf = (session.getMember().getId().equals(member.getId()));
		}
		return isSelf;
	}

	private class EventCallback implements AsyncCallback<GwtEvent<?>> {
		@Override
		public void onFailure(Throwable arg0) {
			arg0.printStackTrace();
		}

		@Override
		public void onSuccess(GwtEvent<?> event) {
			if (event == null) {
				throw new IllegalArgumentException(
						"Action returned no response.");
			}
			responseHandler.fireEvent(event);
		}
	}


}
