package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.client.ui.api.MessageActionView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MessageActionViewImpl extends Composite implements MessageActionView {

	private static MessageActionViewImplUiBinder uiBinder = GWT
			.create(MessageActionViewImplUiBinder.class);

	interface MessageActionViewImplUiBinder extends
			UiBinder<Widget, MessageActionViewImpl> {
	}
	
	@UiField
	CheckBox showSentMessages;

	@UiField
	CloisterButton reply;

	@UiField
	CloisterButton delete;

	private Presenter presenter;

	public MessageActionViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));		
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
	
	@UiHandler("reply")
	public void onReply(ClickEvent e) {
		presenter.reply();
	}

	@UiHandler("delete")
	public void onDelete(ClickEvent e) {
		presenter.delete();
	}
	
	@UiHandler("showSentMessages")
	public void onShowSentMessagesChange(ClickEvent e){
		presenter.showSentMessage(showSentMessages.getValue());
	}

	@Override
	public void enableReply(boolean enable) {
		reply.setEnabled(enable);
		reply.setVisible(enable);
	}
}
