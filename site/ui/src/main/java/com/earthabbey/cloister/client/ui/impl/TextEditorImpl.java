package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.client.ui.api.TextEditor;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.Widget;

public class TextEditorImpl extends Composite implements TextEditor {

	private static EditPanelUiBinder uiBinder = GWT
			.create(EditPanelUiBinder.class);

	interface EditPanelUiBinder extends UiBinder<Widget, TextEditorImpl> {
	}
	

	@UiField
	HTMLPanel displayView;

	@UiField
	HTMLPanel editView;

	@UiField
	HTML displayArea;

	@UiField
	HTMLPanel editControls;

	@UiField
	RichTextArea editArea;

	@UiField
	CloisterButton edit;

	@UiField
	CloisterButton delete;

	@UiField
	CloisterButton save;

	@UiField
	CloisterButton cancel;

	private Presenter presenter;

	public TextEditorImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void setHTML(String html) {
		editArea.setHTML(html);
		displayArea.setHTML(html);
	}

	@UiHandler("edit")
	public void onEdit(ClickEvent e) {
		setMode(Mode.EDIT);
	}

	@UiHandler("cancel")
	public void onCancel(ClickEvent e) {
		editArea.setHTML(displayArea.getHTML());
		presenter.cancel();
		setMode(Mode.DISPLAY);
	}

	@UiHandler("save")
	public void onSave(ClickEvent e) {
		String html = editArea.getHTML();
		displayArea.setHTML(html);
		setMode(Mode.DISPLAY);
		presenter.save(html);
	}

	@UiHandler("delete")
	public void onDelete(ClickEvent e) {
		presenter.delete();
	}

	public void setEditable(boolean editable) {
		editControls.setVisible(editable);
		edit.setEnabled(editable);
	}

	@Override
	public void setDeletable(boolean deletable) {
		delete.setEnabled(deletable);
		delete.setVisible(deletable);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setMode(Mode mode) {
		editView.setVisible(mode == Mode.EDIT);
		displayView.setVisible(mode == Mode.DISPLAY);
	}
}
