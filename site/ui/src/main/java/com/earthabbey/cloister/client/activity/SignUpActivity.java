package com.earthabbey.cloister.client.activity;

import com.earthabbey.cloister.client.datahygiene.CleanContext;
import com.earthabbey.cloister.client.datahygiene.CleanEmail;
import com.earthabbey.cloister.client.datahygiene.CleanPassword;
import com.earthabbey.cloister.client.datahygiene.CleanString;
import com.earthabbey.cloister.client.place.SignUpPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.SignUpView;
import com.earthabbey.cloister.client.ui.api.SignUpView.Mode;
import com.earthabbey.cloister.client.ui.api.SignUpView.Presenter;

import com.earthabbey.cloister.spi.commands.CreateAccount;
import com.earthabbey.cloister.spi.handlers.CreateAccountResponseHandler;
import com.earthabbey.cloister.spi.responses.CreateAccountResponse;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Sign Up Activity manages the collection and submission of account creation
 * details.
 * 
 * @author rich
 * 
 */
public class SignUpActivity extends AbstractActivity implements
		SignUpView.Presenter, CreateAccountResponseHandler {

	private ClientFactory clientFactory;
	private SignUpView view;

	public SignUpActivity(SignUpPlace signUpPlace, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {
		view = clientFactory.getSignUpView();
		view.setPresenter(this);
		container.setWidget(view);
		eventBus.addHandler(CreateAccountResponse.TYPE, this);
	}

	@Override
	public void attemptSignUp(String emailAddress, String firstName,
			String surname, String password, String confirmPassword) {
		CleanContext context = new CleanContext();
		CleanPassword cleanPassword = new CleanPassword(password,
				confirmPassword, context);

		CleanEmail cleanEmail = new CleanEmail(emailAddress, context);

		CleanString cleanFirstName = new CleanString("First name", firstName,
				context);

		CleanString cleanSurname = new CleanString("Surname", surname, context);

		if (context.isValid()) {
			view.setMode(Mode.PROCESSING);
			clientFactory.getInvoker().send(
					new CreateAccount(cleanEmail.getCleanValue(), //
							cleanFirstName.getCleanValue(), //
							cleanSurname.getCleanValue(), //
							cleanPassword.getCleanValue(), GWT
									.getHostPageBaseURL() + "?ec=")); //
		} else {
			view.displayErrors(context.getIssues());
		}
	}

	@Override
	public void handle(CreateAccountResponse createAccountResponse) {
		if (createAccountResponse.isSuccess()) {
			view.setMode(Mode.COMPLETION);
		} else {
			view.setMode(Mode.FORM);
			view.displayErrors(createAccountResponse.getFailureReasons());
		}
	}

}
