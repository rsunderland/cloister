package com.earthabbey.cloister.client.datahygiene;

public class CleanString extends AbstractCleanValue<String> {

	private static final String VALID_STRING = "[a-zA-Z ]*";

	public CleanString(String fieldName, String rawValue, CleanContext context) {
		super(context);
		if (rawValue == null || rawValue.trim().length() == 0) {
			addIssue(fieldName + " was empty.");
		} else if (rawValue.trim().matches(VALID_STRING)) {
			setCleanValue(rawValue.trim());
		} else {
			addIssue(fieldName
					+ " contained unsupported letters/characters.");
		}
	}

}
