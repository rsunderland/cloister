package com.earthabbey.cloister.client.activity;

import com.earthabbey.cloister.client.place.LoginPlace;
import com.earthabbey.cloister.client.place.SignUpPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.LoginView;
import com.earthabbey.cloister.client.ui.api.LoginView.Presenter;
import com.earthabbey.cloister.spi.commands.Login;
import com.earthabbey.cloister.spi.handlers.LoginResponseHandler;
import com.earthabbey.cloister.spi.responses.LoginResponse;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;


/**
 * Login Activity that handles the process of collecting
 * and submitting user login requests.
 * 
 * @author rich
 */
public class LoginActivity extends AbstractActivity implements
		LoginView.Presenter, LoginResponseHandler {

	private ClientFactory clientFactory;	
	private LoginView view;

	public LoginActivity(LoginPlace loginPlace, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget container, EventBus eventBus) {		
		view = clientFactory.getLoginView();
		view.setPresenter(this);
		container.setWidget(view);
		eventBus.addHandler(LoginResponse.TYPE, this);
	}

	@Override
	public void attemptLogin(String email, String password) {		
		clientFactory.getInvoker().send(new Login(email, password));
	}

	@Override
	public void signUp() {
		clientFactory.getPlaceController().goTo(new SignUpPlace());		
	}
	
	@Override
	public void handleLoginResponse(LoginResponse loginResponse) {
		view.clearPassword();
		view.displayInvalidLogin(loginResponse.getSession() == null);
	}

}
