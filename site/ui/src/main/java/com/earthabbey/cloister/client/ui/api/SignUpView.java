package com.earthabbey.cloister.client.ui.api;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;

public interface SignUpView extends IsWidget {

	public enum Mode {
		FORM, PROCESSING, COMPLETION
	}

	void setMode(Mode mode);

	void displayErrors(List<String> errors);

	void setPresenter(Presenter presenter);

	public interface Presenter {

		void attemptSignUp(String emailAddress, String firstName,
				String surname, String password, String confirmPassword);
	}

}
