package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.client.ui.api.MemberActionView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MemberActionViewImpl extends Composite implements MemberActionView {

	private static MemberActionViewImplUiBinder uiBinder = GWT
			.create(MemberActionViewImplUiBinder.class);

	interface MemberActionViewImplUiBinder extends
			UiBinder<Widget, MemberActionViewImpl> {
	}

	public MemberActionViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiField
	CloisterButton approveApplicant;

	@UiField
	CloisterButton sendMessage;

	private Presenter presenter;

	public MemberActionViewImpl(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));

	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void enableSendMessage(boolean enable) {
		sendMessage.setEnabled(enable);
		sendMessage.setVisible(enable);
	}

	@Override
	public void enableApproveApplicant(boolean enable) {
		approveApplicant.setEnabled(enable);
		approveApplicant.setVisible(enable);
	}

	@UiHandler("sendMessage")
	public void onSendMessage(ClickEvent e) {
		presenter.sendMessage();
	}

	@UiHandler("approveApplicant")
	public void onApproveApplicant(ClickEvent e) {
		presenter.approveApplicant();
	}

}
