package com.earthabbey.cloister.client.ui.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface CloisterLayout extends IsWidget {

	public void setPresenter(Presenter presenter);
		
	public interface Presenter
	{
		void logout();
		void administrate();
		void gotoMessages();
		void gotoMembers();	
	}

	void enableLogout(boolean enable);
	void enableAdmin(boolean enabled);
	void enableGotoButtons(boolean enabled);
}
