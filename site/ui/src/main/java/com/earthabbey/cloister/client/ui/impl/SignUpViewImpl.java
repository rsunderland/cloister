package com.earthabbey.cloister.client.ui.impl;

import java.util.List;

import com.earthabbey.cloister.client.ui.api.SignUpView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class SignUpViewImpl extends Composite implements SignUpView {

	private static final SignUpViewUiBinder uiBinder = GWT
			.create(SignUpViewUiBinder.class);

	interface SignUpViewUiBinder extends UiBinder<Widget, SignUpViewImpl> {
	}

	@UiField
	HTMLPanel form;

	@UiField
	TextBox emailAddress;

	@UiField
	TextBox firstName;

	@UiField
	TextBox surname;

	@UiField
	PasswordTextBox password;

	@UiField
	PasswordTextBox confirmPassword;

	@UiField
	CloisterButton apply;

	@UiField
	HTMLPanel errorBox;

	@UiField
	HTML errorText;

	@UiField
	HTMLPanel processingView;

	@UiField
	HTMLPanel completionView;

	private Presenter presenter;

	public SignUpViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("apply")
	public void onApply(ClickEvent e) {
		errorText.setHTML("");
		errorBox.setVisible(false);

		presenter.attemptSignUp(emailAddress.getText(), firstName.getText(),
				surname.getText(), password.getText(),
				confirmPassword.getText());
	}

	@Override
	public void setMode(Mode mode) {
		form.setVisible(mode == Mode.FORM);
		processingView.setVisible(mode == Mode.PROCESSING);
		completionView.setVisible(mode == Mode.COMPLETION);
	}

	public void displayErrors(List<String> errors) {
		StringBuilder bld = new StringBuilder();
		for (String error : errors) {
			bld.append(error).append("<br>");
		}
		errorText.setHTML(bld.toString());
		errorBox.setVisible(true);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}
}
