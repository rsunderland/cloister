package com.earthabbey.cloister.client.compose;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.google.gwt.event.shared.GwtEvent;

public class ComposeMessageEvent extends GwtEvent<ComposeMessageEventHandler> {

	public static GwtEvent.Type<ComposeMessageEventHandler> TYPE = new GwtEvent.Type<ComposeMessageEventHandler>();

	private MemberSummary sender;
	private MemberSummary recipient;

	
	public ComposeMessageEvent()
	{
		
	}
	public ComposeMessageEvent(MemberSummary sender, MemberSummary recipient) {
		this.sender = sender;
		this.recipient = recipient;
	}

	@Override
	protected void dispatch(ComposeMessageEventHandler arg0) {
		arg0.handleComposeMessageEvent(this);
	}

	@Override
	public Type<ComposeMessageEventHandler> getAssociatedType() {
		return TYPE;
	}

	public MemberSummary getSender() {
		return sender;
	}

	public MemberSummary getRecipient() {
		return recipient;
	}
}
