package com.earthabbey.cloister.client;

import com.earthabbey.cloister.client.place.AdminPlace;
import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.CloisterLayout;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.spi.commands.Logout;
import com.earthabbey.cloister.spi.handlers.LoginResponseHandler;
import com.earthabbey.cloister.spi.handlers.LogoutResponseHandler;
import com.earthabbey.cloister.spi.responses.LoginResponse;
import com.earthabbey.cloister.spi.responses.LogoutResponse;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceChangeEvent;

public class CloisterLayoutPresenter implements CloisterLayout.Presenter,
		LoginResponseHandler, LogoutResponseHandler, PlaceChangeEvent.Handler {

	private ClientFactory clientFactory;
	private CloisterLayout view;

	public CloisterLayoutPresenter(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		view = clientFactory.getCloisterLayout();
		view.setPresenter(this);

		clientFactory.getEventBus().addHandler(LoginResponse.TYPE, this);
		clientFactory.getEventBus().addHandler(LogoutResponse.TYPE, this);
		clientFactory.getEventBus().addHandler(PlaceChangeEvent.TYPE, this);
	}

	@Override
	public void logout() {
		clientFactory.getInvoker().send(new Logout());
	}

	@Override
	public void administrate() {
		clientFactory.getPlaceController().goTo(new AdminPlace());
	}

	@Override
	public void handleLogoutResponse(LogoutResponse loginResponse) {
		view.enableLogout(false);
		view.enableAdmin(false);
	}

	@Override
	public void handleLoginResponse(LoginResponse loginResponse) {
		Session session = loginResponse.getSession();
		if (session != null) {
			view.enableLogout(true);
			view.enableAdmin(session.getPermissions().contains(
					Permission.SITE_ADMIN));
		}
	}

	public CloisterLayout getView() {
		return view;
	}

	@Override
	public void gotoMessages() {
		clientFactory.getPlaceController().goTo(new MessagePlace());
	}

	@Override
	public void gotoMembers() {
		clientFactory.getPlaceController().goTo(new MemberPlace());
	}

	@Override
	public void onPlaceChange(PlaceChangeEvent arg0) {
		Place place = arg0.getNewPlace();
		view.enableGotoButtons(place instanceof MessagePlace
				|| place instanceof MemberPlace);
	}
}
