package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.client.ActionServiceInvoker;
import com.earthabbey.cloister.client.mvp.InterceptingPlaceController;
import com.earthabbey.cloister.client.ui.api.AdminView;
import com.earthabbey.cloister.client.ui.api.AppLayout;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.CloisterLayout;
import com.earthabbey.cloister.client.ui.api.ConfirmEmailView;
import com.earthabbey.cloister.client.ui.api.LoginView;
import com.earthabbey.cloister.client.ui.api.MemberActionView;
import com.earthabbey.cloister.client.ui.api.MemberSelectorView;
import com.earthabbey.cloister.client.ui.api.MemberView;
import com.earthabbey.cloister.client.ui.api.MessageActionView;
import com.earthabbey.cloister.client.ui.api.MessageSelectorView;
import com.earthabbey.cloister.client.ui.api.MessageView;
import com.earthabbey.cloister.client.ui.api.SignUpView;
import com.earthabbey.cloister.spi.CloisterService;
import com.earthabbey.cloister.spi.CloisterServiceAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.Widget;

public class ClientFactoryImpl implements ClientFactory {

	private CloisterServiceAsync service = GWT.create(CloisterService.class);

	private final EventBus eventBus = new SimpleEventBus();

	private final ActionServiceInvoker invoker = new ActionServiceInvoker(
			service, eventBus);

	private final PlaceController placeController = new InterceptingPlaceController(
			eventBus);

	private AppLayoutImpl dynamicContent = new AppLayoutImpl();

	private CloisterLayout layout = new CloisterLayoutImpl(dynamicContent);

	private final LoginView loginView = new LoginViewImpl();

	private SignUpView signUpView = new SignUpViewImpl();

	private MemberViewImpl memberViewImpl = new MemberViewImpl();

	private MessageViewImpl messageViewImpl = new MessageViewImpl();

	private AdminView adminView = new AdminViewImpl();

	private MemberSelectorView memberSelectorViewImpl = new MemberSelectorViewImpl();

	private MessageSelectorView messageSelectorViewImpl = new MessageSelectorViewImpl();

	private MemberActionView memberActionViewImpl = new MemberActionViewImpl();

	private MessageActionView messageActionViewImpl = new MessageActionViewImpl();

	private ConfirmEmailViewImpl confirmEmailViewImpl = new ConfirmEmailViewImpl();

	public ClientFactoryImpl() {
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public PlaceController getPlaceController() {
		return placeController;
	}

	@Override
	public LoginView getLoginView() {
		return loginView;
	}

	@Override
	public ActionServiceInvoker getInvoker() {
		return invoker;
	}

	@Override
	public SignUpView getSignUpView() {
		return signUpView;
	}

	@Override
	public AdminView getAdminView() {
		return adminView;
	}

	@Override
	public CloisterLayout getCloisterLayout() {
		return layout;
	}

	@Override
	public AppLayout getAppLayout() {
		return dynamicContent;
	}

	@Override
	public MemberView getMemberView() {
		return memberViewImpl;
	}

	@Override
	public MessageView getMessageView() {
		return messageViewImpl;
	}

	@Override
	public MemberSelectorView getMemberSelectorView() {
		return memberSelectorViewImpl;
	}

	@Override
	public MessageSelectorView getMessageSelectorView() {
		return messageSelectorViewImpl;
	}

	@Override
	public MemberActionView getMemberActionView() {
		return memberActionViewImpl;
	}

	@Override
	public MessageActionView getMessageActionView() {
		return messageActionViewImpl;
	}

	@Override
	public ConfirmEmailView getConfirmEmailView() {
		return confirmEmailViewImpl;
	}
}
