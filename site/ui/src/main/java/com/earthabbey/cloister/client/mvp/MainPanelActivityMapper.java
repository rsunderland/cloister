package com.earthabbey.cloister.client.mvp;

import com.earthabbey.cloister.client.activity.AdminActivity;
import com.earthabbey.cloister.client.activity.ConfirmEmailActivity;
import com.earthabbey.cloister.client.activity.LoginActivity;
import com.earthabbey.cloister.client.activity.MemberActivity;
import com.earthabbey.cloister.client.activity.MessageActivity;
import com.earthabbey.cloister.client.activity.SignUpActivity;
import com.earthabbey.cloister.client.place.AdminPlace;
import com.earthabbey.cloister.client.place.ConfirmEmailPlace;
import com.earthabbey.cloister.client.place.LoginPlace;
import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.place.SignUpPlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class MainPanelActivityMapper implements ActivityMapper {
	
	private ClientFactory clientFactory;

	public MainPanelActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;		
	}

	@Override
	public Activity getActivity(Place place) {
		if (place instanceof AdminPlace) {
			return getAdminActivity(place);
		} else if (place instanceof LoginPlace) {
			return getLoginActivity(place);
		} else if (place instanceof SignUpPlace) {
			return getSignUpActivity(place);
		} else if (place instanceof MemberPlace) {
			return getMemberActivity(place);
		} else if (place instanceof MessagePlace) {
			return getMessageActivity(place);
		} else if (place instanceof ConfirmEmailPlace){ 
			return getConfirmEmailActivity(place);
		}
		else {
			return null;
		}
	}

	private Activity getAdminActivity(Place place) {
		return new AdminActivity((AdminPlace) place, clientFactory);
	}

	private Activity getLoginActivity(Place place) {
		return new LoginActivity((LoginPlace) place, clientFactory);
	}

	private Activity getSignUpActivity(Place place) {
		return new SignUpActivity((SignUpPlace) place, clientFactory);
	}

	private Activity getMemberActivity(Place place) {
		return new MemberActivity((MemberPlace)place, clientFactory);
	}

	private Activity getMessageActivity(Place place) {
		return new MessageActivity((MessagePlace)place, clientFactory);
	}

	private Activity getConfirmEmailActivity(Place place) {
		return new ConfirmEmailActivity((ConfirmEmailPlace)place, clientFactory);
	}
}
