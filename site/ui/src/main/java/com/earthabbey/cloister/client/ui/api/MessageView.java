package com.earthabbey.cloister.client.ui.api;

import java.util.Date;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.google.gwt.user.client.ui.IsWidget;

public interface MessageView extends IsWidget {
	
	void setTitle(String title);
	void setSender(MemberSummary sender);
	void setDate(Date date);
	void setContent(String html);
		
	void setPresenter(Presenter presenter);
	
	public interface Presenter
	{
		//No callbacks yet.
	}
}
