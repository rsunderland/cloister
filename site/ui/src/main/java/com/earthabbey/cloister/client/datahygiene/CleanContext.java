package com.earthabbey.cloister.client.datahygiene;

import java.util.ArrayList;
import java.util.List;

public class CleanContext {

	private List<String> issues;

	public CleanContext() {
		issues = new ArrayList<String>();
	}

	public void addIssue(String issue) {
		issues.add(issue);
	}

	public boolean isValid() {
		return issues.isEmpty();
	}

	public List<String> getIssues() {

		return issues;
	}
}
