package com.earthabbey.cloister.client.ui.impl;

import com.earthabbey.cloister.client.ui.api.CloisterLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class CloisterLayoutImpl extends Composite  implements CloisterLayout {

	private static CloisterLayoutUiBinder uiBinder = GWT
			.create(CloisterLayoutUiBinder.class);

	interface CloisterLayoutUiBinder extends UiBinder<Widget, CloisterLayoutImpl> {
	}

	private Presenter presenter;
		
	
	public  static final CommonResources base = GWT.create(CommonResources.class);
	
	private static CloisterLayoutResources local = GWT.create(CloisterLayoutResources.class);
	
	@UiField
	CloisterButton logoutButton;
	
	@UiField
	CloisterButton adminButton;
	
	@UiField
	CloisterButton messagesButton;
	
	@UiField
	CloisterButton membersButton;

	@UiField(provided=true)
	AppLayoutImpl dynamicContent;

	static
	{
		base.style().ensureInjected();
		local.style().ensureInjected();
	}
	

	public CloisterLayoutImpl(AppLayoutImpl layout) {
		dynamicContent = layout;
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	
	@UiHandler("logoutButton")
	public void onLogout(ClickEvent e) {
		presenter.logout();		
	}
	
	@UiHandler("adminButton")
	public void onAdmin(ClickEvent e) {
		presenter.administrate();
	}
	
	@UiHandler("membersButton")
	public void onMembers(ClickEvent e) {
		presenter.gotoMembers();
	}
	
	@UiHandler("messagesButton")
	public void onMessages(ClickEvent e) {
		presenter.gotoMessages();
	}

	@Override
	public void enableLogout(boolean enable) {
		logoutButton.setVisible(enable);
	}

	@Override
	public void enableAdmin(boolean enabled) {
		adminButton.setVisible(enabled);
	}

	@Override
	public void enableGotoButtons(boolean enabled) {
		membersButton.setVisible(enabled);
		messagesButton.setVisible(enabled);
	}
}
