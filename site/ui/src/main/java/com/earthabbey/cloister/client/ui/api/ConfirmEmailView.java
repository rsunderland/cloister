package com.earthabbey.cloister.client.ui.api;

import com.google.gwt.user.client.ui.IsWidget;

public interface ConfirmEmailView extends IsWidget {
		
	public void setMemberName(String name);
	
	public void setPresenter(Presenter presenter);
	
	public void setSiteUrl(String url);
	
	public void showChecking();
	
	public void showSuccess(boolean success);
	
	public void setMentorName(String name);
		
	public interface Presenter
	{
		
	}
}
