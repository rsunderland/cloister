package com.earthabbey.cloister.client.ui.impl;

import java.util.List;

import com.earthabbey.cloister.client.ui.api.MessageSelectorView;
import com.earthabbey.cloister.client.ui.api.MessageSelectorView.Presenter;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.MessageSummary;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

public class MessageSelectorViewImpl extends Composite implements
		MessageSelectorView, SelectionChangeEvent.Handler {

	private static MessageSelectorViewImplUiBinder uiBinder = GWT
			.create(MessageSelectorViewImplUiBinder.class);

	interface MessageSelectorViewImplUiBinder extends
			UiBinder<Widget, MessageSelectorViewImpl> {
	}

	public static final CommonResources base = GWT
			.create(CommonResources.class);

	static {
		base.style().ensureInjected();
	}

	@UiField(provided = true)
	CellList<MessageSummary> messageList;

	private Presenter presenter;

	private SingleSelectionModel<MessageSummary> selectionModel;

	private MessageSummaryCell cell;

	public MessageSelectorViewImpl() {
		cell = new MessageSummaryCell();
		messageList = new CellList<MessageSummary>(cell);
		initWidget(uiBinder.createAndBindUi(this));

		selectionModel = new SingleSelectionModel<MessageSummary>();
		messageList.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(this);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void setMessageSummaries(List<MessageSummary> summaries) {
		messageList.setRowCount(summaries.size());
		messageList.setRowData(0, summaries);
		messageList.redraw();
	}

	@Override
	public void onSelectionChange(SelectionChangeEvent arg0) {
		MessageSummary selected = selectionModel.getSelectedObject();
		presenter.select(selected);
	}

	@Override
	public void setViewerId(long viewerId) {
		cell.setViewerId(viewerId);
	}

	@Override
	public void setShowSentMessages(boolean show) {
		cell.setShowSent(show);
	}
}
