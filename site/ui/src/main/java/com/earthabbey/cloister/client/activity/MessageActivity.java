package com.earthabbey.cloister.client.activity;

import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.ui.api.ClientFactory;
import com.earthabbey.cloister.client.ui.api.MessageView;
import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.spi.commands.GetMessage;
import com.earthabbey.cloister.spi.commands.GetMessages;
import com.earthabbey.cloister.spi.handlers.GetMessageResponseHandler;
import com.earthabbey.cloister.spi.responses.GetMessageResponse;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

/**
 * Message Activity manages display of message details.
 * 
 * @author rich
 */
public class MessageActivity extends AbstractActivity implements
		MessageView.Presenter, GetMessageResponseHandler {

	private ClientFactory clientFactory;
	private MessageView view;
	private MessagePlace place;
	private Message message;

	public MessageActivity(MessagePlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.place = place;

	}

	@Override
	public void start(AcceptsOneWidget container, EventBus event) {
		view = clientFactory.getMessageView();
		view.setPresenter(this);
		container.setWidget(view);
		clientFactory.getEventBus().addHandler(GetMessageResponse.TYPE, this);
		clientFactory.getInvoker().send(new GetMessages());
		if (place.getId() != null) {
			clientFactory.getInvoker().send(new GetMessage(place.getId()));
		}
	}

	@Override
	public void handleGetMessageResponse(GetMessageResponse response) {
		message = response.getMessage();
		view.setTitle(message.getTitle());
		view.setSender(message.getSender());
		view.setDate(message.getSendTime());
		view.setContent(response.getMessage().getContent());
	}
}
