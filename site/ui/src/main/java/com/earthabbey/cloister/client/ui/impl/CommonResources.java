package com.earthabbey.cloister.client.ui.impl;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

public interface CommonResources extends ClientBundle {

	@Source("cloister-base.css")
	CloisterBaseStyle style();

	@ImageOptions(repeatStyle = RepeatStyle.Both)
	@Source("com/earthabbey/bg.jpg")
	ImageResource backgroundImage();

	@ImageOptions(repeatStyle = RepeatStyle.Both)
	@Source("com/earthabbey/bg_tint1.jpg")
	ImageResource tint1Image();

	@ImageOptions(repeatStyle = RepeatStyle.Both)
	@Source("com/earthabbey/bg_tint2.jpg")
	ImageResource tint2Image();

	@ImageOptions(repeatStyle = RepeatStyle.Both)
	@Source("com/earthabbey/bg_tint3.jpg")
	ImageResource tint3Image();

	@ImageOptions(repeatStyle = RepeatStyle.Both)
	@Source("com/earthabbey/bg_tint4.jpg")
	ImageResource tint4Image();

	public interface CloisterBaseStyle extends CssResource {
		String background();

		String tint1();

		String tint2();

		String tint3();

		String tint4();
		
		String panel();
	}
}
