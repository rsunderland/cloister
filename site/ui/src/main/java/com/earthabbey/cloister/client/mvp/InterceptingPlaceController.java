package com.earthabbey.cloister.client.mvp;

import com.earthabbey.cloister.client.place.ConfirmEmailPlace;
import com.earthabbey.cloister.client.place.LoginPlace;
import com.earthabbey.cloister.client.place.MemberPlace;
import com.earthabbey.cloister.client.place.MessagePlace;
import com.earthabbey.cloister.client.place.SignUpPlace;
import com.earthabbey.cloister.spi.handlers.LoginResponseHandler;
import com.earthabbey.cloister.spi.handlers.LogoutResponseHandler;
import com.earthabbey.cloister.spi.responses.LoginResponse;
import com.earthabbey.cloister.spi.responses.LogoutResponse;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;

public class InterceptingPlaceController extends PlaceController implements
		LoginResponseHandler, LogoutResponseHandler {

	private boolean loggedIn;

	private MessagePlace lastMessagePlace;
	private MemberPlace lastMemberPlace;

	public InterceptingPlaceController(EventBus eventBus) {
		super(eventBus);
		eventBus.addHandler(LoginResponse.TYPE, this);
		eventBus.addHandler(LogoutResponse.TYPE, this);		
		lastMemberPlace = new MemberPlace();
		lastMessagePlace = new MessagePlace();
	}

	public void goTo(Place newPlace) {
		if (loggedIn || newPlace instanceof SignUpPlace || newPlace instanceof ConfirmEmailPlace) {
			if (newPlace instanceof MessagePlace) {
				goToMessagePlace((MessagePlace) newPlace);
			} else if (newPlace instanceof MemberPlace) {
				goToMemberPlace((MemberPlace) newPlace);
			} else {
				super.goTo(newPlace);
			}
		} else {
			super.goTo(new LoginPlace());
		}
	}

	private void goToMemberPlace(MemberPlace newPlace) {				
		newPlace.populateNullValuesFrom(lastMemberPlace);
		lastMemberPlace = newPlace;		
		super.goTo(newPlace);		
	}

	private void goToMessagePlace(MessagePlace newPlace) {
		newPlace.populateNullValuesFrom(lastMessagePlace);
		lastMessagePlace = newPlace;
		super.goTo(newPlace);	
	}

	@Override
	public void handleLogoutResponse(LogoutResponse loginResponse) {
		loggedIn = false;
		lastMessagePlace = null;
		lastMemberPlace = null;
	}

	@Override
	public void handleLoginResponse(LoginResponse loginResponse) {
		loggedIn = (loginResponse.getSession() != null);
	}

}
