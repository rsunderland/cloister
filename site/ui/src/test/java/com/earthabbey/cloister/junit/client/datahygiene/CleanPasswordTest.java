package com.earthabbey.cloister.junit.client.datahygiene;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.earthabbey.cloister.client.datahygiene.CleanContext;
import com.earthabbey.cloister.client.datahygiene.CleanPassword;

/**
 * Test unit for {@link CleanPassword}.
 * 
 * @author richards
 * 
 */
public class CleanPasswordTest {

	@Test
	public final void test() {
		test(null, null, false, null);
		test("", null, false, null);
		test(null, "", false, null);
		test("", "", false, null);
		test("aoeuaoeu", "aoeuaoeu", true, "aoeuaoeu");
	}

	private void test(String password, String confirmPassword,
			boolean expectedIsValid, String expectedCleanValue) {
		CleanContext context = new CleanContext();
		CleanPassword cleanPassword = new CleanPassword(password,
				confirmPassword, context);
		assertThat(context.isValid(), is(expectedIsValid));
		if (expectedIsValid) {
			assertThat(cleanPassword.getCleanValue(), is(expectedCleanValue));
		}
	}

}
