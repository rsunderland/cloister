package com.earthabbey.cloister.junit.client.datahygiene;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.earthabbey.cloister.client.datahygiene.CleanContext;
import com.earthabbey.cloister.client.datahygiene.CleanEmail;

public class CleanEmailTest {

	@Test
	public final void test() {
		test(" richard@sunder.co.uk ", true, "richard@sunder.co.uk");
		test(" richard@sunder.co.uk.", false, null);
		test(" richard", false, null);
		test(" @richard", false, null);
		test(" a@b", false, null);
	}

	private void test(String rawEmail, boolean expectedIsValid,
			String expectedCleanEmail) {
		CleanContext context = new CleanContext();
		CleanEmail email = new CleanEmail(rawEmail, context);
		assertThat(context.isValid(), is(expectedIsValid));
		if (expectedIsValid) {
			assertThat(email.getCleanValue(), is(expectedCleanEmail));
		}
	}
}
