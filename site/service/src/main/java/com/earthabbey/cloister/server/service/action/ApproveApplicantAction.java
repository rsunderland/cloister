package com.earthabbey.cloister.server.service.action;

import static com.earthabbey.cloister.model.domain.MemberType.APPLICANT;
import static com.earthabbey.cloister.model.domain.Permission.APPROVE_APPLICANT;

import java.util.Arrays;

import org.apache.commons.mail.EmailException;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.model.entity.RoleEntity;
import com.earthabbey.cloister.model.store.MemberStore;
import com.earthabbey.cloister.model.store.Store;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.EmailSubSystem;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.server.service.EmailSubSystem.Template;
import com.earthabbey.cloister.spi.commands.ApproveApplicant;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;
import com.google.gwt.core.client.GWT;

/**
 * Approve Applicant
 * <ol>
 * <li>Confirm that current principal has privileges to approve applicant</li>
 * <li>Confirm that current applicant is really an applicant.</li>
 * <li>Change applicants role to member.</li>
 * </ol>
 */
public class ApproveApplicantAction implements
		Action<ApproveApplicant, RefreshMemberResponse> {

	private EmailSubSystem emailSubSystem;

	/**
	 * Constructor.
	 * 
	 * @param emailSubSystem
	 *            system used to send welcome message.
	 */
	public ApproveApplicantAction(EmailSubSystem emailSubSystem) {
		this.emailSubSystem = emailSubSystem;
	}

	@Override
	public Class<ApproveApplicant> getCommandClass() {
		return ApproveApplicant.class;
	}

	@Override
	public RefreshMemberResponse perform(ApproveApplicant arg,
			ServiceContext serviceContext) throws PermissionViolationException {
		GWT.log("Approve Applicant" + arg.getApplicant());
		serviceContext.assertCan(APPROVE_APPLICANT);

		Long memberId = arg.getApplicant().getId();

		MemberEntity member = serviceContext.getStore().getMemberStore()
				.readEntity(memberId, true);

		boolean memberChanged = approveMember(member, serviceContext);

		return buildResponse(serviceContext, member, memberChanged);
	}

	private boolean approveMember(MemberEntity member,
			ServiceContext serviceContext) {
		boolean memberChanged = false;
		Store store = serviceContext.getStore();

		memberChanged = upgradeMemberType(member, memberChanged);

		memberChanged = upgradePrincipal(member, memberChanged, store);

		// presist changes
		store.flush();

		if (memberChanged) {
			sendConfirmationEmail(member);
		} else {
			GWT.log("Attempt to approve member " + member
					+ " had not affect because they where already a member.");
		}

		return memberChanged;
	}

	private void sendConfirmationEmail(MemberEntity member) {
		try {
			String emailAddress = member.getPrincipal().getId();
			emailSubSystem.sendMessage(Template.APPROVAL_CONFIRMATION,
					Arrays.asList(emailAddress), member.getFirstName());
		} catch (EmailException e) {
			System.out.println("Failed to send confirmation email "
					+ e.getLocalizedMessage());
		}
	}

	private boolean upgradeMemberType(MemberEntity member, boolean memberChanged) {
		if (member.getType() != MemberType.MEMBER) {
			member.setType(MemberType.MEMBER);
			memberChanged = true;
		}
		return memberChanged;
	}

	private boolean upgradePrincipal(MemberEntity member,
			boolean memberChanged, Store store) {
		PrincipalEntity principal = member.getPrincipal();
		RoleEntity memberRole = store.getRoleStore()
				.getDefaultMemberRoleEntity();
		if (!principal.getRole().equals(memberRole)) {
			principal.setRole(memberRole);
			memberChanged = true;
		}
		return memberChanged;
	}

	private RefreshMemberResponse buildResponse(ServiceContext serviceContext,
			MemberEntity member, boolean memberChanged) {

		Member memberDto = serviceContext.getStore().getMemberStore()
				.translate(member);

		boolean updatable = serviceContext.canUpdate(memberDto.summerize());

		return new RefreshMemberResponse(memberDto, updatable, memberChanged);
	}

}
