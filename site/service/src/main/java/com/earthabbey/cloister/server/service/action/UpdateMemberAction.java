package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.UpdateMember;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

public class UpdateMemberAction implements
		Action<UpdateMember, RefreshMemberResponse> {

	@Override
	public Class<UpdateMember> getCommandClass() {
		return UpdateMember.class;
	}

	@Override
	public RefreshMemberResponse perform(UpdateMember arg,
			ServiceContext context) throws PermissionViolationException {

		Member member = arg.getMember();
		
		context.assertCanUpdate(member.summerize());

		context.getStore().getMemberStore().update(member);

		
		context.getStore().flush();
		return new RefreshMemberResponse(member, context.canRead(member
				.summerize()), false);
	}
}
