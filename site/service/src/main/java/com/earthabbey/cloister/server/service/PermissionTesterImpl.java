package com.earthabbey.cloister.server.service;

import static com.earthabbey.cloister.model.domain.Permission.MESSAGE_ANY;
import static com.earthabbey.cloister.model.domain.Permission.MESSAGE_MENTOR;
import static com.earthabbey.cloister.model.domain.Permission.READ_ANY;
import static com.earthabbey.cloister.model.domain.Permission.READ_APPLICANTS;
import static com.earthabbey.cloister.model.domain.Permission.READ_MEMBERS;
import static com.earthabbey.cloister.model.domain.Permission.READ_MENTOR;
import static com.earthabbey.cloister.model.domain.Permission.READ_SELF;
import static com.earthabbey.cloister.model.domain.Permission.UPDATE_ANY;
import static com.earthabbey.cloister.model.domain.Permission.UPDATE_SELF;

import java.util.Set;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Permission;

public class PermissionTesterImpl implements PermissionTester {

	private Member actor;
	private Set<Permission> permissions;

	public PermissionTesterImpl(Member actor, Set<Permission> permissions) {
		this.actor = actor;
		this.permissions = permissions;
	}

	public boolean canRead(MemberSummary candidate) {
		return can(READ_ANY) || (candidate.is(actor) && can(READ_SELF))
				|| (candidate.isMember() && can(READ_MEMBERS))
				|| (candidate.isApplicant() && can(READ_APPLICANTS))
				|| (candidate.isMentorOf(actor) && can(READ_MENTOR));
	}

	public boolean canUpdate(MemberSummary candidate) {
		return can(UPDATE_ANY) || (candidate.is(actor) && can(UPDATE_SELF));
	}

	public boolean canMessage(MemberSummary candidate) {
		return can(MESSAGE_ANY)
				|| (candidate.isMentorOf(actor) && can(MESSAGE_MENTOR));
	}

	public boolean can(Permission permission) {
		return permissions.contains(permission);
	}

	public void assertCan(Permission permission)
			throws PermissionViolationException {
		if (!can(permission)) {
			throw new PermissionViolationException(
					"Insufficient privileges for " + permission);
		}
	}

	public void assertCanRead(MemberSummary candidate)
			throws PermissionViolationException {
		if (!canRead(candidate)) {
			throw new PermissionViolationException(actor, candidate,
					" cannot read ");
		}
	}

	public void assertCanUpdate(MemberSummary candidate)
			throws PermissionViolationException {
		if (!canUpdate(candidate)) {
			throw new PermissionViolationException(actor, candidate,
					" cannot update ");
		}
	}

	public void assertCanMessage(MemberSummary candidate)
			throws PermissionViolationException {
		if (!canMessage(candidate)) {
			throw new PermissionViolationException(actor, candidate,
					" cannot message ");
		}
	}
}
