package com.earthabbey.cloister.server.service.action;

import java.util.ArrayList;
import java.util.List;

import com.earthabbey.cloister.model.domain.MessageSummary;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.GetMessages;
import com.earthabbey.cloister.spi.responses.GetMessagesResponse;

public class GetMessagesAction implements
		Action<GetMessages, GetMessagesResponse> {

	@Override
	public Class<GetMessages> getCommandClass() {
		return GetMessages.class;
	}

	@Override
	public GetMessagesResponse perform(GetMessages request,
			ServiceContext serviceContext) {
		List<MessageSummary> summaries = new ArrayList<MessageSummary>();

		if (serviceContext.getMember() != null)
		{
			summaries = serviceContext.getStore().getMessageStore()
					.getMembersMessages(serviceContext.getMember().summerize());
		}

		return new GetMessagesResponse(summaries);
	}

}
