package com.earthabbey.cloister.server.service;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Permission;

public interface PermissionTester {
	
	public boolean can(Permission permission);
	
	public boolean canRead(MemberSummary candidate);

	public boolean canUpdate(MemberSummary candidate);

	public boolean canMessage(MemberSummary candidate);
	
	
	public void assertCan(Permission permission) throws PermissionViolationException;
	
	public void assertCanRead(MemberSummary candidate) throws PermissionViolationException;

	public void assertCanUpdate(MemberSummary candidate) throws PermissionViolationException;

	public void assertCanMessage(MemberSummary candidate)  throws PermissionViolationException;

}