package com.earthabbey.cloister.server.service.action;

import java.util.Arrays;
import java.util.Date;

import org.apache.commons.mail.EmailException;

import com.earthabbey.cloister.model.domain.EmailConfirmation;
import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.model.domain.MessageState;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.model.store.Store;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.EmailSubSystem;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.server.service.EmailSubSystem.Template;
import com.earthabbey.cloister.spi.commands.SendMessage;
import com.earthabbey.cloister.spi.responses.SendMessageResponse;

public class SendMessageAction implements
		Action<SendMessage, SendMessageResponse> {

	/**
	 * Email system used to send the welcome message.
	 */
	private EmailSubSystem emailSubSystem;

	/**
	 * Constructor.
	 * 
	 * @param emailSubSystem
	 *            system used to send welcome message.
	 */
	public SendMessageAction(EmailSubSystem emailSubSystem) {
		this.emailSubSystem = emailSubSystem;
	}

	@Override
	public Class<SendMessage> getCommandClass() {
		return SendMessage.class;
	}

	@Override
	public SendMessageResponse perform(SendMessage request,
			ServiceContext context) throws PermissionViolationException {

		checkPermissions(request, context);

		sendMessages(request, context);

		context.getStore().flush();
		return new SendMessageResponse();
	}

	private void sendMessages(SendMessage request, ServiceContext serviceContext) {
		for (MemberSummary recipient : request.getRecipients()) {
			
			sendMessage(serviceContext, request.getSender(), recipient,
					request.getTitle(), request.getContent());

			try {
				sendEmail(serviceContext, request.getSender(), recipient,
						request.getTitle(), request.getContent());
			} catch (EmailException e) {
				System.out.println("Failed to send confirmation email "
						+ e.getLocalizedMessage());
			}
		}
	}

	private void checkPermissions(SendMessage request,
			ServiceContext serviceContext) throws PermissionViolationException {
		for (MemberSummary candidate : request.getRecipients()) {
			if (!serviceContext.canMessage(candidate)) {
				// if the list of recipients contains even a single
				// recipient to which this principal can not message
				// none of the messages should be sent.
				throw new PermissionViolationException(
						"Attempted to send message when did not have permission.");
			}
		}
	}

	private void sendMessage(ServiceContext serviceContext,
			MemberSummary sender, MemberSummary recipient, String title,
			String content) {
		Long id = null; // this will be assigned by the store.
		MessageState state = MessageState.NEW;
		Date sendTime = new Date();
		// owned by recipient
		Message message = new Message(id, state, title, content, recipient,
				recipient, sender, sendTime);

		// owned by sender
		Message copy = new Message(id, state, title, content, sender,
				recipient, sender, sendTime);
		copy.setState(MessageState.READ);

		serviceContext.getStore().getMessageStore().createEntity(message);
		serviceContext.getStore().getMessageStore().createEntity(copy);
	}

	public void sendEmail(ServiceContext serviceContext, MemberSummary sender,
			MemberSummary recipientSummary, String title, String content)
			throws EmailException {

		Store store = serviceContext.getStore();
		MemberEntity recipient = store.getMemberStore().readEntity(recipientSummary.getId(), true);
		PrincipalEntity recipientPrincipal = recipient.getPrincipal();
		String recipientEmail = recipientPrincipal.getId();
		
		content = content.replace("<br>", "\n");

		emailSubSystem.sendMessage(Template.NEW_MESSAGE,
				Arrays.asList(recipientEmail),
				recipientSummary.getDisplayText(), sender.getDisplayText());

	}
}