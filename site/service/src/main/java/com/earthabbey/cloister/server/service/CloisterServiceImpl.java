package com.earthabbey.cloister.server.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.earthabbey.client.cli.RestoreDriver;
import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.model.domain.SessionHandle;
import com.earthabbey.cloister.model.store.Store;
import com.earthabbey.cloister.server.service.action.ApproveApplicantAction;
import com.earthabbey.cloister.server.service.action.ConfirmEmailAction;
import com.earthabbey.cloister.server.service.action.CreateAccountAction;
import com.earthabbey.cloister.server.service.action.CreateMemberHelpOfferAction;
import com.earthabbey.cloister.server.service.action.CreateMemberHelpRequestAction;
import com.earthabbey.cloister.server.service.action.DeleteMemberHelpOfferAction;
import com.earthabbey.cloister.server.service.action.DeleteMemberHelpRequestAction;
import com.earthabbey.cloister.server.service.action.DeleteMessageAction;
import com.earthabbey.cloister.server.service.action.GetMemberAction;
import com.earthabbey.cloister.server.service.action.GetMembersAction;
import com.earthabbey.cloister.server.service.action.GetMessageAction;
import com.earthabbey.cloister.server.service.action.GetMessagesAction;
import com.earthabbey.cloister.server.service.action.LoginAction;
import com.earthabbey.cloister.server.service.action.LogoutAction;
import com.earthabbey.cloister.server.service.action.SendMessageAction;
import com.earthabbey.cloister.server.service.action.UpdateMemberAction;
import com.earthabbey.cloister.server.service.action.UpdateMemberHelpOfferAction;
import com.earthabbey.cloister.server.service.action.UpdateMemberHelpRequestAction;
import com.earthabbey.cloister.spi.CloisterService;
import com.earthabbey.cloister.spi.Command;
import com.earthabbey.cloister.spi.responses.ServiceErrorResponse;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * Cloister Service Implmentation.
 * 
 * Responsible for providing the communications between the GWT generated
 * java-script and the standard java server side components. Primary role is the
 * creation/update and deletion of bits of the domain model.
 * 
 * @author rich
 * 
 */
public class CloisterServiceImpl extends RemoteServiceServlet implements
		CloisterService {

	private static final String PERSISTENCE_UNIT_NAME = "PRODUCTION_PERSISTENCE_UNIT";

	private EntityManagerFactory entityManagerFactory;

	private EmailSubSystem emailSubSystem;

	public CloisterServiceImpl() throws IOException {

		this(PERSISTENCE_UNIT_NAME);
	}

	public CloisterServiceImpl(String persistenceUnitName) throws IOException {

		this.entityManagerFactory = null;

		if (Boolean.getBoolean("TEST_CONFIG")) {
			this.entityManagerFactory = Persistence
					.createEntityManagerFactory("TEST_PERSISTENCE_UNIT");
			loadSampleData();
		} else {
			this.entityManagerFactory = Persistence
					.createEntityManagerFactory(persistenceUnitName);
		}

		initActions();

	}

	private void loadSampleData() throws IOException {

		InputStream in = null;
		try {
			in = getClass().getClassLoader().getResourceAsStream(
					"sample_backup.xml");			
			new RestoreDriver(entityManagerFactory).restoreBackup(in);
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	private void initActions() {
		actions = new HashMap<String, Action<?, ?>>();

		emailSubSystem = new EmailSubSystem();

		createActions();
	}

	public CloisterServiceImpl(EntityManagerFactory factory) throws IOException {
		this.entityManagerFactory = factory;
		initActions();
	}

	/**
	 * Default serial id.
	 */
	private static final long serialVersionUID = 1L;

	Map<String, Action<?, ?>> actions;

	public <R extends Command> void addAction(Action<R, ?> action) {
		actions.put(action.getCommandClass().getCanonicalName(), action);
	}

	@SuppressWarnings("unchecked")
	public GwtEvent<?> perform(Command request, SessionHandle sessionHandle) {

		if (request == null) {
			throw new IllegalArgumentException("Null action.");
		}

		Action action = actions.get(request.getClass().getCanonicalName());

		if (action == null) {
			throw new IllegalArgumentException("Unknown action "
					+ request.getClass().getCanonicalName());
		}

		CommandActionTask task = new CommandActionTask(request, action);

		return peform(task, sessionHandle);
	}

	private GwtEvent<?> peform(CloisterTask task, SessionHandle sessionHandle) {
		EntityManager entityManager = entityManagerFactory
				.createEntityManager();
		try {
			return transactionalPerform(task, sessionHandle, entityManager);
		} finally {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}
	}

	private GwtEvent<?> transactionalPerform(CloisterTask task,
			SessionHandle sessionHandle, EntityManager entityManager) {

		entityManager.getTransaction().begin();

		Store store = new Store(entityManager);

		ServiceContext serviceContext = getServiceContext(store, sessionHandle);

		GwtEvent<?> result = robustPerform(task, serviceContext);

		entityManager.getTransaction().commit();

		return result;
	}

	private GwtEvent<?> robustPerform(CloisterTask task,
			ServiceContext serviceContext) {
		try {
			return task.perform(serviceContext);
		} catch (PermissionViolationException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			StringBuilder bld = new StringBuilder();
			Throwable prev = null;
			int count = 0;
			while (prev != e && count < 5) {
				bld.append(e.getLocalizedMessage()).append("\n");
				prev = e;
				count++;
			}
			return new ServiceErrorResponse(bld.toString());
		}
	}

	private void createActions() {
		addAction(new LoginAction());
		addAction(new LogoutAction());
		addAction(new GetMemberAction());
		addAction(new GetMembersAction());
		addAction(new UpdateMemberAction());
		addAction(new CreateAccountAction(emailSubSystem));
		addAction(new ApproveApplicantAction(emailSubSystem));
		addAction(new ConfirmEmailAction(emailSubSystem));
		addAction(new GetMessageAction());
		addAction(new GetMessagesAction());
		addAction(new SendMessageAction(emailSubSystem));
		addAction(new CreateMemberHelpOfferAction());
		addAction(new UpdateMemberHelpOfferAction());
		addAction(new DeleteMemberHelpOfferAction());
		addAction(new CreateMemberHelpRequestAction());
		addAction(new UpdateMemberHelpRequestAction());
		addAction(new DeleteMemberHelpRequestAction());
		addAction(new DeleteMessageAction());
	}

	protected ServiceContext getServiceContext(Store store,
			SessionHandle sessionHandle) {

		Store localStore = null; // only include reference to store if session
		// is valid
		Principal principal = null; // the principal that authenticated the
		// session
		Member member = null; // the member associated with the principal (if
		// any)

		if (sessionHandle == null) {
			return new ServiceContextImpl(store, null, null, null);
		} else {
			Session session = store.getSessionStore().read(sessionHandle, true);

			String token = session.getToken();
			if (token == null || !session.getToken().equals(token)) {
				throw new IllegalArgumentException("Invalid session token");
			}

			if (session.isExpired()) {
				throw new IllegalArgumentException("Session has expired");
			}

			localStore = store;
			principal = store.getPrincipalStore().read(session.getPrincipal(),
					true);
			if (principal.getMember() != null) {
				member = store.getMemberStore().read(principal.getMember(),
						false);
			}
			return new ServiceContextImpl(localStore, session, principal,
					member);
		}
	}

}
