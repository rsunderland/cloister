package com.earthabbey.cloister.server.service.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.apache.commons.mail.EmailException;
import org.mindrot.jbcrypt.BCrypt;

import com.earthabbey.cloister.model.domain.EmailConfirmation;
import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.model.store.Store;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.EmailSubSystem;
import com.earthabbey.cloister.server.service.EmailSubSystem.Template;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.CreateAccount;
import com.earthabbey.cloister.spi.responses.CreateAccountResponse;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;

/**
 * Create Account Action.
 * <ol>
 * <li>Select a mentor.</li>
 * <li>Create a principal.</li>
 * <li>Create a member.</li>
 * <li>Send welcome email.</li>
 * <li>NOT IMPLEMENTED YET Send email to mentor, Send message to member</li>
 * </ol>
 */
public class CreateAccountAction implements
		Action<CreateAccount, CreateAccountResponse> {

	/**
	 * Email system used to send the welcome message.
	 */
	private EmailSubSystem emailSubSystem;

	/**
	 * Constructor.
	 * 
	 * @param emailSubSystem
	 *            system used to send welcome message.
	 */
	public CreateAccountAction(EmailSubSystem emailSubSystem) {
		this.emailSubSystem = emailSubSystem;
	}

	/**
	 * Perform the action.
	 */
	@Override
	public CreateAccountResponse perform(CreateAccount application,
			ServiceContext context) {

		AccountCreationProcess process = new AccountCreationProcess(
				application, context.getStore());

		process.perform();

		return new CreateAccountResponse(process.isSuccess(),
				process.getFailureReasons(), context.getStore()
						.getMemberStore().translate(process.getMember()));
	}

	@Override
	public Class<CreateAccount> getCommandClass() {
		return CreateAccount.class;
	}

	/**
	 * Account Creation Process.
	 * 
	 * Manages account creation process, recording the artifacts generated as it
	 * goes.
	 * 
	 * @author rich
	 * 
	 */
	private class AccountCreationProcess {

		/**
		 * The application being processed.
		 */
		private final CreateAccount application;

		/**
		 * Store of all system persistent state.
		 */
		private final Store store;

		/**
		 * Reasons why the process has been unsuccessful.
		 */
		private List<String> failureReasons;

		/**
		 * The newly created member.
		 */
		private MemberEntity newMember;

		/**
		 * The mentor of the newly created member.
		 */
		private MemberEntity mentor;

		/**
		 * The principal of the newly created member.
		 */
		private PrincipalEntity principal;

		/**
		 * Constructor.
		 * 
		 * @param application
		 *            the application to process.
		 * @param store
		 *            access to the system persistent state.
		 */
		public AccountCreationProcess(CreateAccount application, Store store) {
			this.application = application;
			this.store = store;
			this.failureReasons = new ArrayList<String>();
		}

		/**
		 * Process the application.
		 */
		public void perform() {
			selectMentor();
			if (isSuccess()) {
				createPrincipal();
			}
			if (isSuccess()) {
				createMember();
			}
			store.flush();
			if (isSuccess()) {
				sendWelcomeEmail(application.getEmailConfirmationBaseUrl());
				setMessageToMentor();
			}			
		}

		/**
		 * Send a welcome email to applicant.
		 * 
		 * The welcome email will include basic information about the
		 * registration process, including a link to confirm that the email
		 * address is valid.
		 */
		private void sendWelcomeEmail(String emailConfirmationBaseUrl) {
			String token = UUID.randomUUID().toString();

			try {
				emailSubSystem.sendMessage(Template.EMAIL_CONFIRMATION, Arrays
						.asList(application.getEmailAddress()), application
						.getFirstName(), application.getLastName(),
						emailConfirmationBaseUrl + token);

				store.getEmailConfirmationStore().create(
						new EmailConfirmation(token, application
								.getEmailAddress()));
			} catch (EmailException e) {
				e.printStackTrace();
				failureReasons
						.add("Failed to send welcome email. Please contact EarthAbbey directly.");
			}
		}

		private void setMessageToMentor() {
			try {
				String emailAddress = mentor.getPrincipal().getId();
				emailSubSystem
						.sendMessage(Template.NEW_MENTEE,
								Arrays.asList(emailAddress),
								mentor.getFirstName(), newMember.getFirstName()
										+ " " + newMember.getSurname());
			} catch (EmailException e) {
				System.out.println("Failed to send confirmation email "
						+ e.getLocalizedMessage());
			}
		}

		/**
		 * Create the member.
		 */
		private void createMember() {
			newMember = store.getMemberStore().createEntity(
					new Member(null, principal.summerize(),
							MemberType.APPLICANT, application.getFirstName(),
							application.getLastName(), null,
							mentor.summerize(), null, null));

			newMember.setMentor(mentor);
		}

		/**
		 * Select a mentor for the member.
		 * 
		 * Will fail if no existing members have the
		 * {@link Permission.ACCEPT_NEW_MENTEE} permission.
		 */
		private void selectMentor() {

			List<MemberEntity> candidates = new ArrayList<MemberEntity>(store
					.getMemberStore().findMembersWithPermission(
							Permission.ACCEPT_NEW_MENTEE));

			if (candidates.isEmpty()) {
				failureReasons
						.add("Could not find any available mentors."
								+ " Please contain EarthAbbey directly via phone or email.");
			} else {
				Collections.shuffle(candidates);
				mentor = candidates.get(0);
			}
		}

		/**
		 * Create a principal for the member.
		 */
		private void createPrincipal() {
			Role role = store.getRoleStore().getCreationRole();

			String passwordHash = BCrypt.hashpw(application.getPassword(),
					BCrypt.gensalt());

			principal = store.getPrincipalStore().createEntity(
					new Principal(application.getEmailAddress(), passwordHash,
							role.summerize()));
		}

		/**
		 * @return the newly created member.
		 */
		public MemberEntity getMember() {
			return newMember;
		}

		/**
		 * @return whether the process has been successful.
		 */
		public boolean isSuccess() {
			return failureReasons.isEmpty();
		}

		/**
		 * @return list of reasons why the process failed.
		 */
		public List<String> getFailureReasons() {
			return failureReasons;
		}
	}

}
