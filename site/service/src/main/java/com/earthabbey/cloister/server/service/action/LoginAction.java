package com.earthabbey.cloister.server.service.action;

import java.util.Set;
import java.util.UUID;

import org.mindrot.jbcrypt.BCrypt;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.model.store.Store;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.Login;
import com.earthabbey.cloister.spi.responses.LoginResponse;
import com.google.gwt.core.client.GWT;

public class LoginAction implements Action<Login, LoginResponse> {
	
	@Override
	public LoginResponse perform(Login request, ServiceContext context) {
		GWT.log("Login " + request.getUsername());
		Session session = null;
		Store store = context.getStore();
		Principal principal = store.getPrincipalStore().read(
				request.getUsername(), false);

		if (credentialsAreValid(request, store, principal)) {
			session = createNewSession(store, principal);
		}

		context.getStore().flush();
		return new LoginResponse(session);
	}

	private Session createNewSession(Store store, Principal principal) {
		Session session;
		MemberSummary member = null;

		if (principal.getMember() != null) {
			member = store.getMemberStore().read(principal.getMember(), true)
					.summerize();
		}

		session = store.getSessionStore().create(
				new Session(UUID.randomUUID().toString(), principal
						.summerize(), member));
		return session;
	}

	private boolean credentialsAreValid(Login request, Store store,
			Principal principal) {
		return principal != null //
				&& hasLoginPermission(store, principal) //
				&& passwordCorrect(request, principal);
	}

	private boolean hasLoginPermission(Store store, Principal principal) {
		Set<Permission> permissions = store.getRoleStore().read(
				principal.getRole(), true).getPermissions();
		return permissions.contains(Permission.LOGIN);
	}

	private boolean passwordCorrect(Login request, Principal principal) {
		return BCrypt.checkpw(request.getPassword(), principal
				.getHashedPassword());
	}

	@Override
	public Class<Login> getCommandClass() {
		return Login.class;
	}



}
