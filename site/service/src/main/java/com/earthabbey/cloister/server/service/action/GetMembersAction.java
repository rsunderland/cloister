package com.earthabbey.cloister.server.service.action;

import java.util.ArrayList;
import java.util.List;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.GetMembers;
import com.earthabbey.cloister.spi.responses.GetMembersResponse;

public class GetMembersAction implements
		Action<GetMembers, GetMembersResponse> {

	@Override
	public GetMembersResponse perform(GetMembers request,
			ServiceContext context) {
		List<MemberSummary> result = new ArrayList<MemberSummary>();
		List<MemberSummary> candidates = context.getStore().getMemberStore()
				.getSummaries();
		for (MemberSummary candidate : candidates) {
			if (context.canRead(candidate)) {
				result.add(candidate);
			}
		}
		return new GetMembersResponse(result);
	}

	@Override
	public Class<GetMembers> getCommandClass() {
		return GetMembers.class;
	}
}
