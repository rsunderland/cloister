package com.earthabbey.cloister.server.service;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.model.store.Store;

public interface ServiceContext  extends PermissionTester {
	public Member getMember();
	public Principal getPrincipal();
	public Store getStore();
	public Session getSession();
}