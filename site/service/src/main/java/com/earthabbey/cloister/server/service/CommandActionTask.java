package com.earthabbey.cloister.server.service;

import com.earthabbey.cloister.spi.Command;
import com.google.gwt.event.shared.GwtEvent;

public class CommandActionTask implements CloisterTask {

	private Command command;
	private Action action;

	public CommandActionTask(Command command, Action action) {
		this.command = command;
		this.action = action;
	}

	public GwtEvent<?> perform(ServiceContext context)
			throws PermissionViolationException {
		return action.perform(command, context);
	}
}
