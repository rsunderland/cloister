package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.DeleteMemberHelpRequest;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

public class DeleteMemberHelpRequestAction implements
		Action<DeleteMemberHelpRequest, RefreshMemberResponse> {

	@Override
	public Class<DeleteMemberHelpRequest> getCommandClass() {
		return DeleteMemberHelpRequest.class;
	}

	@Override
	public RefreshMemberResponse perform(DeleteMemberHelpRequest command,
			ServiceContext serviceContext) throws PermissionViolationException {

		MemberHelpRequest request = serviceContext.getStore()
				.getMemberHelpRequestStore().read(
						command.getMemberHelpRequestId(), true);

		serviceContext.assertCanUpdate(request.getRequestingMember());

		serviceContext.getStore().getMemberHelpRequestStore().delete(request);


		Member member = serviceContext.getStore().getMemberStore()
				.read(request.getRequestingMember().getId(), true);

		serviceContext.getStore().flush();
		return new RefreshMemberResponse(member, serviceContext.canRead(member
				.summerize()), false);
	}
}
