package com.earthabbey.cloister.server.service;

import com.google.gwt.event.shared.GwtEvent;

public interface CloisterTask {

	GwtEvent<?> perform(ServiceContext context)
			throws PermissionViolationException;

}