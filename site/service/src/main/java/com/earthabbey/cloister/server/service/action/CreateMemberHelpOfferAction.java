package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.entity.MemberHelpOfferEntity;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.CreateMemberHelpOffer;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

public class CreateMemberHelpOfferAction implements
		Action<CreateMemberHelpOffer, RefreshMemberResponse> {

	@Override
	public Class<CreateMemberHelpOffer> getCommandClass() {
		return CreateMemberHelpOffer.class;
	}

	@Override
	public RefreshMemberResponse perform(CreateMemberHelpOffer command,
			ServiceContext serviceContext) throws PermissionViolationException {
		serviceContext.assertCanUpdate(command.getOfferingMember());

		MemberHelpOfferEntity offer = serviceContext
				.getStore()
				.getMemberHelpOfferStore()
				.createEntity(
						new MemberHelpOffer(null, command.getText(), command
								.getOfferingMember()));

		Member member = serviceContext.getStore().getMemberStore()
				.translate(offer.getOfferingMember());

		boolean editable = serviceContext.canUpdate(member.summerize());

		serviceContext.getStore().flush();
		return new RefreshMemberResponse(member, editable, false);
	}
}
