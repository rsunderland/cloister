package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.model.domain.MessageState;
import com.earthabbey.cloister.model.domain.MessageSummary;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.GetMessage;
import com.earthabbey.cloister.spi.responses.GetMessageResponse;

public class GetMessageAction implements Action<GetMessage, GetMessageResponse> {
	@Override
	public Class<GetMessage> getCommandClass() {
		return GetMessage.class;
	}

	@Override
	public GetMessageResponse perform(GetMessage request,
			ServiceContext serviceContext) throws PermissionViolationException {

		if (serviceContext.getMember() == null) {
			throw new PermissionViolationException(
					"Can not get message is actor is not specified.");
		}

		Message message = null;

		message = serviceContext.getStore().getMessageStore()
				.read(request.getMessageId(), true);

		if (!serviceContext.getMember().getId().equals(message.getOwner()
				.getId())) {
			throw new PermissionViolationException(serviceContext.getMember(),
					message.getRecipient(), " is not recipient ");
		}

		if (message.getState() == MessageState.NEW) {
			message.setState(MessageState.READ);
			serviceContext.getStore().getMessageStore().update(message);
		}
		
		return new GetMessageResponse(message);
	}
}
