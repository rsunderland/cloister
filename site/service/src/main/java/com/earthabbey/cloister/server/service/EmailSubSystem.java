package com.earthabbey.cloister.server.service;

import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.EnumMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;

import org.apache.commons.io.IOUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class EmailSubSystem {

	public enum Template {
		EMAIL_CONFIRMATION, NEW_MESSAGE, APPROVAL_CONFIRMATION, NEW_MENTEE, USER_GUIDE
	}

	private final EnumMap<Template, MessageFormat> contentFormatters;
	private final EnumMap<Template, MessageFormat> titleFormatters;

	public EmailSubSystem() {
		contentFormatters = loadFormatterFromFiles();
		titleFormatters = loadFormatterFromProperties("email_title");
	}

	private EnumMap<Template, MessageFormat> loadFormatterFromProperties(
			String bundle) {
		EnumMap<Template, MessageFormat> formatters = new EnumMap<EmailSubSystem.Template, MessageFormat>(
				Template.class);
		ResourceBundle resources = ResourceBundle.getBundle(bundle);
		for (Template template : Template.values()) {
			formatters.put(template, new MessageFormat(resources
					.getString(template.toString())));
		}

		return formatters;
	}

	private EnumMap<Template, MessageFormat> loadFormatterFromFiles() {
		EnumMap<Template, MessageFormat> result = new EnumMap<Template, MessageFormat>(
				Template.class);
		for (Template template : Template.values()) {
			String name = template.toString() + ".txt";
			URL url = getClass().getClassLoader().getResource(name);

			if (url == null) {
				throw new NoSuchElementException("Could not find resource "
						+ name);
			}

			try {
				result.put(template, new MessageFormat(IOUtils.toString(url)));
			} catch (IOException e) {
				throw new NoSuchElementException("Could not find " + name);
			}

		}
		return result;
	}

	public void sendMessage(Template template, List<String> recipients,
			Object... parameters) throws EmailException {
		String title = titleFormatters.get(template).format(parameters);
		String content = contentFormatters.get(template).format(parameters);

		Email email = new SimpleEmail();
		email.setDebug(true);
		email.setFrom("cloister@earthabbey.com");
		email.setSubject(title);
		email.setMsg(content);
		email.setHostName("localhost");
		for (String recipient : recipients) {
			email.addTo(recipient);
		}
		email.send();

	}
}
