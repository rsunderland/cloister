package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.GetMember;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

public class GetMemberAction implements
		Action<GetMember, RefreshMemberResponse> {

	@Override
	public RefreshMemberResponse perform(GetMember request,
			ServiceContext context) throws PermissionViolationException {

		if (request == null) {
			throw new IllegalArgumentException(
					"Failed to read member details, argument was null.");
		}

		if (request.getId() == null) {
			throw new IllegalArgumentException(
					"Failed to read member because id was null.");
		}

		Member member = context.getStore().getMemberStore()
				.read(request.getId(), true);

		context.assertCanRead(member.summerize());
	
		return new RefreshMemberResponse(member, context.canUpdate(member
				.summerize()), false);
	}

	@Override
	public Class<GetMember> getCommandClass() {
		return GetMember.class;
	}
}
