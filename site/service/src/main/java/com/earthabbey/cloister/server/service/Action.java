package com.earthabbey.cloister.server.service;

import com.earthabbey.cloister.spi.Command;
import com.google.gwt.event.shared.GwtEvent;


public interface Action<C extends Command, R extends GwtEvent<?>> {

	R perform(C command, ServiceContext serviceContext) throws PermissionViolationException;
	
	Class<C> getCommandClass();
}
