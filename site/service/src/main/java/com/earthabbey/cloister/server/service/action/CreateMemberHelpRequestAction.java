package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpRequestEntity;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.CreateMemberHelpRequest;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

public class CreateMemberHelpRequestAction implements
		Action<CreateMemberHelpRequest, RefreshMemberResponse> {

	@Override
	public Class<CreateMemberHelpRequest> getCommandClass() {
		return CreateMemberHelpRequest.class;
	}

	@Override
	public RefreshMemberResponse perform(CreateMemberHelpRequest command,
			ServiceContext serviceContext) throws PermissionViolationException {

		serviceContext.assertCanUpdate(command.getRequestingMember());

		MemberHelpRequestEntity request = serviceContext
				.getStore()
				.getMemberHelpRequestStore()
				.createEntity(
						new MemberHelpRequest(null, command.getText(), command
								.getRequestingMember()));

		Member member = serviceContext.getStore().getMemberStore()
				.translate(request.getRequestingMember());

		boolean editable = serviceContext.canUpdate(member.summerize());

		serviceContext.getStore().flush();
		return new RefreshMemberResponse(member, editable, false);
	}
}
