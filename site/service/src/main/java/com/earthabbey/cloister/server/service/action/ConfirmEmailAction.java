package com.earthabbey.cloister.server.service.action;

import java.util.Arrays;
import java.util.logging.Logger;

import org.apache.commons.mail.EmailException;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.entity.EmailConfirmationEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.model.store.EmailConfirmationStore;
import com.earthabbey.cloister.model.store.MemberStore;
import com.earthabbey.cloister.model.store.PrincipalStore;
import com.earthabbey.cloister.model.store.RoleStore;
import com.earthabbey.cloister.model.store.Store;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.EmailSubSystem;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.server.service.EmailSubSystem.Template;
import com.earthabbey.cloister.spi.commands.ConfirmEmail;
import com.earthabbey.cloister.spi.responses.ConfirmEmailResponse;

/**
 * Confirm Email Action.
 * <ol>
 * <li>Identify the confirm email record for the key provided</li>
 * <li>Identify the principal that is confirming their email</li>
 * <li>Set the role of the principal to applicant</li>
 * <li>Retrieve the member associated with the principal (if any)</li>
 * <li>Delete the confirm email record</li>
 * <li>return success flag and member (if any).</li>
 * </ol>
 */
public class ConfirmEmailAction implements
		Action<ConfirmEmail, ConfirmEmailResponse> {

	private static final Logger LOGGER = Logger
			.getLogger(ConfirmEmailAction.class.getCanonicalName());
	private final EmailSubSystem emailSubsystem;

	public ConfirmEmailAction(EmailSubSystem emailSubsystem) {
		this.emailSubsystem = emailSubsystem;
	}

	@Override
	public ConfirmEmailResponse perform(ConfirmEmail request,
			ServiceContext context) {

		String confirmationKey = request.getToken();
		Store store = context.getStore();
		EmailConfirmationStore emailConfirmationStore = store
				.getEmailConfirmationStore();

		RoleStore roleStore = store.getRoleStore();
		PrincipalStore principalStore = store.getPrincipalStore();
		MemberStore memberStore = store.getMemberStore();

		EmailConfirmationEntity confirmation = emailConfirmationStore
				.readEntity(confirmationKey, false);

		boolean success = false;
		Member memberDto = null;

		if (confirmation == null) {
			LOGGER
					.warning("Attempt to confirm email failed due to unrecognised token "
							+ confirmationKey);
		} else {
			String emailAddress = confirmation.getEmailAddress();
			PrincipalEntity principal = principalStore.readEntity(emailAddress,
					true);
			principal.setRole(roleStore.getApplicantRoleEntity());
			emailConfirmationStore.deleteEntity(confirmation);

			if (principal.getMember() != null) {
				memberDto = memberStore.translate(principal.getMember());
			}

			emailConfirmationStore.flush();
			success = true;

			try {
				emailSubsystem.sendMessage(Template.USER_GUIDE, Arrays
						.asList(emailAddress), memberDto.summerize()
						.getDisplayText());
			} catch (EmailException e) {
				System.out.println("Failed to send user guide "
						+ e.getLocalizedMessage());
			}
		}

		store.flush();
		return new ConfirmEmailResponse(success, memberDto);
	}

	@Override
	public Class<ConfirmEmail> getCommandClass() {
		return ConfirmEmail.class;
	}
}
