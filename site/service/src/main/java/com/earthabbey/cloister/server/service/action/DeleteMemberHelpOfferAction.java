package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.DeleteMemberHelpOffer;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

public class DeleteMemberHelpOfferAction implements
		Action<DeleteMemberHelpOffer, RefreshMemberResponse> {

	@Override
	public Class<DeleteMemberHelpOffer> getCommandClass() {
		return DeleteMemberHelpOffer.class;
	}

	@Override
	public RefreshMemberResponse perform(DeleteMemberHelpOffer command,
			ServiceContext serviceContext) throws PermissionViolationException {

		MemberHelpOffer offer = serviceContext.getStore()
				.getMemberHelpOfferStore()
				.read(command.getMemberHelpOfferId(), true);

		serviceContext.assertCanUpdate(offer.getOfferingMember());

		serviceContext.getStore().getMemberHelpOfferStore().delete(offer);

		Member member = serviceContext.getStore().getMemberStore()
				.read(offer.getOfferingMember().getId(), true);

		serviceContext.getStore().flush();
		return new RefreshMemberResponse(member, serviceContext.canRead(member
				.summerize()), false);
	}
}
