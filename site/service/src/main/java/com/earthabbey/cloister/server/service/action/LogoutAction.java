package com.earthabbey.cloister.server.service.action;

import java.util.Date;

import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.model.store.SessionStore;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.Logout;
import com.earthabbey.cloister.spi.responses.LogoutResponse;

public class LogoutAction  implements Action<Logout, LogoutResponse>{

	@Override
	public Class<Logout> getCommandClass() {
		return Logout.class;
	}

	@Override
	public LogoutResponse perform(Logout arg,
			ServiceContext context) {		
		SessionStore sessionStore = context.getStore().getSessionStore();		
		Session session = sessionStore.read(context.getSession(), true);		
		session.setExpired(true);
		session.setLogOutTime(new Date());
		sessionStore.update(session);	
		context.getStore().flush();
		return new LogoutResponse();
	}
}
