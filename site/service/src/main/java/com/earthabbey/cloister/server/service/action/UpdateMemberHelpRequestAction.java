package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpRequestEntity;
import com.earthabbey.cloister.model.store.MemberHelpRequestStore;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.UpdateMemberHelpRequest;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

public class UpdateMemberHelpRequestAction implements
		Action<UpdateMemberHelpRequest, RefreshMemberResponse> {

	@Override
	public Class<UpdateMemberHelpRequest> getCommandClass() {
		return UpdateMemberHelpRequest.class;
	}

	@Override
	public RefreshMemberResponse perform(UpdateMemberHelpRequest command,
			ServiceContext context) throws PermissionViolationException {

		MemberHelpRequestStore requestStore = context.getStore()
				.getMemberHelpRequestStore();

		Long requestId = command.getMemberHelpRequestId();

		if (requestId == null) {
			throw new IllegalArgumentException(
					"Null id for help request supplied.");
		}

		MemberHelpRequestEntity request = requestStore.readEntity(requestId, true);
		
		MemberEntity requestingMember = request.getRequestingMember();

		context.assertCanUpdate(requestingMember.summerize());

		request.setText(command.getReplacementText());

		Member member = context.getStore().getMemberStore().translate(requestingMember);				

		context.getStore().flush();
		return new RefreshMemberResponse(member, context.canRead(member
				.summerize()), false);
	}
}
