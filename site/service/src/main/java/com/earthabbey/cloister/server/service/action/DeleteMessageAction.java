package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.model.domain.MessageState;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.DeleteMessage;
import com.earthabbey.cloister.spi.commands.GetMessages;
import com.earthabbey.cloister.spi.responses.GetMessagesResponse;

public class DeleteMessageAction implements
		Action<DeleteMessage, GetMessagesResponse> {

	/**
	 * Delegate used to retrieve messages visible to the user.
	 */
	private GetMessagesAction delegate;

	public DeleteMessageAction() {
		delegate = new GetMessagesAction();
	}

	@Override
	public GetMessagesResponse perform(DeleteMessage command,
			ServiceContext serviceContext) throws PermissionViolationException {
		Long messageId = command.getMessageId();

		Message message = serviceContext.getStore().getMessageStore()
				.read(messageId, true);

		if (!message.getOwner().getId()
				.equals(serviceContext.getMember().getId())) {
			throw new PermissionViolationException(
					"Members can only delete there own messages");
		}

		message.setState(MessageState.DELETED);

		serviceContext.getStore().getMessageStore().update(message);

		serviceContext.getStore().flush();
		return delegate.perform(new GetMessages(), serviceContext);
	}

	@Override
	public Class<DeleteMessage> getCommandClass() {
		return DeleteMessage.class;
	}

}
