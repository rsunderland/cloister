package com.earthabbey.cloister.server.service;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Permission;

public class PermissionViolationException extends Exception {

	
	public PermissionViolationException(String cause)
	{
		super(cause);
	}
	
	public PermissionViolationException(Member actor, Permission missingPermssion)
	{
		super(actor.toString() +  " does not have permission " + missingPermssion);
	}
	
	public PermissionViolationException(Member actor, MemberSummary candidate,
			String cannotDetails) {
		super("" + actor + cannotDetails + candidate);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
