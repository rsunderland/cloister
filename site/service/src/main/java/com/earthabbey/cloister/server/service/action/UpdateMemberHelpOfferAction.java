package com.earthabbey.cloister.server.service.action;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpOfferEntity;
import com.earthabbey.cloister.server.service.Action;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.spi.commands.UpdateMemberHelpOffer;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

public class UpdateMemberHelpOfferAction implements
		Action<UpdateMemberHelpOffer, RefreshMemberResponse> {

	@Override
	public Class<UpdateMemberHelpOffer> getCommandClass() {
		return UpdateMemberHelpOffer.class;
	}

	@Override
	public RefreshMemberResponse perform(UpdateMemberHelpOffer command,
			ServiceContext context) throws PermissionViolationException {

		Long helpOfferId = command.getMemberHelpOfferId();

		if (helpOfferId == null) {
			throw new IllegalArgumentException(
					"Null id for help offer supplied.");
		}

		MemberHelpOfferEntity offer = context.getStore()
				.getMemberHelpOfferStore()
				.readEntity(command.getMemberHelpOfferId(), true);

		MemberEntity offeringMember = offer.getOfferingMember();

		context.assertCanUpdate(offeringMember.summerize());

		offer.setText(command.getReplacementText());

		Member member = context.getStore().getMemberStore()
				.translate(offeringMember);

		context.getStore().flush();

		return new RefreshMemberResponse(member, context.canRead(member
				.summerize()), false);
	}
}
