package com.earthabbey.cloister.server.service;

import java.util.Collections;
import java.util.Set;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.model.store.Store;

public class ServiceContextImpl extends PermissionTesterImpl implements ServiceContext {

	private final Principal principal;
	private final Member member;
	private final Store store;
	private final Session session;

	public ServiceContextImpl(Store store, Session session, Principal principal, Member member) {
		super(member, getPermissions(store, principal));
		this.store = store;
		this.session = session;
		this.principal = principal;
		this.member = member;
	}


	@SuppressWarnings("unchecked")
	private static Set<Permission> getPermissions(Store store,
			Principal principal) {
		Set<Permission> result = Collections.EMPTY_SET;
		if (principal != null) {
			result = store.getRoleStore().read(principal.getRole(), true)
					.getPermissions();
		}
		return result;
	}

	public Member getMember() {
		return member;
	}

	public Principal getPrincipal() {
		return principal;
	}

	public Store getStore() {
		return store;
	}

	@Override
	public Session getSession() {
		return session;
	}
}
