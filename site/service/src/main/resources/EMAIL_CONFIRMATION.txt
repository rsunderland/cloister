Dear {0} {1},

Thank you for your interest in becoming a member of EarthAbbey.

Please confirm your email address by clicking on this link: {2}

Regards

EarthAbbey

