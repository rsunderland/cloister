package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.earthabbey.cloister.spi.commands.Logout;
import com.earthabbey.cloister.spi.responses.LogoutResponse;

/**
 * Test unit for {@link LoginAction}.
 * 
 * @author richards
 * 
 */
public class LogoutActionTest extends AbstractActionTest {

	private LogoutAction action;

	@Before
	public void beforeEachTest() throws IOException {							
		configureMocks();		
		action = new LogoutAction();
	}

	@Test
	public void validUsernameAndPasswordWithLoginPermissionCanLogin() {
		Logout request = new Logout();
		LogoutResponse result = action.perform(request, context);
		assertNotNull(result);
		assertThat(session.isExpired(), is(true));
		verify(sessionStore, times(1)).update(session);
	}
	

}
