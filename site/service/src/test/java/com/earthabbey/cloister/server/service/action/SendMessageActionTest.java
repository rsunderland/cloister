package com.earthabbey.cloister.server.service.action;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.earthabbey.cloister.server.service.EmailSubSystem;
import com.earthabbey.cloister.spi.commands.SendMessage;

public class SendMessageActionTest extends AbstractActionTest {

	private SendMessageAction action;

	@Mock
	private EmailSubSystem mockEmailSubsystem;



	@Before
	public void beforeEachTest() throws IOException {
		configureMocks();
		action = new SendMessageAction(mockEmailSubsystem);
	}

	@Test
	public void canSendMessage() {
		SendMessage request = new SendMessage();
	}

}
