package com.earthabbey.cloister.server.service.action;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpOfferEntity;
import com.earthabbey.cloister.model.entity.MemberHelpRequestEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.model.entity.RoleEntity;
import com.earthabbey.cloister.model.store.Store;

public abstract class AbstractEntityManagerTest {

	private static final String PERSISTENCE_UNIT_NAME = "TEST_PERSISTENCE_UNIT";
	private Store store;
	private EntityManager em;
	private EntityManagerFactory emFactory;
	protected RoleEntity newAccountRole;
	protected RoleEntity applicantRole;
	protected RoleEntity memberRole;
	protected RoleEntity moderatorRole;
	protected RoleEntity adminRole;

	@Before
	public void initializePersistence() throws IOException {
		emFactory = Persistence
				.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		em = emFactory.createEntityManager();
		store = new Store(em);
		clearStore();
		startTransaction();
	}

	public void clearStore() {
		startTransaction();
		store.deleteAll();
		commitTransaction();
	}

	@After
	public void afterEachTest() {
		commitTransaction();
	}

	private void commitTransaction() {
		if (em != null && em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
	}

	private void startTransaction() {
		em.getTransaction().begin();
	}

	public EntityManager getEntityManager() {
		return em;
	}

	public Store getStore() {
		return store;
	}

	protected void createStandardRoles() {
		newAccountRole = createTestRoleEntity("NewAccount");
		applicantRole = createTestRoleEntity("Applicant",
				Permission.MESSAGE_MENTOR, Permission.LOGIN,
				Permission.UPDATE_SELF, Permission.READ_MENTOR,
				Permission.READ_SELF);
		memberRole = createTestRoleEntity("Member", Permission.MESSAGE_ANY,
				Permission.LOGIN, Permission.UPDATE_SELF,
				Permission.READ_MEMBERS);
		moderatorRole = createTestRoleEntity("Moderator",
				Permission.APPROVE_APPLICANT, Permission.ACCEPT_NEW_MENTEE,
				Permission.MESSAGE_ANY, Permission.LOGIN,
				Permission.UPDATE_SELF, Permission.READ_MEMBERS,
				Permission.READ_APPLICANTS);
		adminRole = createTestRoleEntity("Moderator",
				Permission.APPROVE_APPLICANT, Permission.MESSAGE_ANY,
				Permission.LOGIN, Permission.UPDATE_ANY, Permission.READ_ANY);
	}

	protected RoleEntity createTestRoleEntity(String roleName,
			Permission... permissions) {
		HashSet<Permission> permissionSet = new HashSet<Permission>();
		permissionSet.addAll(Arrays.asList(permissions));
		return store.getRoleStore().createEntity(
				new Role(null, roleName, permissionSet));
	}

	protected PrincipalEntity createTestPrincipalEntity(String principalName, RoleEntity role) {
		return getStore().getPrincipalStore().createEntity(
				new Principal(principalName, "", role.summerize()));
	}

	protected MemberEntity createTestMemberEntity(String memberName,
			PrincipalEntity principal) {
		return getStore().getMemberStore().createEntity(
				new Member(null, principal.summerize(), MemberType.MEMBER,
						memberName, memberName, null, null, null, null));
	}

	protected MemberHelpOfferEntity createTestOffer(MemberEntity member,
			String offerText) {
		return getStore().getMemberHelpOfferStore().createEntity(
				new MemberHelpOffer(null, offerText, member.summerize()));
	}
	
	protected MemberHelpRequestEntity createTestRequest(MemberEntity member,
			String requestText) {
		return getStore().getMemberHelpRequestStore().createEntity(
				new MemberHelpRequest(null, requestText, member.summerize()));
	}
}
