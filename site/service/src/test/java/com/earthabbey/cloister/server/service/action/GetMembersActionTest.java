package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.spi.commands.GetMembers;
import com.earthabbey.cloister.spi.responses.GetMembersResponse;


/**
 * Test unit for {@link GetMemberAction}.
 * 
 * @author richards
 * 
 */
public class GetMembersActionTest extends AbstractActionTest {

	private GetMembersAction action;

	private MemberSummary summary1 = new MemberSummary(1L, "1",null,
			MemberType.MEMBER);

	private MemberSummary summary2 = new MemberSummary(2L, "2",null,
			MemberType.MEMBER);

	private MemberSummary summary3 = new MemberSummary(3L, "3",null,
			MemberType.MEMBER);

	private MemberSummary summary4 = new MemberSummary(4L, "4",null,
			MemberType.MEMBER);

	private MemberSummary summary5 = new MemberSummary(5L, "5",null,
			MemberType.MEMBER);

	private List<MemberSummary> summaries;

	@Before
	public void beforeEachTest() {
		action = new GetMembersAction();
		summaries = Arrays.asList(summary1, summary2, summary3, summary4,
				summary5);
		MockitoAnnotations.initMocks(this);
		configureMocks();		
		when(memberStore.getSummaries()).thenReturn(summaries);
	}

	@Test
	public final void membersThatCannotBeReadAreFilteredOut() {
		when(context.canRead(summary1)).thenReturn(true);
		when(context.canRead(summary2)).thenReturn(false);
		when(context.canRead(summary3)).thenReturn(true);
		when(context.canRead(summary4)).thenReturn(false);
		when(context.canRead(summary5)).thenReturn(true);
		GetMembersResponse response = action.perform(new GetMembers(), context);
		assertNotNull(response);
		List<MemberSummary> result = response.getMemberSummaries();		
		assertThat(result, is(Arrays.asList(summary1, summary3, summary5)));
	}

	@Test
	public final void canFilterOutAllMembers() {
		when(context.canRead(summary1)).thenReturn(false);
		when(context.canRead(summary2)).thenReturn(false);
		when(context.canRead(summary3)).thenReturn(false);
		when(context.canRead(summary4)).thenReturn(false);
		when(context.canRead(summary5)).thenReturn(false);
		GetMembersResponse response = action.perform(new GetMembers(), context);
		assertNotNull(response);
		List<MemberSummary> result = response.getMemberSummaries();
		assertThat(result.size(), is(0));
	}

	@Test
	public final void canFilterOutNoMembers() {
		when(context.canRead(summary1)).thenReturn(true);
		when(context.canRead(summary2)).thenReturn(true);
		when(context.canRead(summary3)).thenReturn(true);
		when(context.canRead(summary4)).thenReturn(true);
		when(context.canRead(summary5)).thenReturn(true);
		GetMembersResponse response = action.perform(new GetMembers(), context);
		assertNotNull(response);
		List<MemberSummary> result = response.getMemberSummaries();
		assertThat(result, is(Arrays.asList(summary1, summary2, summary3,
				summary4, summary5)));
	}
}
