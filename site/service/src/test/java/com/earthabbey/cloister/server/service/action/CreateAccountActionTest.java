package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.mail.EmailException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.AnyVararg;

import com.earthabbey.cloister.model.domain.EmailConfirmation;
import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.server.service.EmailSubSystem;
import com.earthabbey.cloister.server.service.ServiceContextImpl;
import com.earthabbey.cloister.server.service.EmailSubSystem.Template;
import com.earthabbey.cloister.spi.commands.CreateAccount;
import com.earthabbey.cloister.spi.responses.CreateAccountResponse;

/**
 * Test unit for {@link CreateAccountAction}.
 * 
 * @author richards
 * 
 */
public class CreateAccountActionTest extends AbstractEntityManagerTest {

	private static final String VALID_USERNAME = "moderator@a.place.com";

	private CreateAccountAction action;
	
	private static String EXPECTED_EMAIL = "test@domain.com";
	private static String EXPECTED_FIRST_NAME = "Harry";
	private static String EXPECTED_SURNAME = "Potter";
	private static String EXPECTED_PASSWORD = "test";
	private static String EXPECTED_CALLBACK_URL = "aoeu";

	@Mock
	private EmailSubSystem mockEmailSubsystem;

	private PrincipalEntity mentorPrincipal;

	private MemberEntity mentorMember;

	@Before
	public void beforeEachTest() throws IOException {
		super.initializePersistence();
		MockitoAnnotations.initMocks(this);
		action = new CreateAccountAction(mockEmailSubsystem);
		
		createStandardRoles();

		// create approver with moderator privileges (i.e., the have
		// Permission.ACCEPT_NEW_MENTEE).
		mentorPrincipal = createTestPrincipalEntity(VALID_USERNAME,
				moderatorRole);

		mentorMember = createTestMemberEntity("Member Name",
				mentorPrincipal);
	}

	@Test
	public void testAction() throws EmailException {

		ServiceContextImpl context = new ServiceContextImpl(getStore(), null, null, null);
		
		CreateAccount request = new CreateAccount(EXPECTED_EMAIL, EXPECTED_FIRST_NAME, EXPECTED_SURNAME,
				EXPECTED_PASSWORD, EXPECTED_CALLBACK_URL);
		
		CreateAccountResponse result = action.perform(request, context);

		assertNotNull(result);
		assertTrue(result.isSuccess());		
		Member member = result.getNewlyCreatedMember();
		assertNotNull(member);

		// check response.
		Long memberId = member.getId();
		assertThat(member.getFirstName(), is(EXPECTED_FIRST_NAME));
		assertThat(member.getSurname(), is(EXPECTED_SURNAME));
		assertNull(member.getWayOfLifeSummary());
		assertThat(member.getHelpOffers().isEmpty(), is(true));
		assertThat(member.getHelpRequests().isEmpty(), is(true));
		assertThat(member.getType(), is(MemberType.APPLICANT));
		assertThat(member.getPrincipal().getId(), is(EXPECTED_EMAIL));
		assertThat(member.getMentor().getId(), is (mentorMember.getId()));
		
		// check entities
		MemberEntity memberEntity = getStore().getMemberStore().readEntity(memberId, true);
		assertThat(memberEntity.getFirstName(), is(EXPECTED_FIRST_NAME));
		assertThat(memberEntity.getSurname(), is(EXPECTED_SURNAME));
		assertNull(memberEntity.getWayOfLifeSummary());
		assertThat(memberEntity.getHelpOffers().isEmpty(), is(true));
		assertThat(memberEntity.getHelpRequests().isEmpty(), is(true));
		assertThat(memberEntity.getType(), is(MemberType.APPLICANT));
		
		PrincipalEntity principal = memberEntity.getPrincipal();
		assertNotNull(principal);
		assertThat(principal.getId(), is(EXPECTED_EMAIL));
		assertThat(principal.getRole(), is(newAccountRole));
		
		assertThat(memberEntity.getMentor(), is(mentorMember));
		
		
		verify(mockEmailSubsystem, times(2)).sendMessage(any(Template.class),
				any(List.class), anyVararg());

		
	}
}
