package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.mail.EmailException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.earthabbey.cloister.model.domain.EmailConfirmation;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.model.entity.RoleEntity;
import com.earthabbey.cloister.server.service.EmailSubSystem;
import com.earthabbey.cloister.server.service.ServiceContextImpl;
import com.earthabbey.cloister.server.service.EmailSubSystem.Template;
import com.earthabbey.cloister.spi.commands.ConfirmEmail;
import com.earthabbey.cloister.spi.responses.ConfirmEmailResponse;

public class ConfirmEmailActionTest extends AbstractEntityManagerTest {

	private static final String CONFIRMATION_KEY = "tehthu-aoeunth-aoeunh";

	@Mock
	private EmailSubSystem mockEmailSubsystem;

	private ConfirmEmailAction action;

	private EmailConfirmation confirmation;

	private String VALID_USERNAME = "one@test.com";

	private PrincipalEntity principal;

	private MemberEntity member;

	@Before
	public void beforeEachTest() throws IOException{
		super.initializePersistence();
		MockitoAnnotations.initMocks(this);
		action = new ConfirmEmailAction(mockEmailSubsystem);
		createStandardRoles();
		principal = createTestPrincipalEntity(VALID_USERNAME, newAccountRole);
		member = createTestMemberEntity("Member Name", principal);
		confirmation = new EmailConfirmation(CONFIRMATION_KEY, VALID_USERNAME);
		getStore().getEmailConfirmationStore().create(confirmation);
		
	}

	/**
	 * Main flow. Test the following rules 1) The role of a prinicipal must
	 * APPLICANT when their email address is confirmed. 2) The email
	 * confirmation record must be deleted after a successful email
	 * confirmation.
	 * @throws EmailException 
	 */
	@Test
	public void canConfirmEmailAddress() throws EmailException {
		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				null, null);

		// initially principal has newAccountRole
		assertThat(principal.getRole(), is(newAccountRole));

		// confirmation record exists
		assertNotNull(getStore().getEmailConfirmationStore().readEntity(
				CONFIRMATION_KEY, false));

		ConfirmEmailResponse response = action.perform(new ConfirmEmail(
				CONFIRMATION_KEY), context);
		assertNotNull(response);

		// confirmation has been marked as successful
		assertThat(response.isSuccess(), is(true));

		// member object has been included in the response.
		assertThat(response.getMember().getId(), is(member.getId()));

		// role has been changed.
		assertThat(principal.getRole(), is(applicantRole));

		// confirmation record has been deleted
		assertNull(getStore().getEmailConfirmationStore().readEntity(
				CONFIRMATION_KEY, false));
		
		// that an email has been sent.
		verify(mockEmailSubsystem, times(1)).sendMessage(Template.USER_GUIDE,
				Arrays.asList(VALID_USERNAME), "Member Name Member Name");
	}
}
