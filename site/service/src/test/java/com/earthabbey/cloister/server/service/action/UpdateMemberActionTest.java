package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.spi.commands.UpdateMember;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

/**
 * Test Unit for {@link UpdateMemberAction}.
 * 
 * @author richards
 * 
 */
public class UpdateMemberActionTest extends AbstractActionTest {

	@Before
	public void beforeEachTest() {
		super.configureMocks();
	}

	/**
	 * Test that update member request is delegated to memberStore.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under test conditions.
	 */
	@Test
	public void updateMemberCallsMemberStoreUpdate()
			throws PermissionViolationException {		
		when(context.canUpdate(member1.summerize())).thenReturn(true);
		UpdateMemberAction action = new UpdateMemberAction();
		UpdateMember request = new UpdateMember(member1);
		RefreshMemberResponse response = action.perform(request, context);
		assertNotNull(response);
		verify(memberStore, times(1)).update(member1);
	}

	/**
	 * Test that {@link PermissionViolationException} is forwarded.
	 * 
	 * @throws PermissionViolationException
	 *             should happen.
	 */
	@Test
	public void updateMemberWithPermissionsThrowPermissionViolationException()
			throws PermissionViolationException {
		PermissionViolationException e = new PermissionViolationException(member1, null, null);
		doThrow(e).when(context).assertCanUpdate(member1.summerize());
		UpdateMemberAction action = new UpdateMemberAction();
		UpdateMember request = new UpdateMember(member1);
		try {
			action.perform(request, context);
			fail("PermissionViolationException expected but not found.");
		} catch (PermissionViolationException e2) {
			assertThat(e, is(e2));
		}
	}
}
