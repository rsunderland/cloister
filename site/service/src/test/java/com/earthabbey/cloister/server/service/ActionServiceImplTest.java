package com.earthabbey.cloister.server.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.sql.rowset.spi.TransactionalWriter;

import org.junit.Before;
import org.junit.Test;

import com.earthabbey.cloister.spi.Command;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

import static org.mockito.Mockito.*;

/**
 * Test unit for {@link AbstractActionServiceImpl}.
 * 
 * @author richards
 * 
 */
public class ActionServiceImplTest {

	/**
	 * Class under test.
	 */
	private CloisterServiceImpl cut;

	@Before
	public void beforeEachTest() throws IOException {
		
		EntityManagerFactory entityManageFactory = mock(EntityManagerFactory.class);
		EntityManager entityManager = mock(EntityManager.class);
		EntityTransaction transaction = mock(EntityTransaction.class);
		
		
		when(entityManageFactory.createEntityManager()).thenReturn(entityManager);
		when(entityManager.getTransaction()).thenReturn(transaction);
		
		cut = new CloisterServiceImpl(entityManageFactory);
	}

	@Test
	public void delegationToAction() {
		cut.addAction(new ActionOne());
		cut.addAction(new ActionTwo());

		GwtEvent<?> result1 = cut.perform(new ActionRequestOne(), null);
		assertNotNull(result1);
		assertThat(result1.getClass().getCanonicalName(),
				is(ActionResponseOne.class.getCanonicalName()));

		GwtEvent<?> result2 = cut.perform(new ActionRequestTwo(), null);
		assertNotNull(result2);
		assertThat(result2.getClass().getCanonicalName(),
				is(ActionResponseTwo.class.getCanonicalName()));
	}

	@Test
	public void illegalArgumentIsThrownForNullAction() {		
		try {
			cut.perform(null, null);
			fail("IllegalArgumentException expected but not found.");
		} catch (IllegalArgumentException e) {
			
		}
	}

	@Test
	public void illegalArgumentIsThrownForUnknownAction() {		
		try {
			cut.perform(new ActionRequestOne(), null);
			fail("IllegalArgumentException expected but not found.");
		} catch (IllegalArgumentException e) {
		}
	}
	
	

	@SuppressWarnings("serial")
	private class ActionRequestOne implements Command {

	}

	private class ActionResponseOne extends GwtEvent<ActionResponseOneHandler> {
		@Override
		protected void dispatch(ActionResponseOneHandler arg0) {
		}

		@Override
		public com.google.gwt.event.shared.GwtEvent.Type<ActionResponseOneHandler> getAssociatedType() {
			return new GwtEvent.Type<ActionResponseOneHandler>();
		}
	}

	private class ActionResponseOneHandler implements EventHandler {
	}

	private class ActionOne implements
			Action<ActionRequestOne, ActionResponseOne> {
		@Override
		public Class<ActionRequestOne> getCommandClass() {
			return ActionRequestOne.class;
		}

		@Override
		public ActionResponseOne perform(ActionRequestOne arg, ServiceContext context) {
			return new ActionResponseOne();
		}

	}

	@SuppressWarnings("serial")
	private class ActionRequestTwo implements Command {
	}

	private class ActionResponseTwo extends GwtEvent<ActionResponseTwoHandler> {
		@Override
		protected void dispatch(ActionResponseTwoHandler arg0) {
		}

		@Override
		public com.google.gwt.event.shared.GwtEvent.Type<ActionResponseTwoHandler> getAssociatedType() {
			return new GwtEvent.Type<ActionResponseTwoHandler>();
		}
	}

	private class ActionResponseTwoHandler implements EventHandler {
	}

	private class ActionTwo implements
			Action<ActionRequestTwo, ActionResponseTwo> {
		@Override
		public Class<ActionRequestTwo> getCommandClass() {
			return ActionRequestTwo.class; 
		}

		@Override
		public ActionResponseTwo perform(ActionRequestTwo arg, ServiceContext serviceContext) {
			return new ActionResponseTwo();
		}
	}
}
