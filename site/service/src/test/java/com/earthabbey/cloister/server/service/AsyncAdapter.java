package com.earthabbey.cloister.server.service;

import com.earthabbey.cloister.model.domain.SessionHandle;
import com.earthabbey.cloister.spi.CloisterService;
import com.earthabbey.cloister.spi.CloisterServiceAsync;
import com.earthabbey.cloister.spi.Command;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class AsyncAdapter implements CloisterServiceAsync {
		
	private CloisterService delegate;

	public AsyncAdapter(CloisterService delegate)
	{
		this.delegate = delegate;
	}

	@Override
	public void perform(Command request, SessionHandle sessionHandle,
			AsyncCallback<GwtEvent<?>> eventCallback) {
		GwtEvent<?> event = delegate.perform(request, sessionHandle);
		eventCallback.onSuccess(event);	
	}
}
