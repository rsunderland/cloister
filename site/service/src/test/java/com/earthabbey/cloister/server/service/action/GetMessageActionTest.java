package com.earthabbey.cloister.server.service.action;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.spi.commands.GetMessage;
import com.earthabbey.cloister.spi.responses.GetMessageResponse;

public class GetMessageActionTest  extends AbstractActionTest {
	
	
	private GetMessageAction action;

	@Before
	public void beforeEachTest() throws IOException {							
		configureMocks();		
		action = new GetMessageAction();		
	}
	
	@Test
	public void userCanReadOwnMessages() throws PermissionViolationException
	{
		when(context.getMember()).thenReturn(member1);
		GetMessage request = new GetMessage(message1.getId());		
		GetMessageResponse response = action.perform(request, context);		
		assertNotNull(response);
		assertNotNull(response.getMessage());		
	}
	
	@Test
	public void principalWithNoMemberCannotReadMessage()
	{		
		try
		{
			when(context.getMember()).thenReturn(null);
			action.perform(new GetMessage(message1.getId()), context);
			fail("Expected PermissionViolationException but it was not thrown.");
		}
		catch(PermissionViolationException e)
		{
			
		}
	}
	
	@Test
	public void userCannotAccessAnotherMembersMessage()
	{		
		try
		{
			when(context.getMember()).thenReturn(member2);
			action.perform(new GetMessage(message1.getId()), context);
			fail("Expected PermissionViolationException but it was not thrown.");
		}
		catch(PermissionViolationException e)
		{			
		}
	}

}
