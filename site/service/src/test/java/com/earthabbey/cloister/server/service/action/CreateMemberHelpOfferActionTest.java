package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpOfferEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContextImpl;
import com.earthabbey.cloister.spi.commands.CreateMemberHelpOffer;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

/**
 * Test unit for {@link CreateMemberHelpOfferAction}.
 */
public class CreateMemberHelpOfferActionTest extends AbstractEntityManagerTest {

	private static final String VALID_USERNAME = "user@a.place.com";

	private CreateMemberHelpOfferAction action;

	private static final String VALID_USERNAME2 = "admin@a.place.com";

	private static String EXPECTED_OFFER_TEXT = "example help offer";

	@Before
	public void beforeEachTest() throws IOException {
		super.initializePersistence();
		MockitoAnnotations.initMocks(this);
		action = new CreateMemberHelpOfferAction();

		createStandardRoles();
	}

	/**
	 * Simple case where member adds their own offer.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under text
	 */
	@Test
	public void testAction() throws PermissionViolationException {

		// create approver with member privileges (i.e., the have
		// Permission.UPDATE_SELF).
		PrincipalEntity principal = createTestPrincipalEntity(VALID_USERNAME,
				memberRole);

		MemberEntity member = createTestMemberEntity("Member Name", principal);

		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(principal), getStore()
						.getMemberStore().translate(member));

		CreateMemberHelpOffer request = new CreateMemberHelpOffer(
				member.summerize(), EXPECTED_OFFER_TEXT);

		assertTrue(member.getHelpOffers().isEmpty());

		RefreshMemberResponse result = action.perform(request, context);

		// check response.
		assertNotNull(result);
		assertThat(result.isEditable(), is(true));
		assertThat(result.getMember().getId(), is(member.getId()));

		// check entities
		assertThat(member.getHelpOffers().size(), is(1));
		MemberHelpOfferEntity offer = member.getHelpOffers().get(0);
		assertThat(offer.getText(), is(EXPECTED_OFFER_TEXT));
		assertThat(offer.getOfferingMember(), is(member));
	}

	/**
	 * Prinical other than member used.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under text
	 */
	@Test
	public void testAdminModification() throws PermissionViolationException {

		PrincipalEntity admin = createTestPrincipalEntity(VALID_USERNAME2,
				adminRole);

		// create approver with member privileges (i.e., the have
		// Permission.UPDATE_SELF).
		PrincipalEntity principal = createTestPrincipalEntity(VALID_USERNAME,
				memberRole);

		MemberEntity member = createTestMemberEntity("Member Name", principal);

		// create a session authorized by the admin, rather than the member
		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(admin), null);

		CreateMemberHelpOffer request = new CreateMemberHelpOffer(
				member.summerize(), EXPECTED_OFFER_TEXT);

		assertTrue(member.getHelpOffers().isEmpty());

		RefreshMemberResponse result = action.perform(request, context);

		// check response.
		assertNotNull(result);
		assertThat(result.isEditable(), is(true));
		assertThat(result.getMember().getId(), is(member.getId()));

		// check entities
		assertThat(member.getHelpOffers().size(), is(1));
		MemberHelpOfferEntity offer = member.getHelpOffers().get(0);
		assertThat(offer.getText(), is(EXPECTED_OFFER_TEXT));
		assertThat(offer.getOfferingMember(), is(member));
	}

	/**
	 * Simple case where member adds their own offer.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under text
	 */
	@Test
	public void testActionWithoutPrivileges()
			throws PermissionViolationException {

		// create approver with member privileges (i.e., the have
		// Permission.UPDATE_SELF).
		PrincipalEntity principal1 = createTestPrincipalEntity(VALID_USERNAME,
				memberRole);

		MemberEntity member1 = createTestMemberEntity("Member Name", principal1);

		// create approver with member privileges (i.e., the have
		// Permission.UPDATE_SELF).
		PrincipalEntity principal2 = createTestPrincipalEntity(VALID_USERNAME2,
				memberRole);

		MemberEntity member2 = createTestMemberEntity("Member Name", principal2);

		// create session based on member 2, who does not have permission
		// to create an offer for member 1
		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(principal2),
				getStore().getMemberStore().translate(member2));

		CreateMemberHelpOffer request = new CreateMemberHelpOffer(
				member1.summerize(), EXPECTED_OFFER_TEXT);

		assertTrue(member1.getHelpOffers().isEmpty());

		try {
			action.perform(request, context);
			fail("Expected PermissionViolationException but did not find it.");
		} catch (PermissionViolationException e) {
			// nothing to do.
		}
	}
}
