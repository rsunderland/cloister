package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.spi.commands.GetMember;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

/**
 * Test unit for {@link GetMembersAction}.
 * 
 * @author richards
 * 
 */
public class GetMemberActionTest extends AbstractActionTest {

	private GetMemberAction action;

	private Role role = new Role(2L, "aoeu", null);

	private Principal principal1 = new Principal("snth", "snth",
			role.summerize());
	private Principal principal2 = new Principal("snth2", "snth",
			role.summerize());

	private MemberSummary summary1 = new MemberSummary(1L, "1", null,
			MemberType.MEMBER);

	private MemberSummary summary2 = new MemberSummary(2L, "2", null,
			MemberType.MEMBER);

	private Member member1 = new Member(1L, principal1.summerize(),
			MemberType.MEMBER, "1", "2", null, null, null, null);

	private Member member2 = new Member(2L, principal2.summerize(),
			MemberType.MEMBER, "1", "2", null, null, null, null);

	@Before
	public void beforeEachTest() throws IOException {
		MockitoAnnotations.initMocks(this);
		configureMocks();
		when(memberStore.read(summary1.getId(), true)).thenReturn(member1);
		when(memberStore.read(summary2.getId(), true)).thenReturn(member2);
		when(context.canRead(summary1)).thenReturn(false);
		when(context.canRead(summary2)).thenReturn(false);
		when(context.canUpdate(summary1)).thenReturn(false);
		when(context.canUpdate(summary2)).thenReturn(false);
		action = new GetMemberAction();
	}

	@Test
	public final void willPassPassThroughReadRequestIfHasPermissions()
			throws PermissionViolationException {
		when(context.canRead(summary1)).thenReturn(true);
		RefreshMemberResponse response = action.perform(new GetMember(summary1),
				context);
		assertNotNull(response);
		Member result = response.getMember();
		assertNotNull(result);
		assertThat(result, is(member1));
	}

	@Test
	@Ignore("Underlying mock not handling assert based permissions test.")
	public final void willBlockReadRequestIfHasNotPermissions()
			throws PermissionViolationException {
		
		RefreshMemberResponse response = action.perform(new GetMember(summary1),
				context);
		assertNotNull(response);
		Member result = response.getMember();
		assertNull(result);
	}

	@Test
	public final void willSetEditableHintIfCanUpdate()
			throws PermissionViolationException {
		when(context.canRead(summary1)).thenReturn(true);
		when(context.canUpdate(summary1)).thenReturn(true);
		RefreshMemberResponse response = action.perform(new GetMember(summary1),
				context);
		assertNotNull(response);
		Member result = response.getMember();
		assertThat(response.isEditable(), is(true));
	}

	@Test
	public final void willSetEditableHintIfCannotUpdate()
			throws PermissionViolationException {
		when(context.canRead(summary1)).thenReturn(true);
		when(context.canUpdate(summary1)).thenReturn(false);
		RefreshMemberResponse response = action.perform(new GetMember(summary1),
				context);
		assertNotNull(response);
		Member result = response.getMember();
		assertThat(response.isEditable(), is(false));
	}

}
