package com.earthabbey.cloister.server.service.action;

import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.mail.EmailException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.server.service.EmailSubSystem;
import com.earthabbey.cloister.server.service.EmailSubSystem.Template;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.server.service.ServiceContextImpl;
import com.earthabbey.cloister.spi.commands.ApproveApplicant;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class ApproveApplicantTest extends AbstractEntityManagerTest {

	private static final String VALID_USERNAME = "approver@a.place.com";

	private static final String VALID_USERNAME2 = "approvee@an.other.place.com";

	private ApproveApplicantAction action;
	
	@Mock
	private EmailSubSystem mockEmailSubsystem;


	@Before
	public void beforeEachTest() throws IOException {
		super.initializePersistence();
		MockitoAnnotations.initMocks(this);
		action = new ApproveApplicantAction(mockEmailSubsystem);
		createStandardRoles();
	}

	@Test
	public void canApproveApplicant() throws PermissionViolationException, EmailException {
		// create approver with moderator privileges (i.e., the have
		// Permission.APPROVE_APPLICANT).
		PrincipalEntity approverPrincipal = createTestPrincipalEntity(
				VALID_USERNAME, moderatorRole);

		MemberEntity approverMember = createTestMemberEntity("Member Name",
				approverPrincipal);

		// create member to be approved
		PrincipalEntity approveePrincipal = createTestPrincipalEntity(
				VALID_USERNAME2, applicantRole);
		MemberEntity approveeMember = createTestMemberEntity("Member Name",
				approveePrincipal);

		approveeMember.setType(MemberType.APPLICANT);

		ServiceContext context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(approverPrincipal),
				getStore().getMemberStore().translate(approverMember));

		ApproveApplicant command = new ApproveApplicant(
				approveeMember.summerize());

		// confirm that approvee is in a valid state before invoking the action.
		assertThat(approveeMember.getType(), is(MemberType.APPLICANT));
		assertThat(approveePrincipal.getRole(), is(applicantRole));

		RefreshMemberResponse result = action.perform(command, context);

		assertNotNull(result);

		// check response is correct. Summary has changed, but moderator
		// should not be able to edit user. Also check that the member
		// has been returned.
		assertThat(result.isSummaryChanged(), is(true));
		assertThat(result.isEditable(), is(false));
		assertThat(result.getMember().getId(), is(approveeMember.getId()));
		assertThat(result.getMember().getType(), is(MemberType.MEMBER));
		assertThat(result.getMember().getPrincipal().getId(),
				is(approveePrincipal.getId()));

		// check that the entity has been updated.
		assertThat(approveeMember.getType(), is(MemberType.MEMBER));
		assertThat(approveePrincipal.getRole(), is(memberRole));
		
		verify(mockEmailSubsystem, times(1)).sendMessage(Template.APPROVAL_CONFIRMATION, 
				Arrays.asList(approveePrincipal.getId()),
				approveeMember.getFirstName());
	}

	@Test
	public void cannotApproveApplicantWithoutPermission() {
		// create approver with member privileges (i.e., the have
		// do not have Permission.APPROVE_APPLICANT).
		PrincipalEntity approverPrincipal = createTestPrincipalEntity(
				VALID_USERNAME, memberRole);

		MemberEntity approverMember = createTestMemberEntity("Member Name",
				approverPrincipal);

		// create member to be approved
		PrincipalEntity approveePrincipal = createTestPrincipalEntity(
				VALID_USERNAME2, applicantRole);
		MemberEntity approveeMember = createTestMemberEntity("Member Name",
				approveePrincipal);

		approveeMember.setType(MemberType.APPLICANT);

		ServiceContext context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(approverPrincipal),
				getStore().getMemberStore().translate(approverMember));

		ApproveApplicant command = new ApproveApplicant(
				approveeMember.summerize());

		try {
			RefreshMemberResponse result = action.perform(command, context);
			fail("Expected but did not find a permissions exception.");
		} catch (PermissionViolationException e) {
			// Nothing to do.
		}
	}
	
	@Test
	public void approvingAnAlreadyApprovedMemberDoesNothing() throws PermissionViolationException
	{
		// create approver with moderator privileges (i.e., the have
		// Permission.APPROVE_APPLICANT).
		PrincipalEntity approverPrincipal = createTestPrincipalEntity(
				VALID_USERNAME, moderatorRole);

		MemberEntity approverMember = createTestMemberEntity("Member Name",
				approverPrincipal);

		// create member who has already been approved.
		PrincipalEntity approveePrincipal = createTestPrincipalEntity(
				VALID_USERNAME2, memberRole);
		MemberEntity approveeMember = createTestMemberEntity("Member Name",
				approveePrincipal);
		approveeMember.setType(MemberType.MEMBER);

		ServiceContext context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(approverPrincipal),
				getStore().getMemberStore().translate(approverMember));

		ApproveApplicant command = new ApproveApplicant(
				approveeMember.summerize());

		// confirm that approvee is in a INVALID state before invoking the action.
		assertThat(approveeMember.getType(), is(MemberType.MEMBER));
		assertThat(approveePrincipal.getRole(), is(memberRole));

		RefreshMemberResponse result = action.perform(command, context);

		assertNotNull(result);

		// check response is correct. Summary has not changed, but moderator
		// should not be able to edit user. Also check that the member
		// has been returned.
		assertThat(result.isSummaryChanged(), is(false));
		assertThat(result.isEditable(), is(false));
		assertThat(result.getMember().getId(), is(approveeMember.getId()));
		assertThat(result.getMember().getType(), is(MemberType.MEMBER));
		assertThat(result.getMember().getPrincipal().getId(),
				is(approveePrincipal.getId()));

		// check that the entity has not been changed.
		assertThat(approveeMember.getType(), is(MemberType.MEMBER));
		assertThat(approveePrincipal.getRole(), is(memberRole));
	}

}
