package com.earthabbey.cloister.server.service.action;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.earthabbey.cloister.spi.commands.Login;
import com.earthabbey.cloister.spi.responses.LoginResponse;

/**
 * Test unit for {@link LoginAction}.
 * 
 * @author richards
 * 
 */
public class LoginActionTest extends AbstractActionTest {

	private static final String INVALID_USERNAME = "invalid-user-name@nothing.com";

	private static final String INVALID_PASSWORD = "invalid-password";

	private LoginAction action;

	@Before
	public void beforeEachTest() throws IOException {							
		configureMocks();		
		action = new LoginAction();
	}

	@Test
	public void validUsernameAndPasswordWithLoginPermissionCanLogin() {
		Login credentials = new Login(VALID_USERNAME,
				VALID_PASSWORD);
		LoginResponse result = action.perform(credentials, context);
		assertNotNull(result);
		assertNotNull(result.getSession());
	}
	
	@Test
	public void validUsernameAndPasswordWithoutLoginPermissionCannotLogin(){
		permissions.clear();
		role.setPermissions(permissions);
		Login credentials = new Login(VALID_USERNAME,
				VALID_PASSWORD);
		LoginResponse result = action.perform(credentials, context);
		assertNotNull(result);
		assertNull(result.getSession());
	}

	@Test
	public void invalidUsernameCannotLogin() {
		Login credentials = new Login(INVALID_USERNAME,
				VALID_PASSWORD);
		LoginResponse result = action.perform(credentials, context);
		assertNotNull(result);
		assertNull(result.getSession());
	}

	@Test
	public void invalidPasswordCannotLogin() {
		Login credentials = new Login(VALID_USERNAME,
				INVALID_PASSWORD);
		LoginResponse result = action.perform(credentials, context);
		assertNotNull(result);
		assertNull(result.getSession());
	}
}
