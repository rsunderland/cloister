package com.earthabbey.cloister.server.service;

import static com.earthabbey.cloister.model.domain.Permission.MESSAGE_ANY;
import static com.earthabbey.cloister.model.domain.Permission.MESSAGE_MENTOR;
import static com.earthabbey.cloister.model.domain.Permission.READ_ANY;
import static com.earthabbey.cloister.model.domain.Permission.READ_APPLICANTS;
import static com.earthabbey.cloister.model.domain.Permission.READ_MEMBERS;
import static com.earthabbey.cloister.model.domain.Permission.READ_MENTOR;
import static com.earthabbey.cloister.model.domain.Permission.READ_SELF;
import static com.earthabbey.cloister.model.domain.Permission.UPDATE_ANY;
import static com.earthabbey.cloister.model.domain.Permission.UPDATE_SELF;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;

public class PermissionTesterTest {

	/**
	 * member who will be tested.
	 */
	private Member self;

	/**
	 * mentor of member being tested.
	 */
	private Member mentor;

	/**
	 * other member.
	 */
	private Member otherMember;

	/**
	 * mentor of other member
	 */
	private Member otherMentor;

	/**
	 * Member who is still an applicant.
	 */
	private Member applicant;

	@Before
	public void beforeEachTest() {

		mentor = createMemberAndPrincipal(1L, null, "mentor", "familyName",
				MemberType.MEMBER);

		self = createMemberAndPrincipal(2L, mentor, "actor", "familyname",
				MemberType.MEMBER);

		otherMentor = createMemberAndPrincipal(3L, null, "otherMentor",
				"familyname", MemberType.MEMBER);

		otherMember = createMemberAndPrincipal(5L, otherMentor, "otherMember",
				"familyname", MemberType.MEMBER);

		applicant = createMemberAndPrincipal(5L, null, "applicant",
				"familyname", MemberType.APPLICANT);
	}

	private Member createMemberAndPrincipal(Long id, Member mentor,
			String firstName, String lastName, MemberType type) {

		Role role  = new Role(3L, "role", null);
		
		Principal prinicpal = new Principal(firstName + "@test.com",
				"password", role.summerize());

		Member member = new Member(id, prinicpal.summerize(), type, firstName,
				lastName, null, (mentor == null) ? null : mentor.summerize(),
				null, null);

		prinicpal.setMember(member.summerize());

		return member;
	}

	@Test
	public void readAnyCanReadSelfOtherMemberAndApplicant() {
		canRead(self, READ_ANY);
		canRead(otherMember, READ_ANY);
		canRead(applicant, READ_ANY);
	}

	@Test
	public void readMemberCanReadOtherMemberButNotApplicant() {
		canRead(otherMember, READ_MEMBERS);
		cannotRead(applicant, READ_MEMBERS);
	}

	@Test
	public void readApplicantCanReadApplicantButNotOtherMember() {
		canRead(applicant, READ_APPLICANTS);
		cannotRead(otherMember, READ_APPLICANTS);
	}

	@Test
	public void readApplicantAndMemberCanReadApplicantAndOtherMember() {
		canRead(applicant, READ_APPLICANTS, READ_MEMBERS);
		canRead(otherMember, READ_APPLICANTS, READ_MEMBERS);
	}

	@Test
	public void readSelfCanReadSelfButNotOtherMember() {
		canRead(self, READ_SELF);
		cannotRead(otherMember, READ_SELF);
	}

	@Test
	public void readMentorCanReadOwnMentorButSelfOrOtherMentor() {
		cannotRead(self, READ_MENTOR);
		canRead(mentor, READ_MENTOR);
		cannotRead(otherMentor, READ_MENTOR);
	}

	@Test
	public void updateSelfCanUpdateSelfButNotOtherMember() {
		canUpdate(self, UPDATE_SELF);
		cannotUpdate(otherMember, UPDATE_SELF);
	}

	@Test
	public void updateAnyCanUpdateSelfAndOtherMember() {
		canUpdate(self, UPDATE_ANY);
		canUpdate(otherMember, UPDATE_ANY);
	}

	@Test
	public void messageMentorCanMessageMentorButNotOtherMemberOrOtherMentor() {
		canMessage(mentor, MESSAGE_MENTOR);
		cannotMessage(otherMember, MESSAGE_MENTOR);
		cannotMessage(otherMentor, MESSAGE_MENTOR);
	}

	@Test
	public void messageAnyCanMessageMentorAndOtherMember() {
		canMessage(mentor, MESSAGE_ANY);
		canMessage(otherMember, MESSAGE_ANY);
	}

	private void canRead(Member candidate, Permission... permissions) {
		PermissionTester tester = new PermissionTesterImpl(self,
				new HashSet<Permission>(Arrays.asList(permissions)));
		assertThat(tester.canRead(candidate.summerize()), is(true));
	}

	private void cannotRead(Member candidate, Permission... permissions) {
		PermissionTester tester = new PermissionTesterImpl(self,
				new HashSet<Permission>(Arrays.asList(permissions)));
		assertThat(tester.canRead(candidate.summerize()), is(false));
	}

	private void canUpdate(Member candidate, Permission... permissions) {
		PermissionTester tester = new PermissionTesterImpl(self,
				new HashSet<Permission>(Arrays.asList(permissions)));
		assertThat(tester.canUpdate(candidate.summerize()), is(true));
	}

	private void cannotUpdate(Member candidate, Permission... permissions) {
		PermissionTester tester = new PermissionTesterImpl(self,
				new HashSet<Permission>(Arrays.asList(permissions)));
		assertThat(tester.canUpdate(candidate.summerize()), is(false));
	}

	private void canMessage(Member candidate, Permission... permissions) {
		PermissionTester tester = new PermissionTesterImpl(self,
				new HashSet<Permission>(Arrays.asList(permissions)));
		assertThat(tester.canMessage(candidate.summerize()), is(true));
	}

	private void cannotMessage(Member candidate, Permission... permissions) {
		PermissionTester tester = new PermissionTesterImpl(self,
				new HashSet<Permission>(Arrays.asList(permissions)));
		assertThat(tester.canMessage(candidate.summerize()), is(false));
	}
}
