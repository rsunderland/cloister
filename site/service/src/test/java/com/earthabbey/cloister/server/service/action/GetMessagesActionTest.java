package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.earthabbey.cloister.model.domain.MessageSummary;
import com.earthabbey.cloister.spi.commands.GetMessages;
import com.earthabbey.cloister.spi.responses.GetMessagesResponse;

/**
 * Test Unit for GetMessagesAction.
 * 
 * @author richards
 * 
 */
public class GetMessagesActionTest extends AbstractActionTest {
	
	@Before
	public void beforeEachTest() {
		super.configureMocks();
	}

	@Test
	public void userCanReadTheirMessages() {
		when(context.getMember()).thenReturn(member1);
		GetMessagesAction action = new GetMessagesAction();
		GetMessages request = new GetMessages(member1.summerize());
		GetMessagesResponse response = action.perform(request, context);
		assertNotNull(response);
		assertNotNull(response.getMessageSummaries());
		assertThat(response.getMessageSummaries(), is(Arrays.asList(message1
				.summerize(), message2.summerize())));
	}
	
	@Test
	public void principalWithNoMemberCannotCanReadMessages() {	
		List<MessageSummary> summaries = new ArrayList<MessageSummary>();
		GetMessagesAction action = new GetMessagesAction();
		GetMessages request = new GetMessages(member1.summerize());
		GetMessagesResponse response = action.perform(request, context);
		assertNotNull(response);
		assertNotNull(response.getMessageSummaries());
		assertThat(response.getMessageSummaries(), is(summaries));
	}
}
