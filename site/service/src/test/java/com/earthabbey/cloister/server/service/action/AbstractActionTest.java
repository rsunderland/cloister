package com.earthabbey.cloister.server.service.action;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.model.domain.MessageState;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.model.store.EmailConfirmationStore;
import com.earthabbey.cloister.model.store.MemberStore;
import com.earthabbey.cloister.model.store.MessageStore;
import com.earthabbey.cloister.model.store.PrincipalStore;
import com.earthabbey.cloister.model.store.RoleStore;
import com.earthabbey.cloister.model.store.SessionStore;
import com.earthabbey.cloister.model.store.Store;
import com.earthabbey.cloister.server.service.ServiceContext;
import com.earthabbey.cloister.server.service.ServiceContextImpl;

public abstract class AbstractActionTest {

	protected static final String VALID_USERNAME = "one@test.com";

	protected static final String VALID_PASSWORD = "hpotter";

	protected static final String PASWORD_HASH = "$2a$10$3sacKnddEjJHxhUF0br3luPDNVsQ/JykdnbZA66vTNRHl87SeDHrK";

	@Mock
	protected ServiceContext context;

	@Mock
	protected MemberStore memberStore;

	@Mock
	protected PrincipalStore principalStore;

	@Mock
	protected SessionStore sessionStore;

	@Mock
	protected RoleStore roleStore;

	@Mock
	protected MessageStore messageStore;

	@Mock
	protected EmailConfirmationStore emailConfirmationStore;

	@Mock
	protected Message message1;

	@Mock
	protected Message message2;

	@Mock
	protected Store store;

	protected EnumSet<Permission> permissions;

	protected Role role;

	protected Principal principal;

	protected Session session;

	protected Member member1;

	protected Member member2;

	protected void configureMocks() {

		MockitoAnnotations.initMocks(this);
		permissions = EnumSet.noneOf(Permission.class);
		permissions.add(Permission.LOGIN);
		role = new Role(1L, "TestRole", permissions);

		principal = new Principal(VALID_USERNAME, PASWORD_HASH,
				role.summerize());

		Principal otherPrincipal = new Principal("ted", PASWORD_HASH,
				role.summerize());

		member1 = new Member(1L, principal.summerize(), MemberType.APPLICANT,
				"first", "sur", null, null, null, null);

		member2 = new Member(2L, otherPrincipal.summerize(),
				MemberType.APPLICANT, "second", "aoeu", null, null, null, null);

		session = new Session(1L, "token", false, null, null,
				principal.summerize(), member1.summerize(), null);
		message1 = new Message(3L, MessageState.READ, "title", "content",
				member1.summerize(), member1.summerize(), member2.summerize(), null);
		message2 = new Message(3L, MessageState.READ, "title", "content",
				member1.summerize(), member1.summerize(), member2.summerize(), null);

		when(context.getStore()).thenReturn(store);
		when(store.getMemberStore()).thenReturn(memberStore);
		when(store.getPrincipalStore()).thenReturn(principalStore);
		when(store.getSessionStore()).thenReturn(sessionStore);
		when(store.getRoleStore()).thenReturn(roleStore);
		when(store.getMessageStore()).thenReturn(messageStore);
		when(store.getEmailConfirmationStore()).thenReturn(
				emailConfirmationStore);
		when(context.getPrincipal()).thenReturn(principal);
		when(context.getSession()).thenReturn(session);

		when(principalStore.read(VALID_USERNAME, false)).thenReturn(principal);
		when(principalStore.read(VALID_USERNAME, true)).thenReturn(principal);
		when(roleStore.read(role.summerize(), true)).thenReturn(role);
		when(sessionStore.create(any(Session.class))).thenReturn(session);
		when(sessionStore.read(session, true)).thenReturn(session);
		when(messageStore.getMembersMessages(member1.summerize())).thenReturn(
				Arrays.asList(message1.summerize(), message2.summerize()));

		when(messageStore.read(message1.summerize(), true))
				.thenReturn(message1);
		when(messageStore.read(message1.summerize(), false)).thenReturn(
				message1);

		when(messageStore.read(message1.getId(), true)).thenReturn(message1);
		when(messageStore.read(message1.getId(), false)).thenReturn(message1);

	}

}
