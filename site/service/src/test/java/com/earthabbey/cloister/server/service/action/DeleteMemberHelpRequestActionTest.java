package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpRequestEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContextImpl;
import com.earthabbey.cloister.spi.commands.DeleteMemberHelpRequest;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

/**
 * Test unit for {@link CreateMemberHelpRequestAction}.
 */
public class DeleteMemberHelpRequestActionTest extends AbstractEntityManagerTest {

	private static final String VALID_USERNAME = "user@a.place.com";

	private DeleteMemberHelpRequestAction action;

	private PrincipalEntity principal;

	private MemberEntity member;

	private MemberHelpRequestEntity Request1;

	private MemberHelpRequestEntity Request2;

	private MemberHelpRequestEntity Request3;

	private static final String VALID_USERNAME2 = "admin@a.place.com";

	private static String EXPECTED_REQUEST_TEXT = "example help request";

	@Before
	public void beforeEachTest() throws IOException {
		super.initializePersistence();
		MockitoAnnotations.initMocks(this);
		action = new DeleteMemberHelpRequestAction();

		createStandardRoles();

		principal = createTestPrincipalEntity(VALID_USERNAME, memberRole);
		member = createTestMemberEntity("Member Name", principal);
		Request1 = createTestRequest(member, "Request One");
		Request2 = createTestRequest(member, "Request Two");
		Request3 = createTestRequest(member, "Request One");
	}

	/**
	 * Simple case where member adds their own Request.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under text
	 */
	@Test
	public void testAction() throws PermissionViolationException {

		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(principal), getStore()
						.getMemberStore().translate(member));

		DeleteMemberHelpRequest request = new DeleteMemberHelpRequest(Request2.getId());


		assertThat(member.getHelpRequests().size(), is(3));

		RefreshMemberResponse result = action.perform(request, context);

		// check response.
		assertNotNull(result);
		assertThat(result.isEditable(), is(true));
		assertThat(result.getMember().getId(), is(member.getId()));

		// check entities
		assertThat(member.getHelpRequests().size(), is(2));
		assertThat(member.getHelpRequests().get(0), is(Request1));
		assertThat(member.getHelpRequests().get(1), is(Request3));		
		
	}

	/**
	 * Prinical other than member used.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under text
	 */
	@Test
	public void testAdminModification() throws PermissionViolationException {

		PrincipalEntity admin = createTestPrincipalEntity(VALID_USERNAME2,
				adminRole);


		// create a session authorized by the admin, rather than the member
		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(admin), null);

		DeleteMemberHelpRequest request = new DeleteMemberHelpRequest(Request2.getId());

		assertThat(member.getHelpRequests().size(), is(3));

		RefreshMemberResponse result = action.perform(request, context);

		// check response.
		assertNotNull(result);
		assertThat(result.isEditable(), is(true));
		assertThat(result.getMember().getId(), is(member.getId()));

		// check entities
		assertThat(member.getHelpRequests().size(), is(2));
		assertThat(member.getHelpRequests().get(0), is(Request1));
		assertThat(member.getHelpRequests().get(1), is(Request3));	
	}

	/**
	 * Simple case where member adds their own Request.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under text
	 */
	@Test
	public void testActionWithoutPrivileges()
			throws PermissionViolationException {


		// create approver with member privileges (i.e., the have
		// Permission.UPDATE_SELF).
		PrincipalEntity principal2 = createTestPrincipalEntity(VALID_USERNAME2,
				memberRole);

		MemberEntity member2 = createTestMemberEntity("Member Name", principal2);

		// create session based on member 2, who does not have permission
		// to create an Request for member 1
		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(principal2),
				getStore().getMemberStore().translate(member2));

		DeleteMemberHelpRequest request = new DeleteMemberHelpRequest(Request2.getId());

		assertThat(member.getHelpRequests().size(), is(3));

		
		try {
			 action.perform(request, context);
			fail("Expected PermissionViolationException but did not find it.");
		} catch (PermissionViolationException e) {
			// nothing to do.
		}
	}
}
