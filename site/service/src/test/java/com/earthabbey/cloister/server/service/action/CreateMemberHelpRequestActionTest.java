package com.earthabbey.cloister.server.service.action;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpOfferEntity;
import com.earthabbey.cloister.model.entity.MemberHelpRequestEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.server.service.PermissionViolationException;
import com.earthabbey.cloister.server.service.ServiceContextImpl;
import com.earthabbey.cloister.spi.commands.CreateMemberHelpRequest;
import com.earthabbey.cloister.spi.responses.RefreshMemberResponse;

/**
 * Test unit for {@link CreateMemberHelpOfferAction}.
 */
public class CreateMemberHelpRequestActionTest extends AbstractEntityManagerTest {

	private static final String VALID_USERNAME = "user@a.place.com";

	private CreateMemberHelpRequestAction action;

	private static final String VALID_USERNAME2 = "admin@a.place.com";

	private static String EXPECTED_REQUEST_TEXT = "example help request";

	@Before
	public void beforeEachTest() throws IOException {
		super.initializePersistence();
		MockitoAnnotations.initMocks(this);
		action = new CreateMemberHelpRequestAction();

		createStandardRoles();
	}

	/**
	 * Simple case where member adds their own offer.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under text
	 */
	@Test
	public void testAction() throws PermissionViolationException {

		// create approver with member privileges (i.e., the have
		// Permission.UPDATE_SELF).
		PrincipalEntity principal = createTestPrincipalEntity(VALID_USERNAME,
				memberRole);

		MemberEntity member = createTestMemberEntity("Member Name", principal);

		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(principal), getStore()
						.getMemberStore().translate(member));

		CreateMemberHelpRequest request = new CreateMemberHelpRequest(
				member.summerize(), EXPECTED_REQUEST_TEXT);

		assertTrue(member.getHelpRequests().isEmpty());

		RefreshMemberResponse result = action.perform(request, context);

		// check response.
		assertNotNull(result);
		assertThat(result.isEditable(), is(true));
		assertThat(result.getMember().getId(), is(member.getId()));

		// check entities
		assertThat(member.getHelpRequests().size(), is(1));
		MemberHelpRequestEntity requestEntity = member.getHelpRequests().get(0);
		assertThat(requestEntity.getText(), is(EXPECTED_REQUEST_TEXT));
		assertThat(requestEntity.getRequestingMember(), is(member));
	}

	/**
	 * Prinical other than member used.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under text
	 */
	@Test
	public void testAdminModification() throws PermissionViolationException {

		PrincipalEntity admin = createTestPrincipalEntity(VALID_USERNAME2,
				adminRole);

		// create approver with member privileges (i.e., the have
		// Permission.UPDATE_SELF).
		PrincipalEntity principal = createTestPrincipalEntity(VALID_USERNAME,
				memberRole);

		MemberEntity member = createTestMemberEntity("Member Name", principal);

		// create a session authorized by the admin, rather than the member
		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(admin), null);

		CreateMemberHelpRequest request = new CreateMemberHelpRequest(
				member.summerize(), EXPECTED_REQUEST_TEXT);

		assertTrue(member.getHelpRequests().isEmpty());

		RefreshMemberResponse result = action.perform(request, context);

		// check response.
		assertNotNull(result);
		assertThat(result.isEditable(), is(true));
		assertThat(result.getMember().getId(), is(member.getId()));

		// check entities
		assertThat(member.getHelpRequests().size(), is(1));
		MemberHelpRequestEntity requestEntity = member.getHelpRequests().get(0);
		assertThat(requestEntity.getText(), is(EXPECTED_REQUEST_TEXT));
		assertThat(requestEntity.getRequestingMember(), is(member));
	}

	/**
	 * Simple case where member adds their own offer.
	 * 
	 * @throws PermissionViolationException
	 *             should not happen under text
	 */
	@Test
	public void testActionWithoutPrivileges()
			throws PermissionViolationException {

		// create approver with member privileges (i.e., the have
		// Permission.UPDATE_SELF).
		PrincipalEntity principal1 = createTestPrincipalEntity(VALID_USERNAME,
				memberRole);

		MemberEntity member1 = createTestMemberEntity("Member Name", principal1);

		// create approver with member privileges (i.e., the have
		// Permission.UPDATE_SELF).
		PrincipalEntity principal2 = createTestPrincipalEntity(VALID_USERNAME2,
				memberRole);

		MemberEntity member2 = createTestMemberEntity("Member Name", principal2);

		// create session based on member 2, who does not have permission
		// to create an offer for member 1
		ServiceContextImpl context = new ServiceContextImpl(getStore(), null,
				getStore().getPrincipalStore().translate(principal2),
				getStore().getMemberStore().translate(member2));

		CreateMemberHelpRequest request = new CreateMemberHelpRequest(
				member1.summerize(), EXPECTED_REQUEST_TEXT);

		assertTrue(member1.getHelpRequests().isEmpty());

		try {
			action.perform(request, context);
			fail("Expected PermissionViolationException but did not find it.");
		} catch (PermissionViolationException e) {
			// nothing to do.
		}
	}
}
