package com.earthabbey.cloister.model;

import static org.junit.Assert.assertNotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class StoreSingleton {

	private static final String PERSISTENCE_UNIT_NAME = "TEST_PERSISTENCE_UNIT";

	private static EntityManagerFactory emFactory;

	public static EntityManager getEntityManager() {
		if (emFactory == null) {
			emFactory = Persistence
					.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		EntityManager em = emFactory.createEntityManager();
		assertNotNull("Failed to create entity manager.", em);

		return em;
	}
}
