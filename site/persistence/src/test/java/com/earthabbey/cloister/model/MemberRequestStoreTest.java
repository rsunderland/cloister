package com.earthabbey.cloister.model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.store.MemberHelpRequestStore;
import com.earthabbey.cloister.model.store.MemberStore;

/**
 * Test unit for {@link MemberHelpRequestStore}.
 * 
 * Test the interaction between the members and the requests.
 * 
 * Initial back up contains a request, so request counts are offset by 1.
 * 
 * @author richards
 * 
 */
public class MemberRequestStoreTest extends AbstractEntityManagerTest {

	/**
	 * Store for the member.
	 */
	private MemberStore memberStore;

	/**
	 * Store for requests.
	 */
	private MemberHelpRequestStore memberRequestStore;

	/**
	 * Summary of member whose requests are being tested.
	 */
	private MemberSummary member0Summary;

	/**
	 * Detail of member whose requests are being tested.
	 */
	private Member member0;

	/**
	 * Restore backup and fetch details of exists members.
	 * 
	 * @throws IOException
	 */
	@Before
	public void beforeEachTest() throws IOException {
		super.initializePersistence();
		memberStore = getStore().getMemberStore();
		memberRequestStore = getStore().getMemberHelpRequestStore();
		member0 = createTestMember("member0", "principal0", "role0");
		member0Summary = member0.summerize();
	}

	/**
	 * Check that a request can be added.
	 */
	@Test
	public final void addingRequest() {
		member0 = memberStore.read(member0Summary, true);
		List<MemberHelpRequest> requests = member0.getHelpRequests();
		assertNotNull(requests);
		assertTrue(requests.isEmpty());

		assertThat(memberRequestStore.getSummaries().size(), is(0));
		MemberHelpRequest request1 = memberRequestStore
				.create(new MemberHelpRequest(null, "sampleText",
						member0Summary));
		assertThat(memberRequestStore.getSummaries().size(), is(1));

		member0 = memberStore.read(member0Summary, true);
		requests = member0.getHelpRequests();
		assertNotNull(requests);
		assertFalse(requests.isEmpty());

		MemberHelpRequest request1Read = memberRequestStore.read(request1,
				false);
		assertNotNull(request1Read);
		assertThat(request1Read.getText(), is("sampleText"));
	}

	/**
	 * Check that the details of a request can be updated several times.
	 */
	@Test
	public final void updateRequest() {
		memberRequestStore.create(new MemberHelpRequest(null, "r1",
				member0Summary));
		
		MemberHelpRequest request2 = memberRequestStore
				.create(new MemberHelpRequest(null, "r2", member0Summary));
		memberRequestStore.create(new MemberHelpRequest(null, "r3",
				member0Summary));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.size(), is(3));

		assertThat(memberRequestStore.getSummaries().size(), is(3));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(0).getText(), is("r1"));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(1).getText(), is("r2"));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(2).getText(), is("r3"));

		request2.setText("r2a");

		memberRequestStore.update(request2);

		assertThat(memberRequestStore.getSummaries().size(), is(3));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(0).getText(), is("r1"));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(1).getText(), is("r2a"));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(2).getText(), is("r3"));

		request2.setText("r2b");

		memberRequestStore.update(request2);

		assertThat(memberRequestStore.getSummaries().size(), is(3));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(0).getText(), is("r1"));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(1).getText(), is("r2b"));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(2).getText(), is("r3"));
	}

	/**
	 * Check that requests can be deleted, one by one.
	 */
	@Test
	public final void deleteRequest() {
		MemberHelpRequest request1 = memberRequestStore
				.create(new MemberHelpRequest(null, "r1", member0Summary));
		MemberHelpRequest request2 = memberRequestStore
				.create(new MemberHelpRequest(null, "r2", member0Summary));
		MemberHelpRequest request3 = memberRequestStore
				.create(new MemberHelpRequest(null, "r3", member0Summary));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.size(), is(3));
		assertThat(memberRequestStore.getSummaries().size(), is(3));

		memberRequestStore.delete(request2);
		assertThat(memberRequestStore.getSummaries().size(), is(2));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.size(), is(2));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(0).getText(), is("r1"));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(1).getText(), is("r3"));

		memberRequestStore.delete(request1);
		assertThat(memberRequestStore.getSummaries().size(), is(1));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.size(), is(1));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.get(0).getText(), is("r3"));

		memberRequestStore.delete(request3);
		assertThat(memberRequestStore.getSummaries().size(), is(0));
		assertThat(memberStore.read(member0Summary, true).getHelpRequests()
				.size(), is(0));
	}
}
