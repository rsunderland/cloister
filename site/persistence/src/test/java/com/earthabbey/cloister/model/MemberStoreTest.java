package com.earthabbey.cloister.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.store.MemberStore;
import com.earthabbey.cloister.model.store.PrincipalStore;

public class MemberStoreTest extends AbstractEntityManagerTest {

	@Test
	public void principalReferenceUpdated() {
		PrincipalStore principalStore = getStore().getPrincipalStore();
		MemberStore memberStore = getStore().getMemberStore();

		Role role = getStore().getRoleStore().create(
				new Role(null, "test", null));

		Principal principal = principalStore.create(new Principal("test-user",
				"hashedpassword", role.summerize()));

		assertNull(principal.getMember());

		Member member1 = memberStore.create(new Member(null, principal
				.summerize(), MemberType.APPLICANT, "Richard", "Sunderland",
				null, null, null, null));

		principal = principalStore.read(principal.summerize(), true);

		assertNotNull(principal.getMember());
		assertThat(principal.getMember(), is(member1.summerize()));
	}

	@Test
	public void canFindMembersWithPermission() {

		Member member1 = createTestMember("m1", Permission.MESSAGE_ANY);
		Member member2 = createTestMember("m2", Permission.ACCEPT_NEW_MENTEE);
		Member member3 = createTestMember("m3", Permission.MESSAGE_ANY);
		Member member4 = createTestMember("m4", Permission.ACCEPT_NEW_MENTEE);
		Member member5 = createTestMember("m5", Permission.MESSAGE_ANY);

		List<MemberEntity> members = getStore().getMemberStore()
				.findMembersWithPermission(Permission.ACCEPT_NEW_MENTEE);
		
		assertThat(members.size(),is(2));
		List<Long> ids = new ArrayList<Long>();
		for(MemberEntity member : members)
		{
			ids.add(member.getId());
		}
		
		assertFalse(ids.contains(member1.getId()));
		assertTrue(ids.contains(member2.getId()));
		assertFalse(ids.contains(member3.getId()));
		assertTrue(ids.contains(member4.getId()));
		assertFalse(ids.contains(member5.getId()));

	}

	private Member createTestMember(String baseName, Permission permisson) {
		
		HashSet<Permission> permissions = new HashSet<Permission>();
		permissions.add(permisson);
		Role role = getStore().getRoleStore().create(
				new Role(null, baseName + "Role", permissions));

		Principal principal = getStore().getPrincipalStore().create(
				new Principal(baseName + "Principal", "", role.summerize()));

		Member member = getStore().getMemberStore().create(
				new Member(null, principal.summerize(), MemberType.MEMBER,
						baseName + "First", baseName + "Second", baseName
								+ "Statement", null, null, null));

		return member;
	}
}
