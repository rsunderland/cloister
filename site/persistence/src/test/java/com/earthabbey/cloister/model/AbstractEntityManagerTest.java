package com.earthabbey.cloister.model;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.store.Store;

public abstract class AbstractEntityManagerTest {

	private Store store;
	private EntityManager em;

	@Before
	public void initializePersistence() throws IOException {
		em = StoreSingleton.getEntityManager();		
		store = new Store(em);		
		clearStore();
		startTransaction();				
	}

	private void clearStore() {
		startTransaction();
		store.deleteAll();
		commitTransaction();
	}


	@After
	public void afterEachTest() {
		commitTransaction();				
	}

	private void commitTransaction() {
		if (em != null && em.getTransaction().isActive()) {
			em.getTransaction().commit();
		}
	}
	
	private void startTransaction() {
		em.getTransaction().begin();
	}

	public EntityManager getEntityManager() {
		return em;
	}

	public Store getStore() {
		return store;
	}
	
	protected Role createTestRole(String roleName, Permission... permissions) {
		HashSet<Permission> permissionSet = new HashSet<Permission>();
		permissionSet.addAll(Arrays.asList(permissions));
		return store.getRoleStore().create(
				new Role(null, roleName, permissionSet));
	}
	
	protected Principal createTestPrincipal(String principalName, Role role) {
		return getStore().getPrincipalStore().create(
				new Principal(principalName, "", role.summerize()));
	}
	
	protected Member createTestMember(String memberName, String principalName,
			String roleName, Permission... permissions) {

		Role role = createTestRole(roleName, permissions);

		Principal principal = createTestPrincipal(principalName, role);

		return getStore().getMemberStore().create(
				new Member(null, principal.summerize(), MemberType.MEMBER,
						memberName, memberName, null, null, null, null));
	}
}
