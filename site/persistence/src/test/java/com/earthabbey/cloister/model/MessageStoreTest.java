package com.earthabbey.cloister.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.model.domain.MessageState;
import com.earthabbey.cloister.model.domain.MessageSummary;
import com.earthabbey.cloister.model.store.MemberStore;
import com.earthabbey.cloister.model.store.MessageStore;

public class MessageStoreTest extends AbstractEntityManagerTest {

	private MessageStore messageStore;
	private MemberSummary member0Summary;
	private MemberSummary member1Summary;

	@Before
	public void before() throws IOException {
		super.initializePersistence();

		Member member0 = createTestMember("member0", "principal0", "role0");
		member0Summary = member0.summerize();

		Member member1 = createTestMember("member1", "principal1", "role1");
		member1Summary = member1.summerize();

		Member member2 = createTestMember("member2", "principal2", "role2");

		messageStore = getStore().getMessageStore();

	}

	@Test
	public void canCreateAMessage() {

		Message message1 = createMessage(member0Summary, member1Summary,
				MessageState.NEW);

		Message message2 = createMessage(member1Summary, member0Summary,
				MessageState.READ);

		List<MessageSummary> allSummaries = messageStore.getSummaries();

		assertThat(allSummaries.size(), is(2));
		MessageSummary messageSummary1 = allSummaries.get(0);
		MessageSummary messageSummary2 = allSummaries.get(1);

		assertThat(
				messageSummary1.getTitle(),
				is("This is the title from member0 member0 to member1 member1"));
		assertThat(messageSummary1.isRead(), is(false));

		assertThat(
				messageSummary2.getTitle(),
				is("This is the title from member1 member1 to member0 member0"));
		assertThat(messageSummary2.isRead(), is(true));

		Message message1back = messageStore.read(messageSummary1, true);
		assertThat(message1back.getContent(), is(message1.getContent()));
		assertThat(message1back.getTitle(), is(message1.getTitle()));
		assertThat(message1back.getSender().getId(), is(message1.getSender()
				.getId()));
		assertThat(message1back.getRecipient().getId(), is(message1
				.getRecipient().getId()));
		assertThat(message1back.getSendTime(), is(message1.getSendTime()));
	}

	@Test
	public void canGetAllMessagesAddressedToARecipient() {

		createMessage(member0Summary, member1Summary, MessageState.NEW);
		createMessage(member0Summary, member1Summary, MessageState.READ);
		createMessage(member0Summary, member1Summary, MessageState.DELETED);
		createMessage(member1Summary, member0Summary, MessageState.NEW);
		createMessage(member1Summary, member0Summary, MessageState.READ);
		createMessage(member1Summary, member0Summary, MessageState.DELETED);

		List<MessageSummary> member0Mails = messageStore
				.getMembersMessages(member0Summary);
		
		// will not include the 'deleted' message.
		assertThat(member0Mails.size(), is(2));
	}

	@Test
	public void canDeleteMessages() {
		Message message0 = createMessage(member0Summary, member1Summary,
				MessageState.NEW);

		Message message1 = createMessage(member0Summary, member1Summary,
				MessageState.READ);

		Message message2 = createMessage(member0Summary, member1Summary,
				MessageState.READ);

		List<MessageSummary> member0Mails = messageStore
				.getMembersMessages(member1Summary);
		assertThat(member0Mails.size(), is(3));

		messageStore.deleteMessages(Arrays.asList(member0Mails.get(0),
				member0Mails.get(2)));

		assertThat(messageStore.getMembersMessages(member1Summary).size(),
				is(1));

		assertThat(messageStore.read(message0, true).getState(),
				is(MessageState.DELETED));
		assertThat(messageStore.read(message1, true).getState(),
				is(MessageState.READ));
		assertThat(messageStore.read(message2, true).getState(),
				is(MessageState.DELETED));
	}

	private Message createMessage(MemberSummary from, MemberSummary to,
			MessageState state) {

		Message message = new Message();
		message.setState(state);
		message.setOwner(to);
		message.setSender(from);
		message.setRecipient(to);
		message.setContent("This is the body from " + from.getDisplayText()
				+ " to " + to.getDisplayText());
		message.setTitle("This is the title from " + from.getDisplayText()
				+ " to " + to.getDisplayText());
		message.setSendTime(new Date());
		return messageStore.create(message);
	}
}
