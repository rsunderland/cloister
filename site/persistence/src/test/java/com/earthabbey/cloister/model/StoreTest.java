package com.earthabbey.cloister.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Collections;

import org.junit.Test;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.PrincipalSummary;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.store.MemberStore;

public class StoreTest extends AbstractEntityManagerTest {

	@Test
	public void canCreateTwoMembers() {
		MemberStore memberStore = getStore().getMemberStore();

		Role role = getStore().getRoleStore().create(new Role(null, "test", null));
		Principal principal = getStore().getPrincipalStore().create(new Principal("10","tp", role.summerize()));
		Member member1 = memberStore.create(new Member(null, principal.summerize(),
				MemberType.APPLICANT, "Richard", "Sunderland", null, null,
				null, null));
		Member member2 = memberStore.create(new Member(null, principal.summerize(),
				MemberType.MEMBER, "Catherine", "Sunderland", null, null, null,
				null));
		assertThat(member1.getFirstName(), is("Richard"));
		assertThat(member1.getSurname(), is("Sunderland"));
		assertThat(member1.getType(), is(MemberType.APPLICANT));
		assertThat(member2.getFirstName(), is("Catherine"));
		assertThat(member2.getSurname(), is("Sunderland"));
		assertThat(member2.getType(), is(MemberType.MEMBER));
	}
}
