package com.earthabbey.cloister.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.MemberType;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.store.MemberHelpOfferStore;
import com.earthabbey.cloister.model.store.MemberStore;

/**
 * Test unit for {@link MemberHelpOfferStore}.
 * 
 * Test the interaction between the members and the offers.
 * 
 * Initial back up contains a offer, so offer counts are offset by 1.
 * 
 * @author richards
 * 
 */
public class MemberOfferStoreTest extends AbstractEntityManagerTest {

	/**
	 * Store for the member.
	 */
	private MemberStore memberStore;

	/**
	 * Store for offers.
	 */
	private MemberHelpOfferStore offerStore;

	/**
	 * Summary of member whose offers are being tested.
	 */
	private MemberSummary summary0;

	/**
	 * Detail of member whose offers are being tested.
	 */
	private Member member0;

	/**
	 * Restore backup and fetch details of exists members.
	 * 
	 * @throws IOException
	 */
	@Before
	public void beforeEachTest() throws IOException {
		super.initializePersistence();
		memberStore = getStore().getMemberStore();
		offerStore = getStore().getMemberHelpOfferStore();

		member0 = createTestMember("member0", "principal0", "role0");
		summary0 = member0.summerize();
	}



	/**
	 * Check that a offer can be added.
	 */
	@Test
	public final void addingOffer() {
		member0 = memberStore.read(summary0, true);
		List<MemberHelpOffer> offers = member0.getHelpOffers();
		assertNotNull(offers);
		assertTrue(offers.isEmpty());

		assertThat(offerStore.getSummaries().size(), is(0));
		MemberHelpOffer offer1 = offerStore.create(new MemberHelpOffer(null,
				"sampleText", summary0));
		assertThat(offerStore.getSummaries().size(), is(1));

		member0 = memberStore.read(summary0, true);
		offers = member0.getHelpOffers();
		assertNotNull(offers);
		assertFalse(offers.isEmpty());

		MemberHelpOffer offer1Read = offerStore.read(offer1, false);
		assertNotNull(offer1Read);
		assertThat(offer1Read.getText(), is("sampleText"));
	}

	/**
	 * Check that the details of a offer can be updated several times.
	 */
	@Test
	public final void updateOffer() {
		assertThat(memberStore.read(summary0, true).getHelpOffers().size(),
				is(0));
		offerStore.create(new MemberHelpOffer(null, "r1", summary0));
		assertThat(memberStore.read(summary0, true).getHelpOffers().size(),
				is(1));
		MemberHelpOffer offer2 = offerStore.create(new MemberHelpOffer(null,
				"r2", summary0));
		offerStore.create(new MemberHelpOffer(null, "r3", summary0));
		assertThat(memberStore.read(summary0, true).getHelpOffers().size(),
				is(3));

		assertThat(offerStore.getSummaries().size(), is(3));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(0)
				.getText(), is("r1"));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(1)
				.getText(), is("r2"));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(2)
				.getText(), is("r3"));

		offer2.setText("r2a");

		offerStore.update(offer2);

		assertThat(offerStore.getSummaries().size(), is(3));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(0)
				.getText(), is("r1"));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(1)
				.getText(), is("r2a"));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(2)
				.getText(), is("r3"));

		offer2.setText("r2b");

		offerStore.update(offer2);

		assertThat(offerStore.getSummaries().size(), is(3));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(0)
				.getText(), is("r1"));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(1)
				.getText(), is("r2b"));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(2)
				.getText(), is("r3"));
	}

	/**
	 * Check that offers can be deleted, one by one.
	 */
	@Test
	public final void deleteOffer() {
		MemberHelpOffer offer1 = offerStore.create(new MemberHelpOffer(null,
				"r1", summary0));
		MemberHelpOffer offer2 = offerStore.create(new MemberHelpOffer(null,
				"r2", summary0));
		MemberHelpOffer offer3 = offerStore.create(new MemberHelpOffer(null,
				"r3", summary0));
		assertThat(memberStore.read(summary0, true).getHelpOffers().size(),
				is(3));
		assertThat(offerStore.getSummaries().size(), is(3));

		offerStore.delete(offer2);
		assertThat(offerStore.getSummaries().size(), is(2));
		assertThat(memberStore.read(summary0, true).getHelpOffers().size(),
				is(2));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(0)
				.getText(), is("r1"));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(1)
				.getText(), is("r3"));

		offerStore.delete(offer1);
		assertThat(offerStore.getSummaries().size(), is(1));
		assertThat(memberStore.read(summary0, true).getHelpOffers().size(),
				is(1));
		assertThat(memberStore.read(summary0, true).getHelpOffers().get(0)
				.getText(), is("r3"));

		offerStore.delete(offer3);
		assertThat(offerStore.getSummaries().size(), is(0));
		assertThat(memberStore.read(summary0, true).getHelpOffers().size(),
				is(0));
	}
}
