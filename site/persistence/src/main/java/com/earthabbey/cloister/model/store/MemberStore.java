package com.earthabbey.cloister.model.store;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.PrincipalSummary;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpOfferEntity;
import com.earthabbey.cloister.model.entity.MemberHelpRequestEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;

public class MemberStore extends
		EntityStore<MemberEntity, Long, Member, MemberSummary> {

	public MemberStore(EntityManager entityManager) {
		super(entityManager, MemberEntity.class, new MemberTranslator(
				entityManager));
	}

	public String getQueryString() {
		return "SELECT e FROM " + entityClass.getSimpleName()
				+ " e ORDER BY e.surname";
	}

	/**
	 * Find all the members with a permission.
	 * 
	 * @param permission
	 *            the permission that the members must have.
	 * @return a list (possibly empty) of all the members that have a particular
	 *         permission.
	 */
	public List<MemberEntity> findMembersWithPermission(Permission permission) {

		TypedQuery<MemberEntity> query = entityManager
				.createQuery(
						"SELECT DISTINCT OBJECT(m) FROM " +
						"MemberEntity m, " +
						"IN(m.principal.role.permissions) AS p " +
						"WHERE p=?1",
						MemberEntity.class);

		query.setParameter(1, permission);

		return query.getResultList();

	}

	/**
	 * Set a members mentor.
	 * 
	 * @param mentee
	 *            the member who's mentor is being changed.
	 * @param mentor
	 *            (may be null, indicating no mentor).
	 */
	public void setMentor(Member mentee, MemberSummary mentor) {

		MemberEntity menteeEntity = entityManager.find(MemberEntity.class,
				mentee.getId());
		if (menteeEntity == null) {
			throw new IllegalArgumentException(
					"Attempting to set mentor for none existant member "
							+ mentee);
		}
		MemberEntity mentorEntity = null;
		if (mentor != null) {
			mentorEntity = entityManager.find(MemberEntity.class,
					mentor.getId());
			if (mentorEntity == null) {
				throw new IllegalArgumentException("Attempting to set mentor  "
						+ mentee + " to non existant mentor " + mentor);
			}
		}
		menteeEntity.setMentor(mentorEntity);
	}
	

	public void deleteAll() {

		entityManager.createQuery(
				"UPDATE " + entityClass.getSimpleName()
						+ " e SET e.mentor=null").executeUpdate();
		entityManager.createQuery("DELETE FROM " + entityClass.getSimpleName())
				.executeUpdate();
	}

}

class MemberTranslator implements Translator<MemberEntity, Member> {

	private EntityManager entityManager;

	public MemberTranslator(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public Member createDomainObject(MemberEntity entityRecord) {
		return createDomainObject(entityRecord, true);
	}

	private Member createDomainObject(MemberEntity entityRecord,
			boolean loadMentorSummaryIfAvailable) {

		MemberSummary mentorSummary = summerizeMentor(entityRecord,
				loadMentorSummaryIfAvailable);

		List<MemberHelpOffer> helpOffers = summarizeHelpOffers(entityRecord
				.getHelpOffers());
		List<MemberHelpRequest> helpRequests = summarizeHelpRequests(entityRecord
				.getHelpRequests());

		return new Member(entityRecord.getId(), entityRecord.getPrincipal()
				.summerize(), entityRecord.getType(),
				entityRecord.getFirstName(), entityRecord.getSurname(),
				entityRecord.getWayOfLifeSummary(), mentorSummary,
				helpOffers, helpRequests);
	}

	private MemberSummary summerizeMentor(MemberEntity entityRecord,
			boolean loadMentorSummaryIfAvailable) {
		MemberSummary mentorSummary = null;

		MemberEntity mentor = entityRecord.getMentor();

		if (mentor != null && loadMentorSummaryIfAvailable) {
			mentorSummary = mentor.summerize();
		}
		return mentorSummary;
	}

	private List<MemberHelpRequest> summarizeHelpRequests(
			List<MemberHelpRequestEntity> helpRequests) {
		List<MemberHelpRequest> result = new ArrayList<MemberHelpRequest>();
		if (helpRequests != null) {
			for (MemberHelpRequestEntity entity : helpRequests) {
				result.add(entity.createDomainObject());
			}
		}
		return result;
	}

	private List<MemberHelpOffer> summarizeHelpOffers(
			List<MemberHelpOfferEntity> helpOffers) {
		List<MemberHelpOffer> result = new ArrayList<MemberHelpOffer>();
		if (helpOffers != null) {
			for (MemberHelpOfferEntity entity : helpOffers) {
				result.add(entity.createDomainObject());
			}
		}
		return result;
	}

	@Override
	public void updateEntityRecord(Member domainObject,
			MemberEntity entityRecord) {
		// mentor is not updated, see MemberStore#setMentor
		// helpRequests is not updated, see HelpRequestStore
		// helpOffers is not updated, see HelpOfferStore
		entityRecord.setType(domainObject.getType());
		entityRecord.setFirstName(domainObject.getFirstName());
		entityRecord.setSurname(domainObject.getSurname());
		entityRecord
				.setWayOfLifeSummary(domainObject.getWayOfLifeSummary());
	}

	@Override
	public MemberEntity createEntityRecord(Member domainObject) {
		MemberEntity memberEntity = new MemberEntity();
		// the id is not forwarded, as it is allocated by the database.
		updateEntityRecord(domainObject, memberEntity);

		PrincipalEntity principal = entityManager.find(PrincipalEntity.class,
				domainObject.getPrincipal().getId());
		if (principal == null) {
			throw new IllegalArgumentException("Could not find principal for "
					+ domainObject);
		}

		memberEntity.setPrincipal(principal);
		return memberEntity;
	}
}
