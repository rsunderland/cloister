package com.earthabbey.cloister.model.store;

import javax.persistence.EntityManager;

import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberHelpRequestSummary;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpRequestEntity;

public class MemberHelpRequestStore
		extends
		EntityStore<MemberHelpRequestEntity, Long, MemberHelpRequest, MemberHelpRequestSummary> {

	public MemberHelpRequestStore(EntityManager entityManager) {
		super(entityManager, MemberHelpRequestEntity.class,
				new MemberHelpRequestTranslator(entityManager));
	}
}

class MemberHelpRequestTranslator implements
		Translator<MemberHelpRequestEntity, MemberHelpRequest> {

	private EntityManager entityManager;
	
	public MemberHelpRequestTranslator(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	@Override
	public MemberHelpRequest createDomainObject(
			MemberHelpRequestEntity entityRecord) {
		return entityRecord.createDomainObject();
	}

	@Override
	public MemberHelpRequestEntity createEntityRecord(
			MemberHelpRequest domainObject) {
		MemberHelpRequestEntity entity = new MemberHelpRequestEntity();

		updateEntityRecord(domainObject, entity);
		return entity;
	}

	@Override
	public void updateEntityRecord(MemberHelpRequest domainObject,
			MemberHelpRequestEntity entityRecord) {
		// id is not forwarded as database with either leave it unchanged
		// on allocated it if null.
		MemberEntity entity = entityManager.find(MemberEntity.class,
				domainObject.getRequestingMember().getId());
		if (entity == null) {
			throw new IllegalArgumentException("Could not find member(id="
					+ domainObject.getRequestingMember().getId()
					+ ") for requset (id=" + domainObject.getId() + ")");
		}
		entityRecord.setRequestingMember(entity);
		entityRecord.setText(domainObject.getText());

	}
}
