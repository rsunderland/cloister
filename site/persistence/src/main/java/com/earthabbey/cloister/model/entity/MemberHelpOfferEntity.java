package com.earthabbey.cloister.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpOfferSummary;

@Entity
@Table(name = "member_help_offers")
public class MemberHelpOfferEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private MemberEntity offeringMember;

	private String text;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "member_help_offer_seq")
	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	public MemberEntity getOfferingMember() {
		return offeringMember;
	}

	public void setOfferingMember(MemberEntity offeringMember) {
		this.offeringMember = offeringMember;
	}

	@PreRemove
	public void tidyUpReference() {
		this.offeringMember.getHelpOffers().remove(this);
	}

	@PostPersist
	public void setBackReference() {
		this.offeringMember.getHelpOffers().add(this);
	}

	@Lob
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String toString() {
		ToStringBuilder bld = new ToStringBuilder(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
		bld.append("id", getId());
		bld.append("sender", offeringMember.getId());
		bld.append("content", text);
		return bld.toString();
	}

	public MemberHelpOfferSummary summerize() {
		return new MemberHelpOfferSummary(getId(), text,
				offeringMember.summerize());
	}

	public MemberHelpOffer createDomainObject() {
		return new MemberHelpOffer(getId(), text, offeringMember.summerize());
	}
}
