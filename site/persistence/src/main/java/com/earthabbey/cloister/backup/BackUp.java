package com.earthabbey.cloister.backup;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpOfferEntity;
import com.earthabbey.cloister.model.entity.MessageEntity;

@XmlRootElement
public class BackUp {

	private List<MemberEntity> members;

	private List<MessageEntity> messages;

	public BackUp() {

	}

	@XmlElementWrapper(name = "members")
	@XmlElement(name = "Member")
	public List<MemberEntity> getMembers() {
		if (members == null) {
			members = new ArrayList<MemberEntity>();
		}
		return members;
	}

	public void setMembers(List<MemberEntity> members) {
		this.members = members;
	}


	@XmlElementWrapper(name = "messages")
	@XmlElement(name = "Message")
	public List<MessageEntity> getMessages() {
		if (messages == null) {
			messages = new ArrayList<MessageEntity>();
		}
		return messages;
	}

	public void setMessages(List<MessageEntity> messages) {
		this.messages = messages;
	}

}
