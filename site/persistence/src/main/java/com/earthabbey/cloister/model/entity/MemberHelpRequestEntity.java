package com.earthabbey.cloister.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.MemberHelpRequestSummary;

@Entity
@Table(name = "member_help_request")
public class MemberHelpRequestEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private MemberEntity requestingMember;

	private String text;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="member_help_request_seq")
	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	public MemberEntity getRequestingMember() {
		return requestingMember;
	}

	public void setRequestingMember(MemberEntity requestingMember) {
		this.requestingMember = requestingMember;
	}

	@PostPersist
	public void setBackReference() {
		this.requestingMember.getHelpRequests().add(this);
	}

	@PreRemove
	public void tidyUpReference() {
		this.requestingMember.getHelpRequests().remove(this);
	}

	@Lob
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String toString() {
		ToStringBuilder bld = new ToStringBuilder(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
		bld.append("id", getId());
		bld.append("sender", requestingMember.getId());
		bld.append("content", text);
		return bld.toString();
	}

	public MemberHelpRequestSummary summerize() {
		return new MemberHelpRequestSummary(getId(), text,
				requestingMember.summerize());
	}

	public MemberHelpRequest createDomainObject() {
		return new MemberHelpRequest(getId(), text,
				requestingMember.summerize());
	}
}
