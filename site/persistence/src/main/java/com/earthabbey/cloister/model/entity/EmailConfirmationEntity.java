package com.earthabbey.cloister.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.earthabbey.cloister.model.domain.EmailConfirmation;

@Entity
@Table(name = "email_confirmation")
public class EmailConfirmationEntity {

	private String id;

	private String emailAddress;

	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public EmailConfirmation summerize() {
		return new EmailConfirmation(getId(), emailAddress);
	}
	
	@Override
	public String toString()
	{
		ToStringBuilder bld = new ToStringBuilder(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
		bld.append("id", getId());
		bld.append("emailAddress", emailAddress);		
		return bld.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailConfirmationEntity other = (EmailConfirmationEntity) obj;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
