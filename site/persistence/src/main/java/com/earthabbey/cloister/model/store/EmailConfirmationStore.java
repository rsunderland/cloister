package com.earthabbey.cloister.model.store;

import javax.persistence.EntityManager;

import com.earthabbey.cloister.model.domain.EmailConfirmation;
import com.earthabbey.cloister.model.entity.EmailConfirmationEntity;

public class EmailConfirmationStore
		extends
		EntityStore<EmailConfirmationEntity, String, EmailConfirmation, EmailConfirmation> {

	public EmailConfirmationStore(EntityManager entityManager) {
		super(entityManager, EmailConfirmationEntity.class,
				new EmailConfirmationTranslator());
	}

}

class EmailConfirmationTranslator implements
		Translator<EmailConfirmationEntity, EmailConfirmation> {

	@Override
	public EmailConfirmation createDomainObject(
			EmailConfirmationEntity entityRecord) {
		return new EmailConfirmation(entityRecord.getId(),
				entityRecord.getEmailAddress());
	}

	@Override
	public EmailConfirmationEntity createEntityRecord(
			EmailConfirmation domainObject) {
		EmailConfirmationEntity entity = new EmailConfirmationEntity();
		entity.setId(domainObject.getToken());
		entity.setEmailAddress(domainObject.getEmailAddress());
		return entity;
	}

	@Override
	public void updateEntityRecord(EmailConfirmation domainObject,
			EmailConfirmationEntity entityRecord) {

		throw new UnsupportedOperationException(
				"Email Confirmation Records cannot be updated.");
	}

}
