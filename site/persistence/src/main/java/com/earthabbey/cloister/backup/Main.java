package com.earthabbey.cloister.backup;


import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpOfferEntity;
import com.earthabbey.cloister.model.entity.MessageEntity;


public class Main {
	
	public static void main(String[] args) throws JAXBException, IOException
	{
		 
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("TEST_PERSISTENCE_UNIT");
		
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		
		javax.xml.bind.JAXBContext jaxbContext = JAXBContext.newInstance(BackUp.class);
 
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        
        MemberHelpOfferEntity offer = new MemberHelpOfferEntity();
        offer.setId(560L);
        offer.setText("aoeu");
        

        MemberHelpOfferEntity offer2 = new MemberHelpOfferEntity();
        offer2.setId(560L);
        offer2.setText("aoeu");
        MemberEntity entity = new MemberEntity();
        
        entity.setId(67L);
        entity.setFirstName("<p>Richard</p>");
        entity.setSurname("Sunderland");
        entity.setHelpOffers(Arrays.asList(offer, offer2));
        
        MessageEntity message = new MessageEntity();
        message.setId(8989L);
        message.setOwner(entity);
        message.setSender(entity);
        message.setRecipient(entity);
        message.setTitle("aoeu");
        message.setContent("aoeuaoeuaoeu");
        em.getTransaction().commit();
    	
    	
        
        
        em.getTransaction().begin();
        
        StringWriter dest = new StringWriter();
        BackUp up = new BackUp();
        up.getMembers().add(entity);
        up.getMessages().add(message);
        marshaller.marshal(up, dest);
        dest.close();
        em.getTransaction().commit();
        
        
        StringReader reader = new StringReader(dest.toString());
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        BackUp backup2 = (BackUp)unmarshaller.unmarshal(reader);
        
        System.out.println(backup2.getMembers());
        System.out.println(backup2.getMessages());
        
	}
}
