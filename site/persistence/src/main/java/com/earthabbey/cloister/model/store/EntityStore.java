package com.earthabbey.cloister.model.store;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.earthabbey.cloister.model.domain.DomainObject;
import com.earthabbey.cloister.model.domain.IdentifiableObject;
import com.earthabbey.cloister.model.domain.SummaryObject;

/**
 * @author richards
 * 
 * @param <ER>
 *            entity record.
 * @param <DO>
 *            domain object.
 */
public class EntityStore<ER, K, DO extends DomainObject<K, SO>, SO extends SummaryObject<K>> {

	private final Translator<ER, DO> translator;
	protected final Class<ER> entityClass;
	protected EntityManager entityManager;

	public EntityStore(EntityManager entityManager, Class<ER> entityClass,
			Translator<ER, DO> translator) {
		this.entityManager = entityManager;
		this.translator = translator;
		this.entityClass = entityClass;
	}

	@Deprecated
	public synchronized DO create(DO domainObject) {
		ER entityRecord = translator.createEntityRecord(domainObject);
		entityManager.persist(entityRecord);
		entityManager.flush();
		DO postUpdateDomainObject = translator.createDomainObject(entityRecord);
		return postUpdateDomainObject;
	}
	
	public synchronized ER createEntity(DO domainObject) {
		DO postUpdateDomainObject = create(domainObject);
		return readEntity(postUpdateDomainObject.getId(), true);
	}

	public synchronized List<DO> readAll() {

		Query query = entityManager.createQuery(getQueryString());

		@SuppressWarnings("unchecked")
		List<DO> result = translate((Collection<ER>) query.getResultList());

		return result;

	}

	public List<DO> translate(Collection<ER> entityRecords) {
		List<DO> result = new ArrayList<DO>();
		for (ER entityRecord : entityRecords) {
			result.add(translator.createDomainObject(entityRecord));
		}
		return result;
	}

	public DO translate(ER entityRecord) {
		return translator.createDomainObject(entityRecord);
	}


	protected String getQueryString() {
		return "SELECT e FROM " + entityClass.getSimpleName() + " e";
	}

	public synchronized List<DO> createAll(List<DO> domainObjects) {

		List<DO> result = new ArrayList<DO>();
		for (DO domainObject : domainObjects) {
			ER entityRecord = translator.createEntityRecord(domainObject);
			entityManager.persist(entityRecord);
			entityManager.flush();
			DO postUpdateDomainObject = translator
					.createDomainObject(entityRecord);
			result.add(postUpdateDomainObject);
		}

		return result;

	}

	@Deprecated
	public synchronized DO read(IdentifiableObject<K> handle, boolean mustExist) {
		return read(handle.getId(), mustExist);
	}

	@Deprecated
	public synchronized DO read(K key, boolean mustExist) {

		DO domainObject = null;

		ER entityRecord = entityManager.find(entityClass, key);
		if (entityRecord == null) {
			if (mustExist) {
				throw new IllegalArgumentException("Not record found with id "
						+ key);
			}
		} else {
			domainObject = translator.createDomainObject(entityRecord);
		}

		return domainObject;

	}
		
	public synchronized ER readEntity(K key, boolean mustExist) {
		ER entityRecord = entityManager.find(entityClass, key);
		if (entityRecord == null) {
			if (mustExist) {
				throw new IllegalArgumentException("Not record found with id "
						+ key);
			}
		}
		return entityRecord;
	}

	public synchronized void update(DO domainObject) {

		ER entityRecord = entityManager.find(entityClass, domainObject.getId());
		if (entityRecord == null) {
			throw new IllegalArgumentException(
					"Could not find m detail to update for " + domainObject);
		}
		translator.updateEntityRecord(domainObject, entityRecord);
		entityManager.flush();

	}
	
	public synchronized void flush() {
		entityManager.flush();
	}

	@Deprecated
	public synchronized void delete(DO domainObject) {
		ER entityRecord = entityManager.find(entityClass, domainObject.getId());
		entityManager.remove(entityRecord);
	}
	
	public synchronized void deleteEntity(ER entityRecord) {	
		entityManager.remove(entityRecord);
	}

	public synchronized void deleteAll() {

		entityManager.createQuery("DELETE FROM " + entityClass.getSimpleName())
				.executeUpdate();

	}

	protected List<SO> summerise( Collection<DO> dos) {
		List<SO> result = new ArrayList<SO>();
		for (DO domainObject : dos) {
			result.add(domainObject.summerize());
		}
		return result;
	}

	public synchronized List<SO> getSummaries() {
		return summerise(readAll());
	}
}
