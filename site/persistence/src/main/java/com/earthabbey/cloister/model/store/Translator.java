package com.earthabbey.cloister.model.store;

import com.earthabbey.cloister.model.domain.DomainObject;

/**
 * Translator.
 * 
 * @author richards
 * 
 * @param <ER>
 *            entity record.
 * @param <DO>
 *            domain object.
 */
public interface Translator<ER, DO extends DomainObject<?, ?>> {

	public DO createDomainObject(ER entityRecord);

	public void updateEntityRecord(DO domainObject, ER entityRecord);

	public ER createEntityRecord(DO domainObject);
}
