package com.earthabbey.cloister.model.store;

import javax.persistence.EntityManager;

import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpOfferSummary;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MemberHelpOfferEntity;

public class MemberHelpOfferStore
		extends
		EntityStore<MemberHelpOfferEntity, Long, MemberHelpOffer, MemberHelpOfferSummary> {

	public MemberHelpOfferStore(EntityManager entityManager) {
		super(entityManager, MemberHelpOfferEntity.class, new MemberHelpOfferTranslator(entityManager));
	}

}

class MemberHelpOfferTranslator implements
		Translator<MemberHelpOfferEntity, MemberHelpOffer> {
	
	private EntityManager entityManager;
	

	public MemberHelpOfferTranslator(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	@Override
	public MemberHelpOffer createDomainObject(MemberHelpOfferEntity entityRecord) {
		return entityRecord.createDomainObject();
	}

	@Override
	public MemberHelpOfferEntity createEntityRecord( MemberHelpOffer domainObject) {
		MemberHelpOfferEntity entity = new MemberHelpOfferEntity();
		updateEntityRecord( domainObject, entity);
		return entity;
	}

	@Override
	public void updateEntityRecord( MemberHelpOffer domainObject,
			MemberHelpOfferEntity entityRecord) {
		// id is not forwarded as database with either leave it unchanged
		// on allocated it if null.
		MemberEntity entity = entityManager.find(MemberEntity.class, domainObject
				.getOfferingMember().getId());
		if (entity == null) {
			throw new IllegalArgumentException("Could not find member(id="
					+ domainObject.getOfferingMember().getId()
					+ ") for offer (id=" + domainObject.getId() + ")");
		}
		entityRecord.setOfferingMember(entity);
		entityRecord.setText(domainObject.getText());
	}
}
