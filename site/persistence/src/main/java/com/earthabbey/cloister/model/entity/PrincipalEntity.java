package com.earthabbey.cloister.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.earthabbey.cloister.model.domain.PrincipalSummary;

@Entity
@Table(name = "principals")
public class PrincipalEntity {

	private String id;

	private String passwordHash;

	private MemberEntity member;

	private RoleEntity role;

	public PrincipalEntity() {

	}

	public PrincipalEntity(String username, String passwordHash,
			MemberEntity member, RoleEntity role) {
		this.id = username;
		this.passwordHash = passwordHash;
		this.member = member;
		this.role = role;
	}

	@Id
	@XmlAttribute
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlAttribute
	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	@OneToOne
	public MemberEntity getMember() {
		return member;
	}

	public void setMember(MemberEntity member) {
		this.member = member;
	}

	@ManyToOne
	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	public PrincipalSummary summerize() {
		return new PrincipalSummary(getId());
	}

	public String toString() {
		ToStringBuilder bld = new ToStringBuilder(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
		bld.append("username", getId());
		if (member != null) {
			bld.append("member", member.getId());
		}
		return bld.toString();
	}

}
