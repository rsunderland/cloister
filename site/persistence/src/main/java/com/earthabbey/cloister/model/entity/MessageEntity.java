package com.earthabbey.cloister.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.earthabbey.cloister.model.domain.MessageState;
import com.earthabbey.cloister.model.domain.MessageSummary;

@Entity
@Table(name = "messages")
public class MessageEntity {

	private Long id;

	/**
	 * Member who owns message. May be same as sender or recipient.
	 */
	private MemberEntity owner;

	/**
	 * Member who sent message. May or may not be owner.
	 */
	private MemberEntity sender;

	/**
	 * Member who sent message. May or may not be owner.
	 */
	private MemberEntity recipient;
	private Date sendTime;
	private MessageState state;

	private String title;
	private String content;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "message_seq")
	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@XmlIDREF
	@XmlAttribute
	public MemberEntity getOwner() {
		return owner;
	}

	public void setOwner(MemberEntity owner) {
		this.owner = owner;
	}

	@ManyToOne
	@XmlIDREF
	@XmlAttribute
	public MemberEntity getSender() {
		return sender;
	}

	public void setSender(MemberEntity sender) {
		this.sender = sender;
	}

	@ManyToOne
	@XmlIDREF
	@XmlAttribute
	public MemberEntity getRecipient() {
		return recipient;
	}

	public void setRecipient(MemberEntity recipient) {
		this.recipient = recipient;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@XmlAttribute
	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public MessageState getState() {
		return state;
	}

	public void setState(MessageState state) {
		this.state = state;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Lob
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public MessageSummary summerize() {
		return new MessageSummary(getId(), title, state == MessageState.READ,
				sender.summerize(), recipient.summerize());
	}

	public String toString() {
		ToStringBuilder bld = new ToStringBuilder(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
		bld.append("id", getId());
		bld.append("owner", owner.getId());
		bld.append("sender", sender.getId());
		bld.append("recipient", recipient.getId());
		bld.append("title", title);
		bld.append("content", content);
		bld.append("state", state);
		bld.append("sendTime", sendTime);
		return bld.toString();
	}
}
