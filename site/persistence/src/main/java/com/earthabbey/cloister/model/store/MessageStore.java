package com.earthabbey.cloister.model.store;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.model.domain.MessageState;
import com.earthabbey.cloister.model.domain.MessageSummary;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.MessageEntity;

public class MessageStore extends
		EntityStore<MessageEntity, Long, Message, MessageSummary> {

	public MessageStore(EntityManager entityManager) {
		super(entityManager, MessageEntity.class, new MessageTranslator(
				entityManager));
	}

	@SuppressWarnings("unchecked")
	public List<MessageSummary> getMembersMessages(MemberSummary recipient) {

		MemberEntity memberEntity = entityManager.find(MemberEntity.class,
				recipient.getId());
		Query query = entityManager
				.createQuery("SELECT e FROM MessageEntity e WHERE e.owner= :owner AND NOT (e.state = :state)");
		query.setParameter("owner", memberEntity);
		query.setParameter("state", MessageState.DELETED);

		List<MessageSummary> result = summerise(translate((Collection<MessageEntity>) query
				.getResultList()));

		return result;

	}

	public void deleteMessages(List<MessageSummary> messagesToDelete) {

		for (MessageSummary messageToDelete : messagesToDelete) {
			MessageEntity messageEntity = entityManager.find(
					MessageEntity.class, messageToDelete.getId());
			messageEntity.setState(MessageState.DELETED);
		}

	}
}

class MessageTranslator implements Translator<MessageEntity, Message> {

	private EntityManager entityManager;

	public MessageTranslator(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	@Override
	public Message createDomainObject(MessageEntity entityRecord) {
		Message domainObject = new Message();
		domainObject.setId(entityRecord.getId());
		domainObject.setTitle(entityRecord.getTitle());
		domainObject.setContent(entityRecord.getContent());
		if (entityRecord.getOwner() != null) {
			domainObject.setOwner(entityRecord.getOwner().summerize());
		}
		if (entityRecord.getRecipient() != null) {
			domainObject.setRecipient(entityRecord.getRecipient().summerize());
		}
		if (entityRecord.getSender() != null) {
			domainObject.setSender(entityRecord.getSender().summerize());
		}
		domainObject.setSendTime(entityRecord.getSendTime());
		domainObject.setState(entityRecord.getState());
		return domainObject;
	}

	@Override
	public void updateEntityRecord(Message domainObject,
			MessageEntity entityRecord) {
		entityRecord.setState(domainObject.getState());
		entityRecord.setTitle(domainObject.getTitle());
		entityRecord.setContent(domainObject.getContent());
	}

	@Override
	public MessageEntity createEntityRecord(Message domainObject) {
		MessageEntity entity = new MessageEntity();
		updateEntityRecord(domainObject, entity);
		// the id is not forwarded, as it is allocated by the database.
		entity.setSendTime(domainObject.getSendTime());
		MemberEntity owner = entityManager.find(MemberEntity.class,
				domainObject.getOwner().getId());
		MemberEntity recipient = entityManager.find(MemberEntity.class,
				domainObject.getRecipient().getId());
		MemberEntity sender = entityManager.find(MemberEntity.class,
				domainObject.getSender().getId());
		entity.setOwner(owner);
		entity.setRecipient(recipient);
		entity.setSender(sender);
		return entity;
	}
}
