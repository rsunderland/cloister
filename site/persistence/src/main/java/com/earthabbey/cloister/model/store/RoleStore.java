package com.earthabbey.cloister.model.store;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.domain.RoleSummary;
import com.earthabbey.cloister.model.entity.RoleEntity;

public class RoleStore extends EntityStore<RoleEntity, Long, Role, RoleSummary> {
	public RoleStore(EntityManager entityManager) {
		super(entityManager, RoleEntity.class, new RoleTranslator());
	}

	public Role getCreationRole() {
		return getRole("NewAccount");
	}

	public Role getApplicantRole() {
		return getRole("Applicant");
	}

	public Role getDefaultMemberRole() {
		return getRole("Member");
	}
	
	public RoleEntity getCreationRoleEntity() {
		return getRoleEntity("NewAccount");
	}

	public RoleEntity getApplicantRoleEntity() {
		return getRoleEntity("Applicant");
	}

	public RoleEntity getDefaultMemberRoleEntity() {
		return getRoleEntity("Member");
	}
	
	private RoleEntity getRoleEntity(String string) {
		Query query = entityManager.createQuery("SELECT e FROM "
				+ entityClass.getSimpleName() + " e WHERE e.name=?1");
		query.setParameter(1, string);
		RoleEntity entity = (RoleEntity) query.getSingleResult();
		
		if (entity == null) {	
			throw new IllegalArgumentException("No role with name=" + string
					+ " was found.");
		}
		return entity;		
	}

	private Role getRole(String string) {
		return  translate(getRoleEntity(string));
	}
}

class RoleTranslator implements Translator<RoleEntity, Role> {
	@Override
	public Role createDomainObject(RoleEntity entityRecord) {
		return new Role(entityRecord.getId(), entityRecord.getName(),
				entityRecord.getPermissions());
	}

	@Override
	public RoleEntity createEntityRecord(Role domainObject) {
		// the id is not forwarded, as it is allocated by the database.
		return new RoleEntity(null, domainObject.getName(),
				domainObject.getPermissions());
	}

	@Override
	public void updateEntityRecord(Role domainObject, RoleEntity entityRecord) {
		entityRecord.setName(entityRecord.getName());		
		entityRecord.setPermissions(domainObject.getPermissions());
	}
}
