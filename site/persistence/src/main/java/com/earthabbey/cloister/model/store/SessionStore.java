package com.earthabbey.cloister.model.store;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.PrincipalSummary;
import com.earthabbey.cloister.model.domain.Session;
import com.earthabbey.cloister.model.domain.SessionSummary;
import com.earthabbey.cloister.model.entity.MemberEntity;
import com.earthabbey.cloister.model.entity.PrincipalEntity;
import com.earthabbey.cloister.model.entity.SessionEntity;

public class SessionStore extends
		EntityStore<SessionEntity, Long, Session, SessionSummary> {
	public SessionStore(EntityManager entityManager) {
		super(entityManager, SessionEntity.class, new SessionTranslator(entityManager));
	}
}

class SessionTranslator implements Translator<SessionEntity, Session> {

	private EntityManager entityManager;
	
	public SessionTranslator(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	@Override
	public SessionEntity createEntityRecord(Session domainObject) {
		SessionEntity sessionEntity = new SessionEntity();
		// the id is not forwarded, as it is allocated by the database.
		updateEntityRecord( domainObject, sessionEntity);
		return sessionEntity;
	}

	@Override
	public Session createDomainObject(SessionEntity entityRecord) {

		PrincipalEntity principal = entityRecord.getPrincipal();

		if (principal == null) {
			throw new IllegalStateException(
					"Could not create Session domain object because the entity record did not have a Principal");
		}
		
		Set<Permission> permissions = new HashSet<Permission>();
		permissions.addAll(principal.getRole().getPermissions());
		

		PrincipalSummary summary = principal.summerize();

		MemberSummary member = null;
		if (entityRecord.getMember() != null) {
			member = entityRecord.getMember().summerize();
		}

		Session domainObject = new Session(entityRecord.getId(),
				entityRecord.getToken(), entityRecord.isExpired(),
				entityRecord.getLoginTime(), entityRecord.getLogOutTime(),
				summary, member, permissions);

		return domainObject;
	}

	@Override
	public void updateEntityRecord(Session domainObject,
			SessionEntity entityRecord) {
		entityRecord.setExpired(domainObject.isExpired());
		entityRecord.setLoginTime(domainObject.getLoginTime());
		entityRecord.setLogOutTime(domainObject.getLogOutTime());
		entityRecord.setPrincipal(entityManager.find(PrincipalEntity.class,
				domainObject.getPrincipal().getId()));

		if (domainObject.getMember() != null) {
			entityRecord.setMember(entityManager.find(MemberEntity.class,
					domainObject.getMember().getId()));
		}

		entityRecord.setToken(domainObject.getToken());
	}
}
