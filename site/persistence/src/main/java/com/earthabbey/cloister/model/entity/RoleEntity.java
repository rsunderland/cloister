package com.earthabbey.cloister.model.entity;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.earthabbey.cloister.model.domain.Permission;
import com.earthabbey.cloister.model.domain.RoleSummary;

@Entity
@Table(name = "roles")
public class RoleEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	/**
	 * Set of permissions of the pricipals which have this role.
	 */
	private Set<Permission> permissions;

	/**
	 * Serialisation only.
	 */
	public RoleEntity() {
	}

	public RoleEntity(Long id, String name, Set<Permission> permissions) {
		this.id = id;
		this.name = name;
		this.permissions = permissions;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="role_seq")
	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ElementCollection
	@CollectionTable(name = "role_permissions", joinColumns = @JoinColumn(name = "role_id"))
	@Column(name = "permission")
	@Enumerated(EnumType.STRING)
	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	public RoleSummary summerize() {
		return new RoleSummary(getId(), name);
	}
	
	
	@Override
	public String toString()
	{
		ToStringBuilder bld = new ToStringBuilder(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
		bld.append("id", getId());
		bld.append("name", name);	
		bld.append("perms",permissions);
		return bld.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((permissions == null) ? 0 : permissions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleEntity other = (RoleEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (permissions == null) {
			if (other.permissions != null)
				return false;
		} else if (!permissions.equals(other.permissions))
			return false;
		return true;
	}

}
