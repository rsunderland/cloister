package com.earthabbey.cloister.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;

import com.earthabbey.cloister.model.domain.SessionSummary;

@Entity
@Table(name = "sessions")
public class SessionEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private String token;

	private PrincipalEntity principal;

	private MemberEntity member;

	private Date loginTime;

	private Date logOutTime;

	private boolean expired;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="session_seq")
	@XmlAttribute
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlAttribute
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@XmlAttribute
	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@XmlAttribute
	public Date getLogOutTime() {
		return logOutTime;
	}

	public void setLogOutTime(Date logOutTime) {
		this.logOutTime = logOutTime;
	}

	@XmlAttribute
	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	@ManyToOne
	public PrincipalEntity getPrincipal() {
		return principal;
	}

	public void setPrincipal(PrincipalEntity principal) {
		this.principal = principal;
	}

	@ManyToOne
	public MemberEntity getMember() {
		return member;
	}

	public void setMember(MemberEntity member) {
		this.member = member;
	}

	public SessionSummary summerize() {
		return new SessionSummary(getId());
	}
}
