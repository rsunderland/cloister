package com.earthabbey.cloister.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;

import com.earthabbey.cloister.model.domain.MemberSummary;
import com.earthabbey.cloister.model.domain.MemberType;

@Entity
@Table(name = "members")
public class MemberEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1642892803169401291L;

	private Long id;

	private MemberType type;

	private String firstName;

	private String surname;

	private String wayOfLifeSummary;

	private MemberEntity mentor;

	private List<MemberHelpOfferEntity> helpOffers;

	private List<MemberHelpRequestEntity> helpRequests;

	private PrincipalEntity principal;

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "member_seq")
	@XmlTransient
	public Long getId() {
		return id;
	}

	@Transient
	@XmlID
	@XmlAttribute(name = "id")
	public String getXmlId() {
		return (id != null) ? id.toString() : "0";
	}
	
	public void setXmlId(String xmlId){
		id = Long.parseLong(xmlId);
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlAttribute
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@XmlAttribute
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Lob
	public String getWayOfLifeSummary() {
		return wayOfLifeSummary;
	}

	public void setWayOfLifeSummary(String wayOfLifeSummary) {
		this.wayOfLifeSummary = wayOfLifeSummary;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	public MemberEntity getMentor() {
		return mentor;
	}

	public void setMentor(MemberEntity mentor) {
		this.mentor = mentor;
	}

	public String toString() {
		StringBuilder bld = new StringBuilder();
		bld.append(" id=").append(getId());
		bld.append(" firstname=").append(firstName);
		bld.append(" surname=").append(surname);
		return bld.toString();
	}

	@XmlAttribute
	@Enumerated(EnumType.STRING)
	public MemberType getType() {
		return type;
	}

	public void setType(MemberType type) {
		this.type = type;
	}

	@XmlElementWrapper(name = "helpOffers")
	@XmlElement(name = "MemberHelpOffer")
	@OneToMany(mappedBy = "offeringMember", cascade = CascadeType.MERGE)
	public List<MemberHelpOfferEntity> getHelpOffers() {
		return helpOffers;
	}

	public void setHelpOffers(List<MemberHelpOfferEntity> helpOffers) {
		this.helpOffers = helpOffers;
	}

	@XmlElementWrapper(name = "helpRequests")
	@XmlElement(name = "MemberHelpRequest")
	@OneToMany(mappedBy = "requestingMember", cascade = CascadeType.MERGE)
	public List<MemberHelpRequestEntity> getHelpRequests() {
		return helpRequests;
	}

	public void setHelpRequests(List<MemberHelpRequestEntity> helpRequests) {
		this.helpRequests = helpRequests;
	}

	public MemberSummary summerize() {
		return new MemberSummary(getId(), firstName, surname, type);
	}

	@OneToOne(mappedBy = "member", cascade = CascadeType.REMOVE)
	public PrincipalEntity getPrincipal() {
		return principal;
	}

	public void setPrincipal(PrincipalEntity principal) {
		this.principal = principal;
	}

	@PostPersist
	public void maintainBackReference() {
		principal.setMember(this);
	}
}
