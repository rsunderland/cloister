package com.earthabbey.cloister.model.store;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import com.earthabbey.cloister.model.domain.Backup;
import com.earthabbey.cloister.model.domain.DomainObject;
import com.earthabbey.cloister.model.domain.EmailConfirmation;
import com.earthabbey.cloister.model.domain.Member;
import com.earthabbey.cloister.model.domain.MemberHelpOffer;
import com.earthabbey.cloister.model.domain.MemberHelpRequest;
import com.earthabbey.cloister.model.domain.Message;
import com.earthabbey.cloister.model.domain.Principal;
import com.earthabbey.cloister.model.domain.Role;
import com.earthabbey.cloister.model.domain.RoleSummary;
import com.earthabbey.cloister.model.domain.Session;

public class Store {

	private PrincipalStore principalStore;
	private RoleStore roleStore;
	private MemberStore memberStore;
	private SessionStore sessionStore;
	private MessageStore messageStore;
	private EmailConfirmationStore emailConfirmationStore;
	private MemberHelpOfferStore memberHelpOfferStore;
	private MemberHelpRequestStore memberHelpRequestStore;
	private EntityManager entityManager;

	public Store(EntityManager entityManager) {
		this.entityManager = entityManager;
		roleStore = new RoleStore(entityManager);
		memberStore = new MemberStore(entityManager);
		messageStore = new MessageStore(entityManager);
		principalStore = new PrincipalStore(entityManager);
		sessionStore = new SessionStore(entityManager);
		emailConfirmationStore = new EmailConfirmationStore(entityManager);
		memberHelpOfferStore = new MemberHelpOfferStore(entityManager);
		memberHelpRequestStore = new MemberHelpRequestStore(entityManager);
	}

	public MemberStore getMemberStore() {
		return memberStore;
	}

	public PrincipalStore getPrincipalStore() {
		return principalStore;
	}

	public MessageStore getMessageStore() {
		return messageStore;
	}

	public SessionStore getSessionStore() {
		return sessionStore;
	}

	public RoleStore getRoleStore() {
		return roleStore;
	}

	public EmailConfirmationStore getEmailConfirmationStore() {
		return emailConfirmationStore;
	}

	public Backup createBackup() {
		List<Role> roles = roleStore.readAll();
		List<Principal> principals = principalStore.readAll();
		List<Member> members = memberStore.readAll();
		List<Message> messages = messageStore.readAll();
		List<Session> sessions = sessionStore.readAll();
		List<EmailConfirmation> emailConfirmations = emailConfirmationStore
				.readAll();
		List<MemberHelpOffer> helpOffers = memberHelpOfferStore.readAll();
		List<MemberHelpRequest> helpRequests = memberHelpRequestStore.readAll();
		return new Backup(members, messages, principals, roles, sessions,
				emailConfirmations, helpOffers, helpRequests);
	}

	public void deleteAll() {
		memberHelpOfferStore.deleteAll();
		memberHelpRequestStore.deleteAll();
		emailConfirmationStore.deleteAll();
		sessionStore.deleteAll();
		principalStore.deleteAll();
		messageStore.deleteAll();
		memberStore.deleteAll();
		roleStore.deleteAll();
	}

	public void restoreBackup(Backup backup) {
		// warning: after delete all, all roles will have been destroyed.
		deleteAll();

		emailConfirmationStore.createAll(backup.getEmailConfirmations());

		Map<Long, Long> roleIdMap = restore(roleStore, backup.getRoles());
		updateRoleIds(roleIdMap, backup.getPrincipals());
		principalStore.createAll(backup.getPrincipals());

		restoreMembers(backup); // this will modify the backup to
								// reflect id
		// changes.
		messageStore.createAll(backup.getMessages());
		sessionStore.createAll(backup.getSessions());
		memberHelpOfferStore.createAll(backup.getHelpOffers());
		memberHelpRequestStore.createAll(backup.getHelpRequests());
	}

	private void restoreMembers(Backup backup) {
		Map<Long, Long> memberIdMap = restore(memberStore, backup.getMembers());

		reintroduceRelationships(backup, memberIdMap);

		// update rest of back up to reflect change in member ids.
		updateReferences(memberIdMap, backup.getPrincipals(),
				backup.getMessages(), backup.getHelpOffers(),
				backup.getHelpRequests());
	}

	private void reintroduceRelationships(Backup backup,
			Map<Long, Long> memberIdMap) {
		// now go through members updating them to reintroduce
		// relationships

		for (Member member : backup.getMembers()) {
			Long backupMemberId = member.getId();
			member.setId(memberIdMap.get(backupMemberId));
			if (member.getMentor() != null) {
				Long backupMentorId = member.getMentor().getId();
				Long restoredMentorId = memberIdMap.get(backupMentorId);
				if (restoredMentorId == null) {
					throw new IllegalStateException(
							"Could not restore member id=" + backupMemberId
									+ " because no such mentor id="
									+ backupMentorId);
				} else {
					member.getMentor().setId(restoredMentorId);
				}

				memberStore.setMentor(member, member.getMentor());
			}
		}
	}

	private void updateRoleIds(Map<Long, Long> roleIdMap,
			List<Principal> principals) {
		for (Principal principal : principals) {
			RoleSummary role = principal.getRole();
			if (role == null) {
				throw new IllegalArgumentException(
						"Attempting to update principal " + principal.getId()
								+ " but it did not have a associated role");
			}
			role.setId(roleIdMap.get(role.getId()));
		}
	}

	private void updateReferences(Map<Long, Long> memberIdMap,
			List<Principal> principals, List<Message> messages,
			List<MemberHelpOffer> helpOffers,
			List<MemberHelpRequest> helpRequests) {
		for (Principal principal : principals) {
			if (principal.getMember() != null) {
				principal.getMember().setId(
						memberIdMap.get(principal.getMember().getId()));
			}
		}

		for (Message message : messages) {
			if (message.getOwner() != null) {
				message.getOwner().setId(
						memberIdMap.get(message.getOwner().getId()));
			}
			if (message.getRecipient() != null) {
				message.getRecipient().setId(
						memberIdMap.get(message.getRecipient().getId()));
			}

			if (message.getSender() != null) {
				message.getSender().setId(
						memberIdMap.get(message.getSender().getId()));
			}
		}

		for (MemberHelpOffer helpOffer : helpOffers) {
			helpOffer.getOfferingMember().setId(
					memberIdMap.get(helpOffer.getOfferingMember().getId()));
		}

		for (MemberHelpRequest helpRequest : helpRequests) {
			helpRequest.getRequestingMember().setId(
					memberIdMap.get(helpRequest.getRequestingMember().getId()));
		}
	}

	/**
	 * Create a set of objects and collect a map from their old keys to their
	 * new keys.
	 * 
	 * @param <K>
	 *            type of key.
	 * @param <D>
	 *            type of domain object being restored.
	 * @param store
	 *            the store into which the objects are being restored.
	 * @param backupObjects
	 *            the objects that are being restored.
	 * @return map from the ids of the backup objects to the restored objects.
	 */
	private <K, D extends DomainObject<K, ?>> Map<K, K> restore(
			EntityStore<?, K, D, ?> store, List<D> backupObjects) {
		// map from key in backup to key in database.
		Map<K, K> keyMap = new HashMap<K, K>();
		List<D> restoredObjects = store.createAll(backupObjects);
		for (int i = 0; i < restoredObjects.size(); i++) {
			keyMap.put(backupObjects.get(i).getId(), restoredObjects.get(i)
					.getId());
		}
		return keyMap;
	}

	public MemberHelpOfferStore getMemberHelpOfferStore() {
		return memberHelpOfferStore;
	}

	public MemberHelpRequestStore getMemberHelpRequestStore() {
		return memberHelpRequestStore;
	}

	public void flush() {
		entityManager.flush();
	}
}
