#!/bin/bash

useradd -g tomcat cloister

chmod g+rx /home/cloister

mkdir /home/cloister/prod
mkdir /home/cloister/test
mkdir /home/cloister/tx
mkdir /home/cloister/backup
mkdir /home/cloister/bin

chown cloister /home/cloister/prod /home/cloister/test /home/cloister/tx /home/cloister/backup /home/cloister/bin
chgrp tomcat /home/cloister/prod /home/cloister/test /home/cloister/tx /home/cloister/backup /home/cloister/bin
chmod g+xw  /home/cloister/prod /home/cloister/test /home/cloister/tx /home/cloister/backup /home/cloister/bin

echo "export CLOISTER_PROD=/home/cloister/prod/" >> /home/cloister/.bashrc
echo "export CLOISTER_TEST=/home/cloister/test/" >> /home/cloister/.bashrc
echo "export CLOISTER_BACKUP=/home/cloister/backup/" >> /home/cloister/.bashrc
echo "#export CLOISTER_BACKUP_EMAIL=[REPLACEME]" >> /home/cloister/.bashrc
echo "export CATALINA_HOME=/usr/share/tomcat5" >> /home/cloister/.bashrc


cp backup.sh /home/cloister/bin
crontab -u cloister crontab.txt
