#!/bin/bash


function error {
	echo "BACKUP FAILED"
	echo $1
	exit -1
}


if test "${CLOISTER_PROD+set}" != set ; then
		error "Environment variable CLOISTER_PROD not set."
fi

if [ ! -d "${CLOISTER_PROD}" ]; then
		error "Environment variable CLOISTER_PROD is set to $CLOISTER_PROD but this doesn't exst."
fi

if test "${CLOISTER_BACKUP+set}" != set ; then
		error "Environment variable CLOISTER_BACKUP not set."
fi

if [ ! -d "${CLOISTER_BACKUP}" ]; then
		error "Environment variable CLOISTER_BACKUP is set to $CLOISTER_BACKUP but this doesn't exst."
fi

if test "${CLOISTER_BACKUP_EMAIL+set}" != set ; then
		error "Environment variable CLOISTER_BACKUP_EMAIL not set."
fi



clijar=$(ls ${CLOISTER_PROD}/live/WEB-INF/lib/cloister-cli*.jar)



fullfile="${CLOISTER_BACKUP}/cloister_backup_$(date +%Y%m%d_%H%M).xml"

java -jar ${clijar} -backup ${fullfile}

gzip ${fullfile}

echo "${fullfile} attached" | mutt  -a ${fullfile}.gz   -s "Cloister backup ${fullfile}" ${CLOISTER_BACKUP_EMAIL}

echo "Backup generated and sent"





