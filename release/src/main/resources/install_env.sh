#!/bin/bash

function uninstall {
        if rpm -q --quiet $1; then
                echo -e "    removing   $1"
		yum  erase -y -q $1
        else
                echo -e "    checked absent  $1"
	fi
}

function install {
        if rpm -q --quiet $1; then
                echo -e "    checked present $1"
        else
                echo -e "    installing $1"
		yum install -y -q $1
        fi
}


echo "Earth Abbey Cloister Installation Script"
	uninstall sendmail
	install unzip
	install java-1.6.0-openjdk
	install java-1.6.0-openjdk-devel
	install tomcat5
	install mysql
	install mysql-server
	install vim-enhanced
	install postfix
	install at


# useradd cloister


echo "Setting up port redirects"
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -t nat -I PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8080
iptables -t nat -I OUTPUT -p tcp --dport 80 -j REDIRECT --to-ports 8080
service iptables save

echo "MANUAL STEP -- set proxy port on server.xml"






