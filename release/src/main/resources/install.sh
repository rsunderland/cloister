#!/bin/bash


function error {
	echo "INSTALLATION FAILED"
	echo $1
	exit -1
}

function add_persistence_xml {
	srcpath=$1
	warpath=$2

	jarpath=$(ls ${warpath}/WEB-INF/lib/clo*persistence*.jar)

	if [ ! -e "${srcpath}" ]; then
		error "Could not find persistence xml (${srcpath})"
	fi

	if [ ! -e "${jarpath}" ]; then
		error "Could not find persistence jar (${jarpath})"
	fi

	mkdir META-INF
	cp ${srcpath} META-INF/persistence.xml

	zip -q ${jarpath} META-INF/persistence.xml

	rm -rf META-INF

	echo "Added ${srcpath} to ${jarpath}"
}

function install 
{
	dir=$1
	context=$2

	installwar="${dir}/${base}"

	if [ ! -d "${dir}" ]; then
		mkdir ${dir}
	fi

	if [ -d "${installwar}" ]; then
		echo "Installation directory already existed, removing it.";
		rm -rf ${installwar}
	fi
	
	unzip -q ${war}  -d ${installwar}	

	add_persistence_xml ${dir}/persistence.xml ${installwar}

	echo -e "<Context path=\"\" docBase=\"${installwar}\" />" >  $CATALINA_HOME/conf/Catalina/localhost/${context}

}

function test_install {
	echo -e "Installing test deployment of cloister (verion $base)"
	install ${CLOISTER_TEST} test#${version}.xml
}

function production_install {
	echo -e "Installing production deployment of cloister (verion $base)"
	install ${CLOISTER_PROD} ROOT.xml
	if [ -e ${CLOSTIER_PROD}/live ]; then
		rm ${CLOISTER_PROD}/live
	fi

	ln -s ${CLOISTER_PROD}/${base} ${CLOISTER_PROD}/live
}

war=$(ls cloister*.war)
base=$(basename $war .war)
version=${base#cloister-site-ui-}



if test "${CLOISTER_PROD+set}" != set ; then
		error "Environment variable CLOISTER_PROD not set."
fi

if [ ! -d "${CLOISTER_PROD}" ]; then
		error "Environment variable CLOISTER_PROD is set to $CLOISTER_PROD but this doesn't exst."
fi

if test "${CLOISTER_TEST+set}" != set ; then
		error "Environment variable CLOISTER_TEST not set."
fi

if [ ! -d "${CLOISTER_TEST}" ]; then
		error "Environment variable CLOISTER_TEST is set to $CLOISTER_TEST but this doesn't exst."
fi

if test "${CATALINA_HOME+set}" != set ; then
		error "Environment variable CATALINA_HOME not set."
fi

if [ ! -d "${CATALINA_HOME}" ]; then
		error "Environment variable CATALINA is set to $CATALINA_HOME but this doesn't exst."
fi

echo "Installing version ${version}"


read -p "Is this a (p)roduction installation or a (t)est installation (p/t)? " REPLY

if [ "$REPLY" == "t" ]; then
	 test_install
elif [ "$REPLY" == "p" ]; then
	 production_install
else
	error "Unrecognised option ($REPLY)"
fi



